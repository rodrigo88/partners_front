export const productoresACargo = [
  {
    id: 0,
    name: "Natalia Rodriguez",
    team: 4,
    c: 1000,
    subList: [
      { id: 0, type: "A", subName: "Cartera Natalia", c: 64 },
      { id: 1, type: "A", subName: "Jackelyn Jara", c: 92 },
      { id: 2, type: "A", subName: "Pablo Juanes", c: 100 }
    ]
  },
  {
    id: 1,
    name: "Mariano Gonzalez",
    team: 0,
    c: 152,
    subList: [
      { id: 0, type: "A", subName: "Cartera Natalia", c: 64 },
      { id: 1, type: "A", subName: "Jackelyn Jara", c: 92 },
      { id: 2, type: "A", subName: "Pablo Juanes", c: 100 }
    ]
  },
  {
    id: 2,
    name: "Evelyn Ropaldo",
    team: 10,
    c: 556,
    subList: [
      { id: 0, type: "A", subName: "Cartera Natalia", c: 64 },
      { id: 1, type: "A", subName: "Jackelyn Jara", c: 92 },
      { id: 2, type: "A", subName: "Pablo Juanes", c: 100 }
    ]
  },
  {
    id: 0,
    name: "Juan Acevedo",
    team: 6,
    c: 2500,
    subList: [
      { id: 0, type: "A", subName: "Cartera Natalia", c: 64 },
      { id: 1, type: "A", subName: "Jackelyn Jara", c: 92 },
      { id: 2, type: "A", subName: "Pablo Juanes", c: 100 }
    ]
  }
];

export const teamsPartner = {
  totalProducers: "4",
  totalPortfolio: "41",
  producers: [
    {
      idProducer: 83,
      fullName: "Federico Cabrera",
      totalPortfolio: "16"
    },
    {
      idProducer: 100,
      fullName: "AG Franco 1 Caballero",
      totalPortfolio: "7"
    },
    {
      idProducer: 101,
      fullName: "AG Franco 2 Caballero",
      totalPortfolio: "6"
    },
    {
      idProducer: 102,
      fullName: "AG Franco 3 Caballero",
      totalPortfolio: "10"
    },
    {
      idProducer: 103,
      fullName: "AG Franco 4 Caballero",
      totalPortfolio: "2"
    }
  ]
};
