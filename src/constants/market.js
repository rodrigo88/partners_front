export const optionsTicker = [
  { value: 0, description: "Dólar MEP" },
  { value: 1, description: "Dólar CCL" },
  { value: 2, description: "Pesos ARS" },
  { value: 3, description: "No aplicar filtro" }
];

export const optionsClase = [
  { value: 0, description: "Persona Física" },
  { value: 1, description: "Persona Jurídica" },
  { value: 2, description: "No Aplicar filtero" }
];

export const options = [
  { value: 0, description: "Ultima semana" },
  { value: 1, description: "Ultimos 30 días" },
  { value: 2, description: "Ultimos 60 días" },
  { value: 3, description: "Año calendario" }
];
