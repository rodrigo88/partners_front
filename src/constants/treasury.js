// export const userBalance = {
//   descriptionToday: "availableToday",
//   valueToday: "15.689,57",
//   description24: "available24",
//   value24: "9.689,99",
//   description48: "available48",
//   value48: "12.568,44",
//   descriptionTotal: "total",
//   valueTotal: "37.948,00"
// };

export const userBalance = [
  {
    description: "Disp. Ahora",
    currency: "ARS",
    value: "15.689,57",
    ratio: 50
  },
  {
    description: "Disp. en 24hs",
    currency: "ARS",
    value: "9.689,99",
    ratio: 10
  },
  {
    description: "Disp. en 48hs",
    currency: "ARS",
    value: "12.568,44",
    ratio: 40
  }
];

export const treasuryData = [
  {
    date: "08/04/2020",
    client: "Rodrigo Castro",
    account: "5789",
    tLiquida: "4500",
    detail: "Venta AY24",
    currency: "Pesos",
    amount: 14.0,
    state: "PENDIENTE",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "08/04/2020",
    client: "Marcelo Arias",
    account: "5788",
    tLiquida: "5500",
    detail: "Venta AY23",
    currency: "Pesos",
    amount: 24.0,
    state: "ACEPTADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "09/04/2020",
    client: "Leonardo Arias",
    account: "5787",
    tLiquida: "5500",
    detail: "Venta AY23",
    currency: "Pesos",
    amount: 34.0,
    state: "RECHAZADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "09/04/2020",
    client: "Marcelo Quiroga",
    account: "4787",
    tLiquida: "4500",
    detail: "Venta AY13",
    currency: "Pesos",
    amount: 54.0,
    state: "ACEPTADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "09/04/2020",
    client: "Nahuel Quiroga",
    account: "4787",
    tLiquida: "4500",
    detail: "Venta AY13",
    currency: "Pesos",
    amount: 44.0,
    state: "RECHAZADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },

  {
    date: "09/04/2020",
    client: "Horacio Deluca",
    account: "2787",
    tLiquida: "2500",
    detail: "Venta AY63",
    currency: "Pesos",
    amount: 24.0,
    state: "RECHAZADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "09/04/2020",
    client: "Miguel Deluca",
    account: "2787",
    tLiquida: "2500",
    detail: "Venta AY63",
    currency: "Pesos",
    amount: 24.0,
    state: "ACEPTADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "09/04/2020",
    client: "Carlos Deluca",
    account: "2787",
    tLiquida: "2500",
    detail: "Venta AY63",
    currency: "Pesos",
    amount: 24.0,
    state: "PENDIENTE",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "09/04/2020",
    client: "Hernan Deluca",
    account: "2787",
    tLiquida: "2500",
    detail: "Venta AY63",
    currency: "Pesos",
    amount: 24.0,
    state: "ACEPTADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },

  {
    date: "10/04/2020",
    client: "Pablo Juanes",
    account: "6787",
    tLiquida: "8500",
    detail: "Deposito",
    currency: "Pesos",
    amount: 74.0,
    state: "RECHAZADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "10/04/2020",
    client: "Pablo Vazquez",
    account: "6787",
    tLiquida: "8500",
    detail: "Deposito",
    currency: "Pesos",
    amount: 74.0,
    state: "ACEPTADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "10/04/2020",
    client: "Jackelin Jara",
    account: "6787",
    tLiquida: "8500",
    detail: "Deposito",
    currency: "Pesos",
    amount: 74.0,
    state: "PENDIENTE",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "10/04/2020",
    client: "Evelyn Ropaldo",
    account: "6787",
    tLiquida: "8500",
    detail: "Deposito",
    currency: "Pesos",
    amount: 74.0,
    state: "ACEPTADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },

  {
    date: "12/04/2020",
    client: "Eduardo Toledo",
    account: "1787",
    tLiquida: "1500",
    detail: "Extraccion",
    currency: "Pesos",
    amount: 64.0,
    state: "RECHAZADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "12/04/2020",
    client: "Federico Toledo",
    account: "1787",
    tLiquida: "1500",
    detail: "Extraccion",
    currency: "Pesos",
    amount: 64.0,
    state: "ACEPTADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "12/04/2020",
    client: "Cristian Toledo",
    account: "1787",
    tLiquida: "1500",
    detail: "Extraccion",
    currency: "Pesos",
    amount: 64.0,
    state: "PENDIENTE",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  },
  {
    date: "12/04/2020",
    client: "Nicolas Toledo",
    account: "1787",
    tLiquida: "1500",
    detail: "Extraccion",
    currency: "Pesos",
    amount: 64.0,
    state: "ACEPTADO",
    instrument: "AY24",
    price: "$ 28.00",
    quantity: "500"
  }
];
