export const getDataPartnerById = async (url, id) => {
  let response;

  try {
    const res = await fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        idProducer: id
      })
    });

    response = await res.json();
  } catch (error) {
    response = {
      user: false
    };
  }

  return response;
};
