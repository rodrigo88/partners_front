export const getMarketInfo = async url => {
  let response;

  try {
    const res = await fetch(url);

    response = await res.json();
  } catch (error) {
    response = {
      user: false
    };
  }

  return response;
};
