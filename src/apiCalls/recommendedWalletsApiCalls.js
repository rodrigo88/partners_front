export const getRecommendedWallets = async url => {
  let response;

  try {
    const res = await fetch(url, {
      method: "GET"
      // headers: { "Content-Type": "application/json" }
    });

    response = await res.json();
  } catch (err) {
    console.log(err);
  }

  return response;
};
