export const getTeamPartnerById = async (
  url,
  idProducer,
  idLegalPartner,
  type
) => {
  let response;

  try {
    const res = await fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        idProducer: idProducer,
        idLegalPartner: idLegalPartner,
        type: type
      })
    });

    response = await res.json();
  } catch (error) {
    response = {
      user: false
    };
  }

  return response;
};
