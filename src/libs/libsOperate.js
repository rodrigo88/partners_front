export const randomInt = (min, max) => {
  return min + Math.floor((max - min) * Math.random());
};

export const userWalletsTable = list => {
  return (
    (list &&
      list.map((element, i) => {
        return {
          id: i,
          idCustomer: element.idCustomer,
          fullName: element.name,
          physicalPerson: element.isPhysicalPerson,
          ganancia: "0." + randomInt(10, 90) + "%",
          quantity: "0",
          price: "",
          limit: "$0,00",
          checked: true
        };
      })) ||
    []
  );
};

export const setCheckList = (radio, arrayList) => {
  const id = radio.id;
  const list = arrayList.map(term =>
    id === term.id ? setCheckedRadio(term) : setUncheckedRadio(term)
  );
  return list;
};

export const setCheckedRadio = currentTerm => {
  const { id, term, subText } = currentTerm;
  return { id, term, subText, checked: true };
};

export const setUncheckedRadio = currentTerm => {
  const { id, term, subText } = currentTerm;
  return { id, term, subText, checked: false };
};

export const checkElement = element => {
  const {
    id,
    fullName,
    idCustomer,
    physicalPerson,
    ganancia,
    quantity,
    price,
    limit
  } = element;
  return {
    id,
    fullName,
    idCustomer,
    physicalPerson,
    ganancia,
    quantity,
    price,
    limit,
    checked: true
  };
};

export const unCheckElement = element => {
  const {
    id,
    fullName,
    idCustomer,
    physicalPerson,
    ganancia,
    quantity,
    price,
    limit
  } = element;
  return {
    id,
    fullName,
    idCustomer,
    physicalPerson,
    ganancia,
    quantity,
    price,
    limit,
    checked: false
  };
};

export const updateQuantityElement = (partner, caracter) => {
  const {
    id,
    fullName,
    idCustomer,
    physicalPerson,
    ganancia,
    quantity,
    price,
    limit,
    checked
  } = partner;

  return {
    id,
    fullName,
    idCustomer,
    physicalPerson,
    ganancia,
    quantity: caracter === "" ? "0" : caracter,
    limit,
    checked,
    price
  };
};

export const updatePriceElement = (partner, caracter) => {
  const {
    id,
    fullName,
    idCustomer,
    physicalPerson,
    ganancia,
    quantity,
    price,
    limit,
    checked
  } = partner;

  return {
    id,
    fullName,
    idCustomer,
    physicalPerson,
    ganancia,
    quantity,
    limit,
    checked,
    price: parseInt(caracter)
  };
};
