export const setMapBillingTypes = arrayList => {
  return arrayList.map(element => {
    return {
      description: element.name,
      value: element.codeBillingType
    };
  });
};

export const setMapProvinces = arrayList => {
  return arrayList.map(element => {
    return {
      description: element.name,
      value: element.codeProvince
    };
  });
};

export const selectAccountPaymentMethod = (id, list) => {
  return list.map(account =>
    id === account.id
      ? setPrimaryAccount(account)
      : setSecondaryAccount(account)
  );
};

const setPrimaryAccount = account => {
  const { id, typeAccount, bank, cbuValue } = account;
  return { id, typeAccount, bank, primary: true, cbuValue };
};

const setSecondaryAccount = account => {
  const { id, typeAccount, bank, cbuValue } = account;
  return { id, typeAccount, bank, primary: false, cbuValue };
};
