export const setCheckListRadio = (list, radio) => {
  const id = radio.id;
  const new_list = list.map(term =>
    id === term.id ? setCheckedRadio(term) : setUncheckedRadio(term)
  );
  return new_list;
};

export const setCheckedRadio = currentTerm => {
  const { id, term, days } = currentTerm;
  return { id, term, days, checked: true };
};

export const setUncheckedRadio = currentTerm => {
  const { id, term, days } = currentTerm;
  return { id, term, days, checked: false };
};

export const sortByTicker = arrayList => {
  return arrayList.sort((a, b) => a.ticker.localeCompare(b.ticker));
};

export const termAsociated = (arrayList, element, radio) => {
  const myRadio = arrayList.find(x => x.term === radio);
  return myRadio.days === element.settlementTerm;
};
