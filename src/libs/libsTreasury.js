export const userBalanceList = (arrayBalance, arrayColors) => {
  return arrayBalance.map((element, i) => {
    return {
      id: i + 1,
      color: arrayColors[i],
      name: element.description,
      currency: element.currency,
      value: element.value,
      percentage: parseInt(element.ratio)
    };
  });
};
