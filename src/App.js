import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import { withAuthenticator, AmplifySignOut } from "@aws-amplify/ui-react";
import { Auth } from "aws-amplify";
import awsconfig from "./aws-exports";
import { setLogIn, setLogOut } from "./redux/actions";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { Dashboard } from "./views/dashboard/dashboard";
import { Login } from "./views/login/login";
// import "./App.css";

Auth.configure(awsconfig);

function App() {
  const dispatch = useDispatch();

  const state = useSelector(state => state);
  const { isLogged } = state;

  useEffect(() => {
    // const history = useHistory();
    const getUserLogged = async () => {
      let user = await Auth.currentAuthenticatedUser();
      if (user) {
        dispatch(setLogIn());
        // history.push("/home");
      } else {
        dispatch(setLogOut());
      }
    };

    getUserLogged();
  }, [dispatch]);

  return (
    <div>
      <Router>
        <Switch>{isLogged ? <Dashboard /> : <Login />}</Switch>
      </Router>
    </div>
  );
}

export default App;
// export default withAuthenticator(App);
