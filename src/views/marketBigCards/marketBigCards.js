import React, { useState } from "react";
import { sortByTicker, termAsociated } from "../../libs/libsMarket";
import { Divider } from "../../components/divider/divider";
import iconHomeNewsMarket from "../../utils/Enmascarar grupo 1@2x.png";
import iconMarketCard from "../../utils/icon-mercado-mover.svg";
import iconAmpliarMarketCard from "../../utils/icon-mercado-ampliar.svg";
import iconStaticMarket from "../../utils/Grupo 4029.svg";
import iconArrowRightCardMarket from "../../utils/ArrowRightCardMarket.svg";
import iconAmpliarInfo from "../../utils/Ampliar información.svg";
import "./marketBigCards.css";

export const MarketBigCards = ({
  listMarketCards,
  clients,
  selectedRadio,
  selectCard,
  fundsData,
  bondsData,
  listTerms,
  cedearsData,
  equityData
}) => {
  return (
    <div className="market-container-card-list">
      {listMarketCards &&
        listMarketCards.map(cardMarket => (
          <CardMarket
            key={cardMarket.id}
            cardMarket={cardMarket}
            selectCard={selectCard}
            clients={clients}
            selectedRadio={selectedRadio}
            data={
              cardMarket.type === "FUND"
                ? fundsData
                : cardMarket.type === "CERTIFICATE"
                ? cedearsData
                : cardMarket.type === "EQUITY"
                ? equityData
                : bondsData
            }
            listTerms={listTerms}
          />
        ))}
    </div>
  );
};

/* ************************************************************************ */

const CardMarket = ({
  cardMarket,
  selectCard,
  selectedRadio,
  data,
  listTerms
}) => {
  const [selectedClient, setSelectedClient] = useState(0);

  const chooseClient = idx => setSelectedClient(idx);

  const { title, options, type } = cardMarket;

  const [subItemSelected, setSubItemSelected] = useState(options && options[0]);

  const filteredElements = () => {
    if (type === "FUND") {
      return sortByTicker(
        data.filter(
          element =>
            element.subType === subItemSelected ||
            element.data.PROFILE === subItemSelected
        )
      );
    }
    if (subItemSelected === "") {
      return sortByTicker(
        data.filter(element => termAsociated(listTerms, element, selectedRadio))
      );
    } else {
      return sortByTicker(
        data
          .filter(element => termAsociated(listTerms, element, selectedRadio))
          .filter(
            element =>
              element.subType === subItemSelected ||
              element.data.PROFILE === subItemSelected
          )
      );
    }

    // MOSTRAR SOLO ENABLED = true ...
  };

  return (
    <div className="market-container-card-list-element">
      <div className="market-container-card-list-element-header">
        <div
          onClick={() => selectCard(cardMarket)}
          className="market-container-card-list-element-header-title"
        >
          <div>{title}</div>
          <Divider name="S" />
          <img src={iconAmpliarMarketCard} alt="..." />
        </div>
        <div>
          <img src={iconMarketCard} alt="..." />
        </div>
      </div>
      <div className="market-container-card-list-element-suboptions">
        {options &&
          options.map((option, i) => (
            <div
              onClick={() => {
                subItemSelected === option
                  ? setSubItemSelected("")
                  : setSubItemSelected(option);
              }}
              style={{ pointerEvents: subItemSelected === option && "none" }}
              className={`market-container-card-list-element-suboptions-subitem ${
                subItemSelected === option ? "subItemSelected" : ""
              }`}
              key={i}
            >
              {option.toLowerCase()}
            </div>
          ))}
      </div>
      <div className="market-container-card-list-element-statics">
        <img src={iconStaticMarket} alt="..." />
      </div>
      <div className="market-container-card-list-element-months">
        <div>Enero</div>
        <div>Mes Actual</div>
      </div>
      <div className="market-container-card-list-element-members">
        {filteredElements().map((element, i) => (
          <Client
            key={i}
            element={element}
            index={i}
            selectedClient={selectedClient}
            chooseClient={chooseClient}
          />
        ))}
      </div>
      <div
        className="market-container-card-list-element-more-info"
        onClick={() => selectCard(cardMarket)}
      >
        <img src={iconArrowRightCardMarket} alt="..." />
        <Divider name="XS" />
        <img src={iconAmpliarInfo} alt="..." />
      </div>
    </div>
  );
};

/* ************************************************************************ */

const Client = ({ element, selectedClient, chooseClient, index }) => {
  const { name, ticker, type, data } = element;
  return (
    <div
      onClick={() => chooseClient(index)}
      className={`market-container-card-list-element-members-item ${
        index === selectedClient ? "client-selected" : ""
      }`}
    >
      <div className="market-container-card-list-element-members-item-1">
        <img
          style={{ height: "25px", width: "25px", borderRadius: "50%" }}
          src={iconHomeNewsMarket}
          alt="..."
        />
      </div>
      <div className="market-container-card-list-element-members-item-2">
        {ticker}
        {/* *** */}
        <div>{type !== "FUND" && <div>{name}</div>}</div>
        {/* *** */}
      </div>
      <div className="market-container-card-list-element-members-item-3">
        $ {data.NOW_PRICE}
      </div>
      <div
        className="market-container-card-list-element-members-item-4"
        style={{
          color:
            data.DAY_PERCENTAGE_CHANGE > 0
              ? "#0AB8B2"
              : data.DAY_PERCENTAGE_CHANGE === 0
              ? "#000000"
              : "#E80451"
        }}
      >
        <div>
          {data.DAY_PERCENTAGE_CHANGE && data.DAY_PERCENTAGE_CHANGE.toFixed(2)}%
        </div>
        {type !== "FUND" && <div>$ {data.DAY_PRICE_CHANGE}</div>}
      </div>
    </div>
  );
};
