import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { loadDetailEspecie, setDetailEspecie } from "../../redux/actions";
import { InputTextClientsWallet } from "../../components/inputTextClientsWallet/inputTextClientsWallet";
import { SelectClientsWallet } from "../../components/selectClientsWallet/selectClientsWallet";
import { DetailFundEspecie } from "../../components/detailFundEspecie/detailFundEspecie";
import { OperateDetailEspecie2 } from "../../components/operateDetailEspecie2/operateDetailEspecie2";
import { Divider } from "../../components/divider/divider";
import { termAsociated } from "../../libs/libsMarket";
import { optionsTicker, optionsClase, options } from "../../constants/market";
import iconSetting from "../../utils/icon-analytics333.svg";
import iconFilter from "../../utils/icon-filtrar.svg";
import ArrowDown from "../../utils/Trazado 26.svg";
import iconGroup from "../../utils/icon-grupo.svg";
import "./marketDetailCard.css";

export const MarketDetailCard = ({
  listTerms,
  selectedCardMarket,
  selectedRadio
}) => {
  const state = useSelector(state => state);
  const {
    fundsData,
    bondsData,
    cedearsData,
    equityData,
    loadingDetailEspecie,
    detailEspecie
  } = state;

  const [ticker, setTicker] = useState("");
  const onChange = e => setTicker(e.target.value);

  const [openTicker, setOpenTicker] = useState(false);
  const [openClase, setOpenClase] = useState(false);
  const [selection, setSelection] = useState({ value: -1, description: "" });

  const filteredClients = () => {
    let resultList;
    if (selectedCardMarket.title === "Fondos") {
      resultList = fundsData;
    }
    if (selectedCardMarket.title === "Bonos") {
      resultList = bondsData;
    }
    if (selectedCardMarket.title === "Cedears") {
      resultList = cedearsData;
    }
    if (selectedCardMarket.title === "Acciones") {
      resultList = equityData;
    }

    if (ticker === "") {
      return resultList.filter(element =>
        termAsociated(listTerms, element, selectedRadio)
      );
    } else {
      return resultList
        .filter(element => termAsociated(listTerms, element, selectedRadio))
        .filter(element => element.ticker.toLowerCase().includes(ticker));
    }
  };

  return (
    <div className="market-container-instruments">
      <div className="market-container-instruments-data-table">
        <div className="market-container-instruments-data-table-header">
          <div className="market-container-instruments-data-table-header-left">
            {selectedCardMarket.title}
          </div>
          <div
            className="market-container-instruments-data-table-header-right"
            style={{ float: "right" }}
          >
            <InputTextClientsWallet
              type="text"
              name="searchField"
              placeholder="Escriba aqui..."
              value={ticker}
              onChange={onChange}
              inputSize="input--medium"
            />
            <Divider name="X" />
            <SelectClientsWallet
              icon={iconFilter}
              title="Especie"
              options={options}
              selection={selection}
              setSelection={setSelection}
            />
            <Divider name="X" />
            <SelectClientsWallet
              icon={iconGroup}
              title=""
              options={options}
              selection={selection}
              setSelection={setSelection}
            />
          </div>
        </div>
        <div className="market-container-instruments-data-table-line"></div>
        <Divider name="XXS" />
        <div className="market-container-instruments-data-table-column-header">
          {selectedCardMarket.title === "Fondos" ? (
            <>
              <div className="fondo-ticker">TICKER</div>
              <div
                className="fondo-clase"
                onClick={() => setOpenClase(x => !x)}
              >
                <img src={iconFilter} alt="..." />
                <Divider name="XXS" />
                <div>CLASE</div>
                <Divider name="XXS" />
                <img src={ArrowDown} alt="..." />
              </div>
              <div className="fondo-ultimo">ULTIMO</div>
              <div className="fondo-percDia">%DIA</div>
              <div className="fondo-percMes">%MES</div>
              <div className="fondo-pyty">%YTY</div>
              <div className="fondo-pytd">%YTD</div>
              <div className="fondo-moneda">MONEDA</div>
              <div className="fondo-invMinima">INV. MÍNIMA</div>
            </>
          ) : (
            <>
              <div className="ticker" onClick={() => setOpenTicker(x => !x)}>
                <img src={iconFilter} alt="..." />
                <Divider name="XXS" />
                <div>TICKER</div>
                <Divider name="XXS" />
                <img src={ArrowDown} alt="..." />
              </div>
              <div className="ultimo">ULTIMO</div>
              <div className="porcCambio">% DE CAMBIO</div>
              <div className="cCompra">C. COMPRA</div>
              <div className="pCompra">P. COMPRA</div>
              <div className="pVenta">P. VENTA</div>
              <div className="cVenta">C. VENTA</div>
              <div className="volNom">VOL. NOM</div>
              <div className="pCierre">P. CIERRE</div>
            </>
          )}
          <div
            className={`ticker-container-search ${
              openTicker ? "open-ticker" : ""
            }`}
          >
            {optionsTicker.map(x => {
              return (
                <div key={x.value} className="element">
                  {x.description}
                </div>
              );
            })}
          </div>
          <div
            className={`clase-container-search ${
              openClase ? "open-clase" : ""
            }`}
          >
            {optionsClase.map(x => {
              return (
                <div key={x.value} className="element">
                  {x.description}
                </div>
              );
            })}
          </div>
        </div>
        <div className="market-container-instruments-data-table-users">
          {filteredClients().map((especie, i) => (
            <Especie key={i} element={especie} />
          ))}
        </div>
      </div>
      <Divider name="M" />

      {detailEspecie && (
        <div className="market-container-instruments-data-user">
          <div className="header">
            <div className="info">
              <div className="iconSetting">
                <img src={iconSetting} alt="..." />
              </div>
              <div className="details">
                <div>15 PERSONAS LO TIENEN</div>
                <div className="letra-chica">Primer oración descriptiva</div>
              </div>
            </div>
            <div className="operate-instrument">Operar instrumento</div>
          </div>
          <Divider name="S" />
          {loadingDetailEspecie ? (
            "Cargando..."
          ) : detailEspecie.type === "FUND" ? (
            <DetailFundEspecie detailEspecie={detailEspecie} />
          ) : (
            <OperateDetailEspecie2 detailEspecie={detailEspecie} />
          )}
        </div>
      )}
    </div>
  );
};

const Especie = ({ element }) => {
  const dispatch = useDispatch();

  return (
    <div
      className="market-container-instruments-data-table-user-detail"
      onClick={async () => {
        dispatch(loadDetailEspecie());
        await fetch(
          "https://intranet.bnzlab.com/wp-json/get/detalle_instrumento/" +
            element.code
        )
          .then(res => res.json())
          .then(response => {
            console.log(response);
            dispatch(setDetailEspecie(response));
          })
          .catch(err => console.log(err));
      }}
    >
      <div className="name">{element.ticker}</div>
      <div className="name">{element.code}</div>
      <div className="name">{element.market}</div>
    </div>
  );
};
