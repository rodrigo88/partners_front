import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import starIcon from "../../utils/icon-home-star.svg";
import agressiveIcon from "../../utils/icon-home-agresivo.svg";
import moderateIcon from "../../utils/icon-home-moderado.svg";
import conservativeIcon from "../../utils/icon-home-conservador.svg";
import icon85 from "../../utils/85.svg";
// import iconUpArrow from "../../utils/UpArrow1.svg";
import icon138 from "../../utils/138.svg";
import agressiveImage from "../../utils/agressiveIcon.png";
import moderateImage from "../../utils/moderateIcon.png";
import conservativeImage from "../../utils/conservativeIcon.png";
import { RecommendedWallet } from "../../components/recommendedWallet/recommendedWallet";

import "./recommendedWallets.css";

export const RecommendedWallets = () => {
  // const dispatch = useDispatch();
  const state = useSelector(state => state);

  const { loadingRecommendedWallets, recommendedWallets } = state;

  const [cardProfile, setCardProfile] = useState("");

  const setTypeProfile = type => {
    setCardProfile(type);
  };

  const stars = [1, 2, 3, 4, 5];

  return (
    <div className="recommendedWallets-card">
      {loadingRecommendedWallets ? (
        <LoadinView />
      ) : (
        <>
          <RecommendedWallet
            stars={stars}
            starIcon={starIcon}
            icon={agressiveIcon}
            typeProfile="Perfil Agresivo"
            text="Carteras recomendadas"
            icon085={icon85}
            icon1308={icon138}
            mainImage={agressiveImage}
            showInfo="Ampliar info"
            data={recommendedWallets.agresiva}
          />
          <RecommendedWallet
            stars={stars}
            starIcon={starIcon}
            icon={moderateIcon}
            typeProfile="Perfil Morderado"
            text="Carteras recomendadas"
            icon085={icon85}
            icon1308={icon138}
            mainImage={moderateImage}
            showInfo="Ampliar info"
            data={recommendedWallets.moderada}
          />
          <RecommendedWallet
            stars={stars}
            starIcon={starIcon}
            icon={conservativeIcon}
            typeProfile="Perfil Conservador"
            text="Carteras recomendadas"
            icon085={icon85}
            icon1308={icon138}
            mainImage={conservativeImage}
            showInfo="Ampliar info"
            data={recommendedWallets.conservadora}
          />
        </>
      )}
    </div>
  );
};

const LoadinView = () => (
  <div className="lds-roller">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>
);
