import React, { useState } from "react";
import { useSelector } from "react-redux";
import { TreasuryHeader } from "../../components/treasuryHeader/treasuryHeader";
import { userBalanceList } from "../../libs/libsTreasury";
import { TreasuryHeaderData } from "../../components/treasuryHeaderData/treasuryHeaderData";
import { TreasuryTabs } from "../../components/treasuryTabs/treasuryTabs";
import { TreasurySelections } from "../../components/treasurySelections/treasurySelections";
import { TreasuryData } from "../../components/treasuryData/treasuryData";
import { Divider } from "../../components/divider/divider";
import { TreasuryModalProof } from "../../components/treasuryModalProof/treasuryModalProof";
import { TreasuryModalExtract } from "../../components/treasuryModalExtract/treasuryModalExtract";
import iconBussiness from "../../utils/business-and-finance.svg";
import iconMoney from "../../utils/money.svg";
import "./treasury.css";

export const Treasury = () => {
  const state = useSelector(state => state);
  const { userBalance, treasuryData } = state;

  const [option, setOption] = useState("TOTAL");
  const [search, setSearch] = useState({ cuit: "", name: "" });

  const [openModalProof, setOpenModalProof] = useState(false);
  const [openModalExtract, setOpenModalExtract] = useState(false);

  const onChange = e =>
    setSearch({ ...search, [e.target.name]: e.target.value });

  const arrayColoursARS = ["#3A9DDD", "#D6CD07", "#EB760E"];
  const arrayColoursUSD = ["#2DA991", "#D6CD07", "#EB760E"];

  return (
    <div className="treasury-dashboard-container">
      <TreasuryHeader />
      <Divider name="M" />
      <div className="treasury-section">
        <TreasuryPie dataList={userBalanceList(userBalance, arrayColoursARS)} />
        <Divider name="X" />
        <TreasuryPie dataList={userBalanceList(userBalance, arrayColoursUSD)} />
        <Divider name="X" />
        <div className="buttons">
          <TreasuryMainButton
            icon={iconBussiness}
            title="INGRESAR COMPROBANTES"
            type="INGRESAR"
            onClick={() => setOpenModalProof(true)}
          />
          <Divider name="S" />
          <TreasuryMainButton
            icon={iconMoney}
            title="EXTRACCIÓN DE DINERO"
            type="EXTRACCION"
            onClick={() => setOpenModalExtract(true)}
          />
        </div>
      </div>
      <div className="table">
        <TreasuryTabs option={option} setOption={setOption} />
        <TreasurySelections search={search} onChange={onChange} />
        <TreasuryHeaderData option={option} />
        <TreasuryData treasuryData={treasuryData} search={search} />
      </div>
      <TreasuryModalProof
        openModal={openModalProof}
        closeModal={() => setOpenModalProof(false)}
      />
      <TreasuryModalExtract
        openModal={openModalExtract}
        closeModal={() => setOpenModalExtract(false)}
      />
    </div>
  );
};

/* ********************************************************************************** */
export const TreasuryPie = ({ dataList }) => {
  return (
    <div className="pie">
      <div className="header">
        <div className="title">Mi Saldo en Pesos ARS</div>
        <div className="line" />
      </div>
      <Divider name="S" />
      <div className="statics">
        <PiePercentageStatics dataList={dataList} />
        <ListPercentageStatics currency="ARS" dataList={dataList} />
      </div>
    </div>
  );
};

/* ********************************************************************************** */
export const TreasuryMainButton = ({ icon, title, type, onClick }) => {
  return (
    <div
      className={`container ${type === "INGRESAR" ? "ing" : "ext"}`}
      onClick={onClick}
    >
      <img className="icon" src={icon} alt="..." />
      <div className="title">{title}</div>
    </div>
  );
};

/* ********************************************************************************** */
export const PiePercentageStatics = ({ dataList }) => {
  const initialPosPercentage = id => {
    const percArray = dataList.filter(element => element.id < id);
    const sum = percArray.reduce((acc, x) => acc + x.percentage, 0);
    const result = (sum * 180) / 50;
    return { sum, result };
  };

  return (
    <div className="treasury-pie-main-container">
      <div className="treasury-my-circle-container">
        {dataList.map(({ id, color, name, percentage }) => {
          return (
            <div
              key={id}
              className="treasury-perc-element"
              style={{
                zIndex: id === dataList.length ? 0 : id,
                transform: `rotate(${initialPosPercentage(id).result}deg)`,
                background: `conic-gradient(${color} ${percentage}%, transparent ${percentage}% 100%)`
              }}
            ></div>
          );
        })}
        <div className={`treasury-corona-interna`}></div>
      </div>
    </div>
  );
};

/* ********************************************************************************** */
export const ListPercentageStatics = ({ dataList, currency }) => {
  const totalBalance = () =>
    dataList.reduce((acc, x) => acc + parseFloat(x.value), 0);
  return (
    <div className="treasury-cartera-list">
      {dataList.map(x => {
        return (
          <div className="element" key={x.id}>
            <div className="point-icon" style={{ background: x.color }} />
            <Divider name="S" />
            <div className="description">{x.name}</div>
            <div className="percentage">
              {currency} {x.value}
            </div>
          </div>
        );
      })}
      <div className="line" />
      <div className="total">
        <div className="title">Saldo Total</div>
        <div className="value">
          {currency} {totalBalance()}
        </div>
      </div>
    </div>
  );
};
