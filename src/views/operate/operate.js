import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { loadDetailEspecie, setDetailEspecie } from "../../redux/actions";
import { Divider } from "../../components/divider/divider";
import { OperateHeader } from "../../components/operateHeader/operateHeader";
import { OperateTabs } from "../../components/operateTabs/operateTabs";
import { OperateSubTabs } from "../../components/operateSubTabs/operateSubTabs";
import { OperateMainCardView } from "../../components/operateMainCardViews/operateMainCardViews";
import { OperateTable } from "../../components/operateTable/operateTable";
import { OperateTableHeader } from "../../components/operateTableHeader/operateTableHeader";
import { OperateFooter } from "../../components/operateFooter/operateFooter";
import { OperateDetailEspecie } from "../../components/operateDetailEspecie/operateDetailEspecie";
import { OperateConfirmProcedure } from "../../components/operateConfirmProcedure/operateConfirmProcedure";
import { OperateDetailProcess } from "../../components/operateDetailProcess/operateDetailProcess";
import { OperateProcessOrders } from "../../components/operateProcessOrders/operateProcessOrders";
import { OperateTableEditGroup } from "../../components/operateTableEditGroup/operateTableEditGroup";
import { OperateModalFinish } from "../../components/operateModalFinish/operateModalFinish";
import {
  userWalletsTable,
  setCheckList,
  checkElement,
  unCheckElement,
  updatePriceElement,
  updateQuantityElement
} from "../../libs/libsOperate";
import "./operate.css";
import { OperateScheduleView } from "../../components/operateScheduleView/operateScheduleView";

export const Operate = () => {
  const state = useSelector(state => state);
  const dispatch = useDispatch();
  const { userWalletClients2, loadingUserWalletClients2 } = state;

  const [resultConfirm, setResultConfirm] = useState(null);

  const [openModalFinish, setOpenModalFinish] = useState(false);

  // const [dataTable, setDataTable] = useState(() =>
  //   userWalletsTable(userWalletClients)
  // );
  const [dataTable, setDataTable] = useState(
    loadingUserWalletClients2 ? [] : userWalletClients2
  );

  const [selectedInstrument, setSelectedInstrument] = useState({
    value: -1,
    description: ""
  });

  const [optionItem, setOptionItem] = useState("OPERAR");
  const [subOptionItem, setSubOptionItem] = useState(
    selectedInstrument.description === "Fondos" ? "SUSCRIBIR" : "COMPRAR"
  );

  const [search, setSearch] = useState({
    cuit: "",
    comitente: "",
    name: ""
  });

  const [checkAll, setCheckAll] = useState(false);

  const setSelectedCheck = element => {
    const { id, checked } = element;
    if (checked) {
      setDataTable(dataTable.map(x => (x.id === id ? unCheckElement(x) : x)));
      setCheckAll(false);
    } else {
      setDataTable(dataTable.map(x => (x.id === id ? checkElement(x) : x)));
    }
  };

  const selectAllListElement = () => {
    if (checkAll) {
      setDataTable(dataTable.map(element => unCheckElement(element)));
      setCheckAll(false);
    } else {
      setDataTable(dataTable.map(element => checkElement(element)));
      setCheckAll(true);
    }
  };

  const [selectedRadio, setSelectedRadio] = useState("Contado Inmediato");

  const [listTerms, setListTerms] = useState([
    {
      id: 1,
      term: "Contado Inmediato",
      checked: false
    },
    {
      id: 2,
      term: "24 hs",
      checked: false
    },
    {
      id: 3,
      term: "48 hs",
      checked: false
    },
    {
      id: 4,
      term: "Rescate total",
      checked: false
    },
    {
      id: 5,
      term: "Rescate parcial",
      checked: false
    }
  ]);

  const selectTerm = radio => {
    setSelectedRadio(radio.term);
    setListTerms(setCheckList(radio, listTerms));
  };

  const onChange = e =>
    setSearch({ ...search, [e.target.name]: e.target.value });

  const [selectedGroup, setSelectedGroup] = useState({
    value: -1,
    description: ""
  });

  const [selectedEspecie, setSelectedEspecie] = useState({
    value: -1,
    description: ""
  });

  const [selectedEspecieToShow, setSelectedEspecieToShow] = useState(null);

  const [selectedWallet, setSelectedWallet] = useState({
    value: -1,
    description: ""
  });

  const [activeDetailCard, setActiveDetailCard] = useState(false);

  const [activeDetailProcess, setActiveDetailProcess] = useState(false);

  const [quantityOperateTable, setQuantityOperateTable] = useState("");
  const onChangeQuantity = e => {
    setQuantityOperateTable(e.target.value);
    setDataTable(dataTable.map(x => updateQuantityElement(x, e.target.value)));
  };

  const [priceOperateTable, setPriceOperateTable] = useState("");
  const onChangePrice = e => {
    setPriceOperateTable(e.target.value);
    setDataTable(dataTable.map(x => updatePriceElement(x, e.target.value)));
  };

  const onChangePriceRow = (event, index) => {
    const list = dataTable.map((element, i) => {
      if (i === index) {
        return updatePriceElement(element, parseInt(event.target.value));
      } else {
        return element;
      }
    });
    setDataTable(list);
  };

  const [openConfirmProcedure, setOpenConfirmProcedure] = useState(false);

  const [selectedPendingProcess, setSelectedPendingProcess] = useState(null);

  const confirmProcedure = () => setOpenConfirmProcedure(true);

  const cancelProcedure = () => setOpenConfirmProcedure(false);

  const closeActiveDetailCard = () => setActiveDetailCard(false);

  const chooseInstrument = inst => {
    const { description } = inst;
    if (description === "Fondos") {
      setSubOptionItem("SUSCRIBIR");
    } else {
      setSubOptionItem("COMPRAR");
    }
    setSelectedInstrument(inst);
    setSelectedEspecieToShow(null);
    setSelectedEspecie({
      value: -1,
      description: ""
    });
  };

  const chooseEspecie = async especie => {
    console.log("SELECTION: ", especie);
    setSelectedEspecieToShow(null);
    setSelectedEspecie(especie);
    // setSelectedEspecie(null);
    dispatch(loadDetailEspecie());
    await fetch(
      "https://intranet.bnzlab.com/wp-json/get/detalle_instrumento/" +
        especie.code
    )
      .then(res => res.json())
      .then(response => {
        dispatch(setDetailEspecie(response));
        setSelectedEspecieToShow(response);
      })
      // .then(response => setSelectedEspecie(response))
      // .then(response => dispatch(setDetailEspecie(response)))
      .catch(err => console.log(err));
  };

  const activeDetailPendientesElement = pendingProcess => {
    setSelectedPendingProcess(pendingProcess);
    setActiveDetailProcess(true);
  };

  const handleDetailProcess = process => {
    if (selectedPendingProcess) {
      if (selectedPendingProcess.detalle === process.detalle) {
        setSelectedPendingProcess(null);
        setActiveDetailProcess(false);
      }
    }
  };

  const chooseRecommendedWallet = opt => {
    setSelectedEspecieToShow(null);
    setSelectedEspecie({ value: -1, description: "" });
    setSelectedWallet(opt);
    setActiveDetailCard(true);
    setSubOptionItem("SUSCRIBIR");
    setSelectedInstrument({ value: 2, description: "Fondos" });
  };

  // React.useEffect(() => {
  //   async function getUserWallets() {
  //     setSelectedEspecie({ value: -1, description: "" });
  //     setSelectedInstrument({ value: -1, description: "" });
  //     setSelectedWallet({ value: -1, description: "" });
  //     dispatch(setDetailEspecie({ value: -1, description: "" }));
  //   }
  //   getUserWallets();
  //   return () => {
  //     dispatch(setDetailEspecie({ value: -1, description: "" }));
  //     setSelectedEspecie({ value: -1, description: "" });
  //     setSelectedInstrument({ value: -1, description: "" });
  //     setSelectedWallet({ value: -1, description: "" });
  //   };
  // }, []);

  const dataToSendTable = () => {
    if (search.name === "") {
      return dataTable;
    } else {
      return dataTable.filter(dataElement =>
        dataElement.fullName.toLowerCase().includes(search.name.toLowerCase())
      );
    }
  };

  const allowed = dataToSendTable().filter(x => x.checked);
  const totalPrice = allowed.reduce((acc, x) => acc + parseInt(x.price), 0);
  const totalQuantity = allowed.reduce(
    (acc, x) => acc + parseInt(x.quantity),
    0
  );

  const openModalFinishFunction = (inst, legend) => {
    setResultConfirm({ instrument: inst, legend: legend });
    setOpenModalFinish(true);
  };

  return (
    <div className="operate-dashboard-container">
      <OperateHeader />
      <OperateTabs
        optionItem={optionItem}
        setOptionItem={setOptionItem}
        closeActiveDetailCard={closeActiveDetailCard}
      />
      <DividerLine />
      <Divider name="M" />

      {optionItem === "OPERAR" ? (
        <>
          <OperateSubTabs
            selectedInstrument={selectedInstrument}
            subOptionItem={subOptionItem}
            setSubOptionItem={setSubOptionItem}
          />
          <OperateMainCardView
            onChange={onChange}
            listTerms={listTerms}
            selectedGroup={selectedGroup}
            setSelectedGroup={setSelectedGroup}
            selectedInstrument={selectedInstrument}
            setSelectedInstrument={chooseInstrument}
            selectTerm={selectTerm}
            subOptionItem={subOptionItem}
            selectedEspecie={selectedEspecie}
            selectedEspecieGlobal={state.selectedEspecie}
            // setSelectedEspecie={setSelectedEspecie}
            setSelectedEspecie={chooseEspecie}
            selectedWallet={selectedWallet}
            setSelectedWallet={chooseRecommendedWallet}
            search={search}
          />

          <OperateTableHeader
            dataTable={dataTable}
            checkAll={checkAll}
            selectAllListElement={selectAllListElement}
            selectedInstrument={selectedInstrument}
          />
          <OperateTableEditGroup
            quantityOperateTable={quantityOperateTable}
            priceOperateTable={priceOperateTable}
            onChangeQuantity={onChangeQuantity}
            onChangePrice={onChangePrice}
          />
          {!openConfirmProcedure && (
            <>
              <OperateTable
                // dataTable={dataTable}
                dataTable={dataToSendTable()}
                setSelectedCheck={setSelectedCheck}
                onChangePriceRow={onChangePriceRow}
                priceOperateTable={priceOperateTable}
              />
              <OperateFooter
                dataTable={dataTable}
                selectedInstrument={selectedInstrument}
                selectedEspecie={selectedEspecie}
                selectedWallet={selectedWallet}
                totalPrice={totalPrice}
                totalQuantity={totalQuantity}
                confirmProcedure={confirmProcedure}
              />
            </>
          )}
        </>
      ) : (
        <OperateProcessOrders
          activeDetailPendientesElement={activeDetailPendientesElement}
          handleDetailProcess={handleDetailProcess}
        />
      )}

      {/* VENTANAS EMERGENTES !!! */}
      {optionItem === "OPERAR" && (
        <>
          <OperateDetailEspecie
            selectedInstrument={selectedInstrument}
            activeDetailCard={activeDetailCard}
            selectedEspecie={selectedEspecie}
            selectedEspecieToShow={selectedEspecieToShow}
            selectedWallet={selectedWallet}
            selectedEspecieGlobal={state.selectedEspecie}
          />
        </>
      )}

      {optionItem === "ORDENES EN PROCESO" && (
        <OperateDetailProcess
          activeDetailProcess={activeDetailProcess}
          selectedPendingProcess={selectedPendingProcess}
        />
      )}

      <OperateConfirmProcedure
        selectedInstrument={selectedInstrument}
        dataTable={dataTable.filter(x => x.checked)}
        openConfirmProcedure={openConfirmProcedure}
        cancelProcedure={cancelProcedure}
        resultConfirm={resultConfirm}
        // openModalFinish={() => setOpenModalFinish(true)}
        openModalFinish={openModalFinishFunction}
      />

      <OperateModalFinish
        resultConfirm={resultConfirm}
        openModal={openModalFinish}
        closeModal={() => setOpenModalFinish(false)}
      />
    </div>
  );
};
/* ************************************************************ */
const DividerLine = () => (
  <div
    style={{
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      justifyContent: "flex-start",
      width: "97.2%",
      height: "1px",
      background: "#A4A7BE"
    }}
    alt="..."
  ></div>
);
