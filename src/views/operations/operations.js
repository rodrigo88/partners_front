import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Subtitle } from "../../components/titles/titles";
import { CardOperation } from "../../components/cardOperation/cardOperation";
import { StaticsOperation } from "../../components/staticsOperation/staticsOperation";
// import portIcon from "../../utils/Grupo 3334.svg";
import opeationsIcon from "../../utils/icon-home-operaciones.svg";
import questionIcon from "../../utils/Grupo 3407.svg";
import { Divider } from "../../components/divider/divider";
import "./operations.css";

export const Operations = () => {
  const state = useSelector(state => state);
  const { userOperations, loadingUserOperations } = state;

  const [dma, setDma] = useState(true);
  const [multi, setMulti] = useState(false);

  const onChangeDma = e => {
    setDma(true);
    setMulti(false);
  };

  const onChangeMulti = e => {
    setMulti(true);
    setDma(false);
  };

  return (
    <div className="operations-card">
      {/* ******************************************** */}
      <div className="operations-card-header">
        <div className="operations-header-title">
          <img src={opeationsIcon} alt="..." style={{ marginRight: "15px" }} />
          <Subtitle
            text="Operaciones recientes"
            type="main"
            style={{ fontWeight: "bolder" }}
          />
        </div>
        <div className="operations-header-selection">
          {!loadingUserOperations && (
            <>
              <form>
                <input
                  type="radio"
                  id="dma"
                  name="dma"
                  value={dma}
                  checked={dma}
                  onChange={onChangeDma}
                />
                <label htmlFor="dma">DMA</label>
                <Divider name="XL" />
                <input
                  type="radio"
                  id="multi"
                  name="multi"
                  value={multi}
                  checked={multi}
                  onChange={onChangeMulti}
                />
                <label htmlFor="multi">Multimercado</label>
              </form>

              <img src={questionIcon} alt="..." />
            </>
          )}
        </div>
      </div>
      {/* ******************************************** */}
      {loadingUserOperations ? (
        <LoadinView />
      ) : (
        <div className="operations-content">
          <div className="operations-content-cards">
            <CardOperation
              classCardOperation="nuevas-cuentas"
              description="Nuevas cuentas"
              value={userOperations.newAccounts}
            />
            <CardOperation
              classCardOperation="operaciones-del-dia"
              description="Operaciones del dia"
              value={userOperations.ordersOfToday}
            />
            <CardOperation
              classCardOperation="total-de-cuentas"
              description="Total de cuentas"
              value={userOperations.totalAccounts}
            />
            <CardOperation
              classCardOperation="activation-rate"
              description="Activation rate"
              value={userOperations.activationRate}
            />
          </div>
          <div className="operations-content-statics">
            <StaticsOperation nextIncome={userOperations.nextIncome} />
          </div>
        </div>
      )}
    </div>
  );
};

const LoadinView = () => (
  <div className="lds-roller">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>
);
