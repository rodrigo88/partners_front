import React, { useState } from "react";
import { Button } from "../../components/button/button";
import { InputText } from "../../components/inputText/inputText";
import { Subtitle, Paragraph } from "../../components/titles/titles";
import { Link } from "react-router-dom";
import { setAuth, setLogIn } from "../../redux/actions";
import { useDispatch } from "react-redux";
import { Auth } from "aws-amplify";
import { Divider } from "../../components/divider/divider";
import "./signin.css";

export const Signin = () => {
  const dispatch = useDispatch();

  const initialState = { username: "", password: "" };

  const [user, setUser] = useState(initialState);

  const onChange = e => setUser({ ...user, [e.target.name]: e.target.value });

  const onSubmit = async e => {
    e.preventDefault();
    if (incompleteData(user)) {
      console.log("Complete Your Data Please.");
    } else {
      await Auth.signIn({
        username: user.username,
        password: user.password
      })
        .then(response => {
          console.log("LA RESPUESTA ES: ", response);
          dispatch(setLogIn());
        })
        .catch(err => console.log("HUBO UN ERROR: ", err));
      console.log(user);
      setUser(initialState);
    }
    // dispatch(setAuth());
  };

  const incompleteData = ({ username, password }) =>
    username.length === 0 || password.length === 0;

  return (
    <div className="signin">
      <Subtitle
        type="main"
        text="Por favor, ingresa el usuario y contraseña que te brindaron"
      />
      <Divider name="XXL" />
      <form onSubmit={onSubmit} className="form-signin">
        <InputText
          type="text"
          name="username"
          placeholder="Escriba nombre de usuario"
          value={user.username}
          onChange={onChange}
          inputSize="input--large"
        />
        <Divider name="XXL" />
        <InputText
          type="password"
          name="password"
          placeholder="Password"
          value={user.password}
          onChange={onChange}
          inputSize="input--large"
        />
        <Divider name="X" />
        <Button
          buttonStyle="btn--primary"
          buttonSize="btn--medium"
          type="submit"
        >
          LOGIN
        </Button>
      </form>
      <Divider name="X" />
      <div className="forget-links">
        <div>
          <Link className="no-underline" to={"/recover-user"}>
            <Paragraph text="Olvidé mi usuario" type="link" />
          </Link>
        </div>
        <div>
          <Link className="no-underline" to={"/recover-password"}>
            <Paragraph text="Olvidé mi contraseña" type="link" />
          </Link>
        </div>
      </div>
      <Divider name="XXXXL" />
    </div>
  );
};
