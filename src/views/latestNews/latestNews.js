import React from "react";
import { useSelector } from "react-redux";
// import { latestNews } from "../../constants/constants";
import { Subtitle } from "../../components/titles/titles";
import { Divider } from "../../components/divider/divider";
// import { LatestNew } from "../../components/latestNew/latestNew";
import iconHomeCalendar from "../../utils/icon-home-calendar.svg";
import iconHomeNewsMarket from "../../utils/Enmascarar grupo 1@2x.png";
import iconHomeNewsPayment from "../../utils/icon-home-news-payment.svg";
import iconHomeNewsBirthday from "../../utils/icon-home-news-birthday.svg";
import iconHomeNewsAlert from "../../utils/icon-home-alert.svg";
import iconRightArrow from "../../utils/right-arrow.svg";
import "./latestNews.css";

export const LatestNews = () => {
  const state = useSelector(state => state);
  const { latestNews } = state;

  const news = latestNews.filter(ln => ln.type === "new");
  const events = latestNews.filter(ln => ln.type !== "new");

  const [scrollPosition] = React.useState(0);

  React.useEffect(() => {
    // scrollPosition < 2000 &&
    //   setTimeout(() => {
    //     setScrollPosition(x => x + 1);
    //     const scrollContainer = document.getElementById(
    //       "latestNews-content-news"
    //     );
    //     scrollContainer.scrollLeft = scrollPosition * 50;
    //   }, 1000);
    // if (scrollPosition < 2000) {
    //   function handleAutoScroll() {
    //     setScrollPosition(x => x + 1);
    //     const scrollContainer = document.getElementById(
    //       "latestNews-content-news"
    //     );
    //     scrollContainer.scrollLeft = scrollPosition * 2;
    //     console.log(scrollContainer.scrollLeft);
    //   }
    //   handleAutoScroll();
    //   window.addEventListener("scroll", handleAutoScroll);
    //   return () => window.removeEventListener("scroll", handleAutoScroll);
    // }
  }, [scrollPosition]);

  return (
    <div className="latestNews-container">
      <div className="latestNews-header">
        <div className="latestNews-header-title">
          <img
            src={iconHomeCalendar}
            alt="..."
            style={{ marginRight: "15px" }}
          />
          <Subtitle text="Ultimas noticias y eventos" type="main" />
        </div>
        <div className="latestNews-header-info">
          <img src={iconRightArrow} alt="..." />
          <Divider name="XS" />
          <div>Ampliar información</div>
        </div>
      </div>

      <div className="latestNews-banner">Future Banner</div>

      <Divider name="S" />

      {/* ********************************************************* */}
      <div className="latestNews-content-news" id="latestNews-content-news">
        {news.map(newNotice => (
          <div key={newNotice.id} className="latestNews-content-news-element">
            <img
              className="element-image"
              src={iconHomeNewsMarket}
              alt="..."
              style={{ height: "46px", width: "46px" }}
            />
            <Divider name="S" />
            <div className="element-info">
              <div className="element-info-title">{newNotice.title}</div>
              <div className="element-info-content">{newNotice.content}</div>
            </div>
          </div>
        ))}
      </div>
      {/* ********************************************************* */}
      <Divider name="S" />

      <div className="latestNews-content-events">
        {events.map(newNotice => (
          <div key={newNotice.id} className="latestNews-content-events-element">
            <img
              className="element-image"
              alt="..."
              style={{ height: "46px", width: "46px" }}
              src={
                newNotice.type === "payment"
                  ? iconHomeNewsPayment
                  : newNotice.type === "birth"
                  ? iconHomeNewsBirthday
                  : iconHomeNewsAlert
              }
            />
            <Divider name="S" />
            <div className="element-info">
              <div className="element-info-title">{newNotice.title}</div>
              <div className="element-info-content">{newNotice.content}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
