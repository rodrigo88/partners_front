import React, { useEffect } from "react";
import { items } from "../../constants/constants";
import { userWalletsTable } from "../../libs/libsOperate";
import {
  clients,
  movements,
  // userLogged,
  latestNews,
  listMarketIndex,
  cardsMarket
} from "../../constants/constants";
import { userBalance, treasuryData } from "../../constants/treasury";
import { teamsPartner } from "../../constants/team";
import {
  setRoutes,
  setSelectedRoute,
  loadClients,
  setClients,
  loadMovements,
  setMovements,
  loadUserData,
  setUserData,
  loadLatestNews,
  setLatestNews,
  loadListMarketIndex,
  setListMarketIndex,
  loadMarketCards,
  setMarketCards,
  loadUserInformation,
  setUserInformation,
  loadUserOperations,
  setUserOperations,
  loadUserWalletClients,
  setUserWalletClients,
  loadUserWalletClients2,
  setUserWalletClients2,
  loadRecommendedWallets,
  setRecommendedWallets,
  // loadProvincesData,
  // setProvincesData,
  loadCoinPricesData,
  setCoinPricesData,
  loadCoinMervalData,
  setCoinMervalData,
  loadFundsData,
  setFundsData,
  loadBondsData,
  setBondsData,
  loadCedearsData,
  setCedearsData,
  loadEquityData,
  setEquityData,
  loadUserBalance,
  setUserBalance,
  loadTreasuryData,
  setTreasuryData,
  loadTeamsPartner,
  setTeamsPartner
} from "../../redux/actions";
import { useSelector, useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import { Navbar } from "../../components/navbar/navbar";
import { Sidebar } from "../../components/sidebar/sidebar";
import { MainViews } from "../mainViews/mainViews";
import { getDataPartnerById } from "../../apiCalls/portfolioApiCalls";
import { getRecommendedWallets } from "../../apiCalls/recommendedWalletsApiCalls";
import { getTeamPartnerById } from "../../apiCalls/teamApiCalls";
// import { getProvincesDataApi } from "../../apiCalls/provincesApiCalls";
import { getMarketInfo } from "../../apiCalls/marketApiCalls";
import "./dashboard.css";

export const Dashboard = () => {
  const state = useSelector(state => state);
  const { routes, expanded, openBlur } = state;

  const dispatch = useDispatch();
  const { pathname } = useLocation();

  useEffect(() => {
    /* **************************************************************************** */
    let user = {};
    dispatch(loadUserInformation());
    const getDataUserById = async () => {
      const resData = await getDataPartnerById(
        " https://djufzxp9v1.execute-api.us-east-1.amazonaws.com/bi-dev/api_getpartnerportfolio_stg",
        83
      );
      resData && dispatch(setUserInformation(resData));
    };

    getDataUserById();
    /* **************************************************************************** */
    // let userOperations = {};
    dispatch(loadUserOperations());
    const getDataUserOperationsById = async () => {
      const resDataOperations = await getDataPartnerById(
        "https://djufzxp9v1.execute-api.us-east-1.amazonaws.com/bi-dev/api_getpartner-recent-operations_stg",
        83
      );

      dispatch(setUserOperations(resDataOperations));
    };

    getDataUserOperationsById();
    /* **************************************************************************** */
    let recommendedWallets = {};
    dispatch(loadRecommendedWallets());

    const getDataRecommendedWallets = async () => {
      const resDataAgresiva = await getRecommendedWallets(
        "https://apibroker.bnzlab.com/v1/portfolio/recommended/agresiva"
      );

      recommendedWallets.agresiva = resDataAgresiva[0];

      const resDataModerada = await getRecommendedWallets(
        "https://apibroker.bnzlab.com/v1/portfolio/recommended/moderada"
      );

      recommendedWallets.moderada = resDataModerada[0];

      const resDataConservadora = await getRecommendedWallets(
        "https://apibroker.bnzlab.com/v1/portfolio/recommended/conservadora"
      );

      recommendedWallets.conservadora = resDataConservadora[0];

      recommendedWallets.agresiva &&
        recommendedWallets.moderada &&
        recommendedWallets.conservadora &&
        dispatch(setRecommendedWallets(recommendedWallets));
    };

    getDataRecommendedWallets();

    /* **************************************************************************** */
    dispatch(loadUserWalletClients());
    dispatch(loadUserWalletClients2());
    const getDataUserWalletClientsById = async () => {
      const resDataClientWallets = await getDataPartnerById(
        "https://djufzxp9v1.execute-api.us-east-1.amazonaws.com/bi-dev/partner-producer-customer-portfolio",
        83
      );
      dispatch(setUserWalletClients(resDataClientWallets.customers));
      dispatch(
        setUserWalletClients2(userWalletsTable(resDataClientWallets.customers))
      );
    };

    getDataUserWalletClientsById();
    /* **************************************************************************** */
    dispatch(loadUserData());
    const getDataUser = async () => {
      const resUserData = await getDataPartnerById(
        "https://djufzxp9v1.execute-api.us-east-1.amazonaws.com/bi-dev/api_getpartnerinfo_stg",
        83
      );
      dispatch(setUserData(resUserData));
    };
    getDataUser();
    /* **************************************************************************** */
    dispatch(loadCoinMervalData());
    const getCoinMervalData = async () => {
      const resCoinMervalData = await getMarketInfo(
        "https://p2f5xhzxy0.execute-api.us-east-1.amazonaws.com/prod/prices/last/index"
      );
      dispatch(setCoinMervalData(resCoinMervalData));
    };
    getCoinMervalData();
    /* **************************************************************************** */
    dispatch(loadCoinPricesData());
    const getCoinPricesData = async () => {
      const resCoinPricesData = await getMarketInfo(
        "https://p2f5xhzxy0.execute-api.us-east-1.amazonaws.com/prod/prices/last/ccy"
      );
      dispatch(setCoinPricesData(resCoinPricesData));
    };
    getCoinPricesData();
    /* **************************************************************************** */
    dispatch(loadFundsData());
    const getFundsData = async () => {
      const resFundsData = await getMarketInfo(
        "https://intranet.bnzlab.com/wp-json/get/listado_raw/FUND/ALL"
      );
      dispatch(setFundsData(resFundsData));
    };
    getFundsData();
    /* **************************************************************************** */
    dispatch(loadCedearsData());
    const getCedearsData = async () => {
      const resCedearsData = await getMarketInfo(
        "https://intranet.bnzlab.com/wp-json/get/listado_raw/CERTIFICATE/EQUITY"
      );
      dispatch(setCedearsData(resCedearsData));
    };
    getCedearsData();
    /* **************************************************************************** */
    dispatch(loadEquityData());

    const getEquityData = async () => {
      const resEquityData = await getMarketInfo(
        "https://intranet.bnzlab.com/wp-json/get/listado_raw/BYMA.IGB/EQUITY"
      );
      const resEquityData2 = await getMarketInfo(
        "https://intranet.bnzlab.com/wp-json/get/listado_raw/BYMA.M.AR/EQUITY"
      );
      const resEquityData3 = await getMarketInfo(
        "https://intranet.bnzlab.com/wp-json/get/listado_raw/BYMA.MERV/EQUITY"
      );

      if (resEquityData && resEquityData2 && resEquityData3) {
        const equityGeneral = resEquityData.map(element => {
          return { ...element, subType: "General" };
        });

        const equityArg = resEquityData2.map(element => {
          return { ...element, subType: "Arg" };
        });

        const equityMerval = resEquityData3.map(element => {
          return { ...element, subType: "Merval" };
        });

        dispatch(setEquityData(equityGeneral.concat(equityArg, equityMerval)));
      }
    };
    getEquityData();
    /* **************************************************************************** */
    dispatch(loadBondsData());
    const getBondsData = async () => {
      const resSoberaneBonds = await getMarketInfo(
        "https://intranet.bnzlab.com/wp-json/get/listado_raw/BOND/GOVERNMENT"
      );

      const resPorvincialBonds = await getMarketInfo(
        "https://intranet.bnzlab.com/wp-json/get/listado_raw/BOND/PROVINCIAL"
      );

      const resCorporativeBonds = await getMarketInfo(
        "https://intranet.bnzlab.com/wp-json/get/listado_raw/BOND/CORPORATE"
      );

      if (resSoberaneBonds && resPorvincialBonds && resCorporativeBonds) {
        dispatch(
          setBondsData(
            resSoberaneBonds.concat(resPorvincialBonds, resCorporativeBonds)
          )
        );
      }
    };
    getBondsData();
    /* **************************************************************************** */
    dispatch(loadUserBalance());
    const getUserBalance = () => {
      setTimeout(() => dispatch(setUserBalance(userBalance)), 1000);
    };
    getUserBalance();
    /* **************************************************************************** */
    dispatch(loadTreasuryData());
    const getTreasuryData = () => {
      setTimeout(() => dispatch(setTreasuryData(treasuryData)), 1000);
    };
    getTreasuryData();
    /* **************************************************************************** */
    dispatch(loadTeamsPartner());
    const getTeamsPartnerData = async () => {
      const resTeamsPartnerData = await getTeamPartnerById(
        "https://djufzxp9v1.execute-api.us-east-1.amazonaws.com/bi-dev/api_getteam_stg",
        10,
        1,
        1
      );
      dispatch(setTeamsPartner(resTeamsPartnerData));
    };
    getTeamsPartnerData();
    /* **************************************************************************** */
    /* **************************************************************************** */
    /* **************************************************************************** */
    /* **************************************************************************** */
    dispatch(loadClients());
    const getDataClients = () => {
      setTimeout(async () => {
        await dispatch(setClients(clients));
      }, 2000);
    };
    getDataClients();
    /* **************************************************************************** */
    dispatch(loadMovements());
    const getDataMovements = () => {
      setTimeout(async () => {
        await dispatch(setMovements(movements));
      }, 2000);
    };
    getDataMovements();

    /* **************************************************************************** */
    dispatch(loadLatestNews());
    const getLatestNews = () => {
      setTimeout(async () => {
        await dispatch(setLatestNews(latestNews));
      }, 2000);
    };
    getLatestNews();
    /* **************************************************************************** */
    dispatch(loadListMarketIndex());
    const getMarketIndex = () => {
      setTimeout(async () => {
        await dispatch(setListMarketIndex(listMarketIndex));
      }, 2000);
    };
    getMarketIndex();
    /* **************************************************************************** */
    dispatch(loadMarketCards());
    const getMarketCards = () => {
      setTimeout(async () => {
        await dispatch(setMarketCards(cardsMarket));
      }, 2000);
    };
    getMarketCards();
    /* **************************************************************************** */
  }, []);

  useEffect(() => {
    dispatch(setRoutes(items));
  }, [dispatch]);

  useEffect(() => {
    const route = routes.find(r => r.url === pathname);
    route && dispatch(setSelectedRoute(route.id));
  }, [routes, pathname, dispatch]);

  return (
    <div className={`main-container ${openBlur ? "openBlur" : ""}`}>
      <div className={`first-container ${expanded ? "expanded1" : ""} `}>
        <Sidebar />
      </div>
      <div className="second-container">
        <Navbar />
        <MainViews />
      </div>
      <BlurView />
    </div>
  );
};

const BlurView = ({}) => {
  const state = useSelector(state => state);
  const { openBlur } = state;
  return (
    <div
      className={`blur-view-container ${openBlur ? "openProtectorBlur" : ""}`}
    ></div>
  );
};
