import React, { useState } from "react";
import { useSelector } from "react-redux";
import { MarketIndexMiniCards } from "../marketIndexMiniCards/marketIndexMiniCards";
import { MarketSelections } from "../marketSelections/marketSelections";
import { MarketBigCards } from "../marketBigCards/marketBigCards";
import { MarketDetailCard } from "../marketDetailCard/marketDetailCard";
import { setCheckListRadio } from "../../libs/libsMarket";
import "./market.css";

export const Market = () => {
  const state = useSelector(state => state);
  const {
    listMarketIndex,
    listMarketCards,
    coinPricesData,
    coinMervalData,
    clients,
    fundsData,
    bondsData,
    cedearsData,
    equityData
  } = state;

  const [optionItem, setOptionItem] = useState("COTIZACIONES DE MERCADO");
  const [selectedCardMarket, setSelectedCardMarket] = useState(
    listMarketCards && listMarketCards[0]
  );

  const [selectedRadio, setSelectedRadio] = useState("48hs");
  const [listTerms, setListTerms] = useState([
    {
      id: 0,
      term: "48hs",
      days: 2,
      checked: true
    },
    {
      id: 1,
      term: "24hs",
      days: 1,
      checked: false
    },
    {
      id: 2,
      term: "Contado Inmediato",
      days: 0,
      checked: false
    }
  ]);

  const selectTerm = radio => {
    setSelectedRadio(radio.term);
    setListTerms(setCheckListRadio(listTerms, radio));
  };

  const [selectedRadioCard, setSelectedRadioCard] = useState("Bonos");
  const [listRadioCard, setListRadioCard] = useState([
    {
      id: 0,
      term: "Fondos",
      checked: false
    },
    {
      id: 1,
      term: "Bonos",
      checked: true
    },
    {
      id: 2,
      term: "Acciones",
      checked: false
    },
    {
      id: 3,
      term: "Cedears",
      checked: false
    }
    // {
    //   id: 4,
    //   term: "Commodities",
    //   checked: false
    // }
  ]);

  const selectRadioCard = radio => {
    const titleCard = radio.term;
    setSelectedRadioCard(radio.term);
    const card = listMarketCards.find(x => x.title === titleCard);
    selectCard(card);
    setListRadioCard(setCheckListRadio(listRadioCard, radio));
  };

  const selectCard = card => {
    setSelectedCardMarket(card);
    const radio = listRadioCard.find(rad => rad.term === card.title);
    setListRadioCard(setCheckListRadio(listRadioCard, radio));
    setSelectedRadioCard(card.title);
    setOptionItem("INSTRUMENTOS");
  };

  return (
    <div className="market-container">
      <MarketIndexMiniCards
        listMarketIndex={listMarketIndex}
        coinPricesData={coinPricesData}
        coinMervalData={coinMervalData}
      />

      <MarketSelections
        optionItem={optionItem}
        setOptionItem={setOptionItem}
        listTerms={listTerms}
        selectTerm={selectTerm}
        listRadioCard={listRadioCard}
        selectRadioCard={selectRadioCard}
        selectedCardMarket={selectedCardMarket}
      />

      {optionItem === "COTIZACIONES DE MERCADO" ? (
        <MarketBigCards
          listMarketCards={listMarketCards}
          listTerms={listTerms}
          selectCard={selectCard}
          clients={clients}
          selectedRadio={selectedRadio}
          fundsData={fundsData}
          bondsData={bondsData}
          cedearsData={cedearsData}
          equityData={equityData}
        />
      ) : optionItem === "INSTRUMENTOS" ? (
        <MarketDetailCard
          selectedCardMarket={selectedCardMarket}
          clients={clients}
          listTerms={listTerms}
          selectedRadio={selectedRadio}
        />
      ) : (
        <div style={{ border: "1px solid green" }}>favoritos</div>
      )}
    </div>
  );
};
