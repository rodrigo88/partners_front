import React, { useState } from "react";
import { useSelector } from "react-redux";
import { CardProfile } from "../../components/cardProfile/cardProfile";
import { SelectProfile } from "../../components/selectProfile/selectProfile";
import { StaticsProfile } from "../../components/staticsProfile/staticsProfile";
import { SelectProfileCoin } from "../../components/selectProfileCoin/selectProfileCoin";
import { Subtitle, Title, Text } from "../../components/titles/titles";
import portIcon from "../../utils/Grupo 3334.svg";
import redDownArrow from "../../utils/DownArrow.svg";
import "./portfolio.css";

export const Portfolio = () => {
  const state = useSelector(state => state);
  const { userInformation, loadingUserInformation } = state;

  const [selection, setSelection] = useState({
    value: 3,
    description: "Año calendario"
  });

  const options = [
    { value: 0, description: "Ultima semana" },
    { value: 1, description: "Ultimos 30 días" },
    { value: 2, description: "Ultimos 60 días" },
    { value: 3, description: "Año calendario" }
  ];

  const [selectionCoin, setSelectionCoin] = useState({
    value: 0,
    description: "ARS"
  });

  const optionsCoin = [
    { value: 0, description: "ARS" },
    { value: 1, description: "USD" }
  ];

  const setSelection2 = opt => setSelection(opt);

  const feeToCard = () => {
    let feeValue;
    const { value, description } = selection;
    switch (value) {
      case 0:
        feeValue = userInformation.fee.totalWeek;
        break;
      case 1:
        feeValue = userInformation.fee.totalMonth;
        break;
      case 2:
        feeValue = userInformation.fee.totalTwoMonths;
        break;
      default:
        feeValue = userInformation.fee.totalYear;
        break;
    }
    return feeValue;
  };

  const setSelectionCoin2 = opt => setSelectionCoin(opt);

  const consolidadaToCard = () => {
    // if (loadingUserInformation) return;
    let consValue;
    const { value, description } = selectionCoin;
    switch (value) {
      case 0:
        consValue = userInformation.holdings[1].valuePesos;
        break;
      default:
        consValue = userInformation.holdings[1].valueDollars;
        break;
    }
    return consValue;
  };

  return (
    <div className="portfolio-card">
      <div className="portfolio-card-header">
        <div className="portfolio-header-title">
          <img src={portIcon} alt="..." style={{ marginRight: "15px" }} />
          <Subtitle
            text="Mi Portfolio"
            type="main"
            style={{ fontWeight: "bolder" }}
          />
        </div>
        <div className="portfolio-header-selection">
          {!loadingUserInformation && (
            <>
              <div className="label-select">Visualizar datos</div>
              <SelectProfile
                options={options}
                selection={selection}
                setSelection2={setSelection2}
              />
            </>
          )}
        </div>
      </div>

      {loadingUserInformation ? (
        <LoadingView />
      ) : (
        <>
          <div className="portfolio-content">
            <div className="portfolio-content-tenencia">
              <div>Tenencia Consolidada</div>
              <div className="portfolio-content-tenencia-value-coins">
                <Title type="purple">{consolidadaToCard()}</Title>
                <SelectProfileCoin
                  options={optionsCoin}
                  selection={selectionCoin}
                  setSelectionCoin2={setSelectionCoin2}
                />
              </div>

              <div className="content-porcentual">
                <img src={redDownArrow} alt="..." />
                <Text type="danger" text="13,8 %" />
              </div>
            </div>
            <div style={{ width: "33%" }}>
              <StaticsProfile coin="ARS" />
            </div>
            <div style={{ width: "33%" }}>
              <StaticsProfile coin="USD" />
            </div>
          </div>

          <div className="portfolio-results">
            <CardProfile
              cardStyle="card--footer--portfolio--green"
              description="Tenencia Líquida"
              amountPesos={userInformation.holdings[0].valuePesos}
              amountDollars={userInformation.holdings[0].valueDollars}
            />

            <CardProfile
              cardStyle="card--footer--portfolio--red"
              description="Total en Descubierto"
              amountPesos={userInformation.overdraftPesos}
              amountDollars={userInformation.overdraftDollars}
            />

            <CardProfile
              cardStyle="card--footer--portfolio--hover"
              description="FEE Total Ganancia"
              amountPesos={feeToCard()}
              amountDollars={feeToCard()}
            />
          </div>
        </>
      )}
    </div>
  );
};

export const LoadingView = () => (
  <div className="lds-roller">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>
);
