import React, { useState } from "react";
import { Button } from "../../components/button/button";
import { InputText } from "../../components/inputText/inputText";
import { Subtitle, Paragraph } from "../../components/titles/titles";
import { Link } from "react-router-dom";
import { Divider } from "../../components/divider/divider";
import "./recoverPassword.css";

export const RecoverPassword = () => {
  const initialState = { username: "", email: "" };

  const [user, setUser] = useState(initialState);

  const onChange = e => setUser({ ...user, [e.target.name]: e.target.value });

  const onSubmit = e => e.preventDefault();

  return (
    <div className="signin">
      <Subtitle type="main" text="Recuperar clave de ingreso" />
      <Divider name="X" />
      <div className="input-text-mandatory">
        <Paragraph
          type="text"
          text="Por favor, introduzca la direccion de correo electrónico registrada en su cuenta de Usuario. Un vínculo será enviado a usted. Seleccionandolo podrá acceder al ingreso de una nueva clave."
        />
      </div>
      <Divider name="L" />
      <form onSubmit={onSubmit} className="form-signin">
        <InputText
          type="text"
          name="username"
          placeholder="Usuario"
          value={user.username}
          onChange={onChange}
          inputSize="input--large"
        />
        <Divider name="X" />
        <InputText
          type="email"
          name="email"
          placeholder="Dirección de correo electrónico *"
          value={user.email}
          onChange={onChange}
          inputSize="input--large"
        />
        <Divider name="S" />
        <div className="input-text-mandatory">
          <Paragraph
            type="text"
            text={`* La dirección de correo electrónico ingresada deberá coincidir con la registrada en el usuario`}
          />
        </div>
        <Divider name="X" />
        <div className="forget-links-3">
          <div>
            <Link to={"/"}>
              <Button
                buttonStyle="btn--secondary"
                buttonSize="btn--medium"
                type="button"
              >
                VOLVER
              </Button>
            </Link>
          </div>
          <div>
            <Button
              buttonStyle="btn--primary"
              buttonSize="btn--medium"
              type="submit"
            >
              RECUPERAR
            </Button>
          </div>
        </div>
      </form>
      <Divider name="X" />
    </div>
  );
};
