import React from "react";
import { useDispatch } from "react-redux";
import { setDetailEspecie } from "../../redux/actions";
import { Divider } from "../../components/divider/divider";
import radioElipseIcon from "../../utils/Outer Ellipse.svg";
import iconCalendar from "../../utils/calendar.svg";
import "./marketSelections.css";

export const MarketSelections = ({
  optionItem,
  setOptionItem,
  listTerms,
  selectTerm,
  listRadioCard,
  selectRadioCard,
  selectedCardMarket
}) => {
  const dispatch = useDispatch();

  return (
    <div className="market-container-tabs">
      <div className="market-container-tabs-options">
        {/* TAB SELECTION MARKET */}
        <div className="market-container-tabs-options-items">
          <Divider name="S" />
          <div
            className={`tabs-options-item ${
              optionItem === "COTIZACIONES DE MERCADO" ? "optionItemActive" : ""
            }`}
            onClick={() => setOptionItem("COTIZACIONES DE MERCADO")}
          >
            COTIZACIONES DE MERCADO
          </div>
          <Divider name="XXL" />
          <div
            className={`tabs-options-item ${
              optionItem === "INSTRUMENTOS" ? "optionItemActive" : ""
            }`}
            onClick={() => setOptionItem("INSTRUMENTOS")}
          >
            INSTRUMENTOS
          </div>
          <Divider name="XXL" />
          <div
            className={`tabs-options-item ${
              optionItem === "FAVORITOS" ? "optionItemActive" : ""
            }`}
            onClick={() => setOptionItem("FAVORITOS")}
          >
            FAVORITOS
          </div>
        </div>

        <div className="market-container-tabs-options-line-divider" />
        <Divider name="M" />
        {/* RADIO BUTTONS MARKET */}
        <div
          style={{
            opacity:
              selectedCardMarket &&
              selectedCardMarket.title === "Fondos" &&
              0.3,
            pointerEvents:
              selectedCardMarket &&
              selectedCardMarket.title === "Fondos" &&
              "none"
          }}
          className="market-container-tabs-options-terms"
        >
          <Divider name="S" />
          <div>Tipo de Plazo</div>
          <Divider name="L" />
          <div className="market-container-tabs-options-terms-radios">
            {listTerms.map(termRadio => (
              <div
                className="market-container-tabs-options-radio-term"
                key={termRadio.id}
              >
                <img
                  onClick={() => selectTerm(termRadio)}
                  className={`img-radio-term ${
                    termRadio.checked ? "checked" : ""
                  }`}
                  src={radioElipseIcon}
                  alt="..."
                />
                <Divider name="XS" />
                <div>{termRadio.term}</div>
                <Divider name="L" />
              </div>
            ))}
          </div>
        </div>
        {optionItem === "INSTRUMENTOS" && (
          <div className="market-container-tabs-options-terms">
            <Divider name="S" />
            <div>Mostrar</div>
            <Divider name="L" />
            <div className="market-container-tabs-options-terms-radios">
              {listRadioCard.map(termRadio => (
                <div
                  className="market-container-tabs-options-radio-term"
                  key={termRadio.id}
                >
                  <img
                    onClick={() => {
                      selectRadioCard(termRadio);
                      dispatch(setDetailEspecie(null));
                    }}
                    className={`img-radio-term ${
                      termRadio.checked ? "checked" : ""
                    }`}
                    src={radioElipseIcon}
                    alt="..."
                  />
                  <Divider name="XS" />
                  <div>{termRadio.term}</div>
                  <Divider name="L" />
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
      <Divider name="S" />
      <MarketSchedule icon={iconCalendar} />
      {/* <div className="market-container-tabs-schedule">
        <div className="market-container-tabs-schedule-container">
          <div className="market-container-tabs-schedule-calendar">
            <img src={iconCalendar} alt="..." />
          </div>
          <div
            className="market-container-tabs-schedule-title"
            style={{ fontSize: "14px", fontWeight: "600" }}
          >
            HORARIO
          </div>
          <div
            className="market-container-tabs-schedule-atention"
            style={{ fontSize: "12px" }}
          >
            Lunes a viernes de 10 a 17 hs
          </div>
        </div>
      </div> */}
    </div>
  );
};

/* ************************************************************ */

export const MarketSchedule = ({ icon }) => {
  return (
    <div className="market-container-tabs-schedule">
      <div className="market-container-tabs-schedule-container">
        <div className="market-container-tabs-schedule-calendar">
          <img src={icon} alt="..." />
        </div>
        <div
          className="market-container-tabs-schedule-title"
          style={{ fontSize: "14px", fontWeight: "600" }}
        >
          HORARIO
        </div>
        <div
          className="market-container-tabs-schedule-atention"
          style={{ fontSize: "12px" }}
        >
          Lunes a viernes de 10 a 17 hs
        </div>
      </div>
    </div>
  );
};
