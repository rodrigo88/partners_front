import React, { useState } from "react";
// import { WalletLeft } from "../../components/walletLeft/walletLeft";
// import { WalletRight } from "../../components/walletRight/walletRight";
// import { TreasuryModalProof } from "../../components/treasuryModalProof/treasuryModalProof";
import { TeamLeft } from "../../components/teamLeft/teamLeft";
import { TeamRight } from "../../components/teamRight/teamRight";
import { TeamModalReasignarCartera } from "../../components/teamModalReasignarCartera/teamModalReasignarCartera";
import { TeamModalDeshabilitarCuenta } from "../../components/teamModalDeshabilitarCuenta/teamModalDeshabilitarCuenta";
import "./team.css";

export const Team = () => {
  const [producer, setProducer] = useState(null);
  const [selectionPopupMenu, setSelectionPopupMenu] = useState(null);

  const [openModalReasignar, setOpenModalReasignar] = useState(false);
  const [openModalDeshabilitar, setOpenModalDeshabilitar] = useState(false);
  const [openModalCambiarRol, setOpenModalCambiarRol] = useState(false);

  const setSelectionPopupMenu2 = option => {
    console.log("OPTION: ", option);
    setSelectionPopupMenu(option);
    switch (option) {
      case "REASIGNAR CARTERA":
        setOpenModalReasignar(true);
      // setOpenModalDeshabilitar(false);
      // setOpenModalCambiarRol(false);
      case "DESHABILITAR CUENTA":
        setOpenModalDeshabilitar(true);
      // setOpenModalReasignar(false);
      // setOpenModalCambiarRol(false);
      default:
        setOpenModalCambiarRol(true);
      // setOpenModalReasignar(false);
      // setOpenModalDeshabilitar(false);
    }
  };

  return (
    <div className="team-dashboard-container">
      <TeamLeft
        setProducer={setProducer}
        setSelectionPopupMenu={setSelectionPopupMenu2}
      />
      <TeamRight />
      <TeamModalReasignarCartera
        openModal={openModalReasignar}
        closeModal={() => setOpenModalReasignar(false)}
      />
      <TeamModalDeshabilitarCuenta
        openModal={openModalDeshabilitar}
        closeModal={() => setOpenModalDeshabilitar(false)}
      />
    </div>
  );
};
