import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { Home } from "../home/home";
import { Market } from "../market/market";
import { Operate } from "../operate/operate";
import { Treasury } from "../treasury/treasury";
import { Wallet } from "../wallet/wallet";
import { Team } from "../team/team";
import "./mainViews.css";

export const MainViews = () => {
  return (
    <div className="main-views-container">
      <Switch>
        <Route path="/team">
          <Team />
        </Route>
        <Route path="/wallet">
          <Wallet />
        </Route>
        <Route path="/treasury">
          <Treasury />
        </Route>
        <Route path="/operate">
          <Operate />
        </Route>
        <Route path="/market">
          <Market />
        </Route>
        <Route exact path="/home">
          <Home />
        </Route>
        <Redirect from="/" to="/team" />
      </Switch>
    </div>
  );
};
