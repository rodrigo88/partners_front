import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Button } from "../../components/button/button";
import { Subtitle, Paragraph } from "../../components/titles/titles";
import { InputText } from "../../components/inputText/inputText";
import { Divider } from "../../components/divider/divider";
import "./recoverUser.css";

export const RecoverUser = () => {
  const initialState = { email: "" };

  const [user, setUser] = useState(initialState);

  const onChange = e => setUser({ ...user, [e.target.name]: e.target.value });

  const onSubmit = e => {
    e.preventDefault();
    console.log(user.email);
  };

  return (
    <div className="recover-container">
      <Subtitle type="main" text="Recuperar usuario" />
      <Divider name="XS" />
      <div className="input-text-mandatory">
        <Paragraph
          type="text"
          text={`Por favor, introduzca la dirección de correo electrónico registrada en su cuenta de Usuario..Su usuario será enviado por correo electrónico`}
        />
      </div>
      <Divider name="XXL" />
      <form onSubmit={onSubmit} className="form-signin">
        <InputText
          placeholder="Dirección de correo electrónico *"
          type="email"
          value={user.email}
          name="email"
          onChange={onChange}
          inputSize="input--large"
        />
        {/* <Divider name="XS" /> */}
        <div className="input-text-mandatory">
          <Paragraph
            type="text"
            text={`*La dirección de correo electrónico ingresada deberá coincidir con la registrada en el usuario`}
          />
        </div>
        <Divider name="XXL" />
        <div className="redirect-buttons">
          <div>
            <Link to={"/"}>
              <Button
                buttonStyle="btn--secondary"
                buttonSize="btn--medium"
                type="button"
              >
                VOLVER A LOGIN
              </Button>
            </Link>
          </div>
          <div>
            <Button
              buttonStyle="btn--primary"
              buttonSize="btn--medium"
              type="submit"
            >
              RECUPERAR
            </Button>
          </div>
        </div>
      </form>
      <Divider name="XXXXL" />
    </div>
  );
};
