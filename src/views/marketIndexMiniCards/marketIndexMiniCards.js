import React from "react";
import { Divider } from "../../components/divider/divider";
import upArrowIcon from "../../utils/UpArrowMarket.svg";
import downArrowIcon from "../../utils/DownArrowMarket.svg";
import iconHomeNewsMarket from "../../utils/Enmascarar grupo 1@2x.png";
import "./marketIndexMiniCards.css";

export const MarketIndexMiniCards = ({ coinPricesData, coinMervalData }) => {
  return (
    <div className="market-container-index">
      <div className="merval">
        <div className="title-list">INDICES DE MERCADO</div>
        {coinMervalData.map((merval, i) => (
          <CoinData key={i} element={merval} />
        ))}
      </div>
      <Divider name="S" />
      <div className="divisas">
        <div className="title-list">DIVISAS</div>
        <div className="divisas-list-elements">
          {coinPricesData.map((coin, i) => (
            <CoinData key={i} element={coin} />
          ))}
        </div>
      </div>
    </div>
  );
};

const CoinData = ({ element }) => {
  const { NAME, VAR, OPEN, HIGH, LOW } = element;
  const countryArg = NAME.substring(0, 3);
  const countryOther = NAME.substring(3, 6);
  return (
    <div
      className={`market-container-index-element ${VAR >= 0 ? "green" : "red"}
      ${countryArg === "MER" ? "black1" : ""}`}
    >
      <div className={`market-container-index-element-left`}>
        <div className="image-country">
          <img src={iconHomeNewsMarket} alt="..." />
          <Divider name="XXS" />
          <div style={{ color: countryArg === "MER" ? "white" : "#67698b" }}>
            {countryArg}
          </div>
        </div>
        <Divider name="L" />
        <div className="image-arrow">
          <img src={VAR >= 0 ? upArrowIcon : downArrowIcon} alt="..." />
          <Divider name="XXS" />
          <div
            style={{ color: countryArg === "MER" ? "white" : "#67698b" }}
            className="var"
          >
            {VAR}%
          </div>
        </div>
      </div>
      <div className="market-container-index-element-right">
        <div
          style={{ color: countryArg === "MER" ? "white" : "#67698b" }}
          className="low"
        >
          {HIGH}
        </div>
        {countryArg === "MER" && (
          <div
            style={{ color: countryArg === "MER" ? "white" : "#67698b" }}
            className="low"
          >
            {LOW}
          </div>
        )}
      </div>
    </div>
  );
};
