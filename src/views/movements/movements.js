import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Button } from "../../components/button/button";
import { Movement } from "../../components/movement/movement";
import { Subtitle } from "../../components/titles/titles";
// import { movements } from "../../constants/constants";
import { Divider } from "../../components/divider/divider";
import { SelectMovementsProof } from "../../components/selectMovementsProof/selectMovementsProof";
import { InputTextMovementsProof } from "../../components/inputTextMovemenProof/inputTextMovementsProof";
import movsIcon from "../../utils/Grupo 3389.svg";
import "./movements.css";

export const Movements = () => {
  const state = useSelector(state => state);
  const { movements } = state;

  const [proof, setProof] = useState(false);
  const [selection, setSelection] = useState({ value: -1, description: "" });

  const options = [
    { value: 0, description: "Ultima semana" },
    { value: 1, description: "Ultimos 30 días" },
    { value: 2, description: "Ultimos 60 días" },
    { value: 3, description: "Año calendario" }
  ];

  return (
    <div className="movements-container">
      <div className="movements-header">
        <div className="movements-header-title">
          <img src={movsIcon} alt="..." style={{ marginRight: "15px" }} />
          <Subtitle text="Ultimos movimientos" type="main" />
        </div>
        <div className="movements-header-buttons">
          <Button
            onClick={() => setProof(x => !x)}
            type="button"
            buttonStyle="btn--secondary"
            buttonSize="btn--small"
          >
            CARGAR COMPROBANTE
          </Button>
          <Divider name="M" />
          <Link to={"/treasury"}>
            <Button
              type="button"
              buttonStyle="btn--primary"
              buttonSize="btn--small"
            >
              IR A TESORERIA
            </Button>
          </Link>
        </div>
      </div>
      <div className="movements-content">
        {movements.map(mov => {
          return <Movement key={mov.id} movement={mov} />;
        })}
      </div>

      {proof && (
        <div className="movements-proof-load">
          <div className="movements-proof-load-cancel">X</div>
          <div className="movements-proof-load-title">
            Ingresar comprobantes
          </div>
          <div className="movements-proof-load-names">
            <div className="movements-proof-labels">Nombre del cliente</div>
            <Divider name="XXS" />
            <InputTextMovementsProof
              type="text"
              name="description"
              placeholder="Apellido y nombre cliente"
              // value={data.description}
              // onChange={onChange}
              inputSize="input--medium"
            />
          </div>
          <div className="movements-proof-load-selects">
            <div className="movements-proof-load-selects-coin">
              <div className="movements-proof-labels">Moneda</div>
              <Divider name="XXS" />
              <SelectMovementsProof
                title="Pesos"
                options={options}
                selection={selection}
                setSelection={setSelection}
              />
            </div>
            <Divider name="M" />
            <div className="movements-proof-load-selects-account">
              <div className="movements-proof-labels">
                Selecciona cuenta de origen
              </div>
              <Divider name="XXS" />
              <SelectMovementsProof
                title="CBU"
                options={options}
                selection={selection}
                setSelection={setSelection}
              />
            </div>
          </div>
          <div className="movements-proof-load-content"></div>
        </div>
      )}
    </div>
  );
};
