import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Button } from "../../components/button/button";
import { ClientWallet } from "../../components/clientWallet/clientWallet";
import { Subtitle } from "../../components/titles/titles";
// import { clients } from "../../constants/constants";
import { Divider } from "../../components/divider/divider";
import { SelectClientsWallet } from "../../components/selectClientsWallet/selectClientsWallet";
import { InputTextClientsWallet } from "../../components/inputTextClientsWallet/inputTextClientsWallet";
import iconWallet from "../../utils/icon-home-cartera.svg";
import iconFilter from "../../utils/icon-filtrar.svg";
import iconGroup from "../../utils/icon-grupo.svg";
import basicTabIcon from "../../utils/BasicTab.svg";
import "./clientsWallet.css";

export const ClientsWallet = () => {
  const state = useSelector(state => state);
  const { userWalletClients, loadingUserWalletClients } = state;

  // const [data, setData] = useState({description: ""});
  // const onChange = e => setData({ ...data, [e.target.name]: e.target.value });
  const [searchClientWallet, setSearchClientWallet] = React.useState("");
  const [myFilteredClients, setMyFilteredClients] = React.useState([]);
  const onChangeClientWallet = e => {
    setSearchClientWallet(e.target.value);
    const enteredSearch = e.target.value.toLowerCase();
    const filteredWalletClents = userWalletClients.filter(wclient =>
      belongsTo(enteredSearch, wclient)
    );
    setMyFilteredClients(filteredWalletClents);
  };

  const belongsTo = (value, client) =>
    client.name.toLowerCase().includes(value);

  const filteredClientsWallet = () =>
    searchClientWallet === "" ? userWalletClients : myFilteredClients;

  const [selection, setSelection] = useState({ value: -1, description: "" });

  const [openId, setOpenId] = useState(-1);
  const [openOptions, setOpenOptions] = useState(false);

  const [itemsOptions] = useState([
    { id: 0, option: "APLICAR CARTERA" },
    { id: 1, option: "OPERAR INDIVIDUAL" },
    { id: 2, option: "Ver ficha del cliente" },
    { id: 3, option: "Enviar Mensaje" },
    { id: 4, option: "Felicitar" }
  ]);

  const options = [
    { value: 0, description: "Ultima semana" },
    { value: 1, description: "Ultimos 30 días" },
    { value: 2, description: "Ultimos 60 días" },
    { value: 3, description: "Año calendario" }
  ];

  const setOpenOptionsClient = idClient => {
    if (openId === idClient) {
      setOpenOptions(false);
    } else {
      setOpenId(idClient);
      setOpenOptions(false);
      setInterval(() => {
        setOpenOptions(true);
      }, 300);
    }
  };

  return (
    <div className="clientsWallet-container">
      <div className="clientsWallet-header">
        <div className="clientsWallet-header-title">
          <img src={iconWallet} alt="..." style={{ marginRight: "15px" }} />
          <Subtitle text="Cartera de clientes" type="main" />
        </div>
        <div className="clientsWallet-header-selections">
          {!loadingUserWalletClients && (
            <>
              <InputTextClientsWallet
                type="text"
                name="searchField"
                placeholder="Escriba aqui..."
                value={searchClientWallet}
                onChange={onChangeClientWallet}
                inputSize="input--medium"
              />
              <SelectClientsWallet
                icon={iconGroup}
                title=""
                options={options}
                selection={selection}
                setSelection={setSelection}
              />
              <SelectClientsWallet
                icon={iconFilter}
                title="Instrumento"
                options={options}
                selection={selection}
                setSelection={setSelection}
              />
              <SelectClientsWallet
                icon={iconFilter}
                title="Especie"
                options={options}
                selection={selection}
                setSelection={setSelection}
              />
              <Link to={"/operate"}>
                <Button
                  onClick={() => console.log("COMPROBANTE")}
                  type="button"
                  buttonStyle="btn--primary"
                  buttonSize="btn--medium"
                >
                  OPERAR BLOQUE
                </Button>
              </Link>
            </>
          )}
        </div>
      </div>

      {loadingUserWalletClients ? (
        <LoadinView />
      ) : (
        <>
          <img style={{ width: "100%" }} src={basicTabIcon} alt="..." />

          <Divider name="M" />

          <div className="clientsWallet-header-content">
            <div className="clientsWallet-header-content-detail">DETALLE</div>
            <div className="clientsWallet-header-content-item">AUM ARS</div>
            <div className="clientsWallet-header-content-item">AUM USD</div>
            <div className="clientsWallet-header-content-item">T.M ARS</div>
            <div className="clientsWallet-header-content-item">T.M USD</div>
            <div className="clientsWallet-header-content-item">FEE</div>
          </div>

          <div className="clientsWallet-content">
            {filteredClientsWallet() &&
              filteredClientsWallet().map(client => (
                <ClientWallet
                  key={client.idCustomer}
                  client={client}
                  openId={openId}
                  setOpenOptionsClient={setOpenOptionsClient}
                  closeOptions={() => {
                    setOpenOptions(false);
                    setOpenId(-1);
                  }}
                />
              ))}
          </div>

          {openOptions && openId !== -1 && (
            <div className="options-container">
              {itemsOptions.map(({ id, option }) => (
                <div
                  onClick={() => {
                    console.log(option, openId);
                    setOpenId(-1);
                  }}
                  className={`option-container ${
                    id < itemsOptions.length - 1 ? "style-border" : ""
                  }`}
                  key={id}
                >
                  {option}
                </div>
              ))}
            </div>
          )}
        </>
      )}
    </div>
  );
};

const LoadinView = () => (
  <div className="lds-roller">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>
);
