import React, { useState } from "react";
import { WalletLeft } from "../../components/walletLeft/walletLeft";
import { WalletRight } from "../../components/walletRight/walletRight";
import "./wallet.css";

export const Wallet = () => {
  const [option, setOption] = useState("TENENCIA");
  const [selectedClientDashboard, setSelectedClientDashboard] = useState(null);
  const [loadingData, setLoadingData] = useState(false);

  const beginToWait = () => {
    setLoadingData(true);
    setTimeout(() => setLoadingData(false), 1500);
  };

  return (
    <div className="wallet-dashboard-container">
      <WalletLeft
        setOption={setOption}
        setSelectedClientDashboard={setSelectedClientDashboard}
        beginToWait={beginToWait}
      />
      <WalletRight
        option={option}
        setOption={setOption}
        selectedClientDashboard={selectedClientDashboard}
        loadingData={loadingData}
      />
    </div>
  );
};
