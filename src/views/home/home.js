import React from "react";
import { Portfolio } from "../portfolio/portfolio";
import { Movements } from "../movements/movements";
import { Operations } from "../operations/operations";
import { ClientsWallet } from "../clientsWallet/clientsWallet";
import { RecommendedWallets } from "../recommendedWallets/recommendedWallets";
import { LatestNews } from "../latestNews/latestNews";
import "./home.css";

export const Home = () => {
  return (
    <div className="home-container">
      <Portfolio />
      <Movements />
      <Operations />
      <RecommendedWallets />
      <ClientsWallet />
      <LatestNews />
    </div>
  );
};
