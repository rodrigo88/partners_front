import React from "react";
import { Route, Redirect } from "react-router-dom";
import { HeaderSingin } from "../../components/headerSingin/headerSingin";
import { FooterSignin } from "../../components/footerSignin/footerSignin";
import { RecoverUser } from "../../views/recoverUser/recoverUser";
import { RecoverPassword } from "../../views/recoverPassword/recoverPassword";
import { Signin } from "../../views/signin/signin";
import { Divider } from "../../components/divider/divider";
import leftImage from "../../utils/Grupo 3339.png";
import "./login.css";

export const Login = () => {
  return (
    <div className="container-login">
      <img src={leftImage} className="left-image" alt="..." />
      <div className="container-2">
        <Divider name="X" />
        <HeaderSingin />
        <Divider name="XXL" />
        <Route path="/recover-user" component={RecoverUser} />
        <Route path="/recover-password" component={RecoverPassword} />
        <Route exact path="/" component={Signin} />
        <Redirect from="*" to="/" />
        <FooterSignin />
      </div>
    </div>
  );
};
