import React, { useState } from "react";
import ArrowDown from "../../utils/Trazado 26.svg";

import "./selectMovementsProof.css";

export const SelectMovementsProof = ({
  label,
  title,
  options,
  selection,
  setSelection
}) => {
  const [openSelect, setOpenSelect] = useState(false);

  return (
    <div className="custom--select--description">
      <div className="label">{label}</div>
      <button className="button" onClick={() => setOpenSelect(x => !x)}>
        <div style={{ textAlign: "left", color: "#000", fontWeight: "700" }}>
          {selection.value === -1 ? (
            <div className="selection">{title && title}</div>
          ) : (
            selection.description
          )}
        </div>
        <ArrowDirection open={openSelect} />
      </button>

      <div className={`options ${openSelect ? "open" : ""}`}>
        {options.map((opt, i) => {
          return (
            <div
              className="item"
              key={i}
              onClick={() => {
                setSelection(opt);
                setOpenSelect(false);
              }}
            >
              {opt.description}
            </div>
          );
        })}
      </div>
    </div>
  );
};

const ArrowDirection = ({ open }) => (
  <img className={open ? "arrow-invert" : ""} src={ArrowDown} alt="..." />
);
