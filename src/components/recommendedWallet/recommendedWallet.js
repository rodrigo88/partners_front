import React, { useState } from "react";
import { setOpenBlur } from "../../redux/actions";
import closeIcon from "../../utils/icon-close.svg";
import { useDispatch } from "react-redux";
import { Button } from "../../components/button/button";
import { Divider } from "../../components/divider/divider";
import "./recommendedWallet.css";

export const RecommendedWallet = ({
  stars,
  icon,
  starIcon,
  text,
  typeProfile,
  showInfo,
  icon085,
  icon1308,
  mainImage,
  data
}) => {
  const dispatch = useDispatch();

  const [openCardDetails, setOpenCardDetails] = useState(false);

  const [viewTab, setViewTab] = useState("fondos");

  const arrayColours = [
    "#003BFB",
    "#608EF4",
    "#95BAF9",
    "#00E396",
    "#33AA82",
    "#423994",
    "#7947B3",
    "#737DFF",
    "#E6DA2C",
    "#FFF675",
    "#608EF4",
    "#00000029",
    "#707070",
    "#CECECE",
    "#AAA453"
  ];

  const { funds, assets, currencies } = data;

  const rangosFunds = funds.map((element, i) => {
    return {
      id: i + 1,
      color: arrayColours[i],
      name: element.name,
      percentage: parseInt(element.percentage)
    };
  });

  const rangosAssets = assets.map((element, i) => {
    return {
      id: i + 1,
      color: arrayColours[i],
      name: element.name,
      percentage: parseInt(element.percentage)
    };
  });

  return (
    <div className="recommendedWallet-container">
      <div className="recommendedWallet-card-header">
        <div>
          {stars.map(number => (
            <img
              style={{ marginRight: "5px" }}
              key={number}
              src={starIcon}
              alt="..."
            />
          ))}
        </div>
        <div>
          <img src={icon} alt="..." />
        </div>
      </div>
      <div className="recommendedWallet-card-sub-header">{text}</div>
      <div className="recommendedWallet-card-description">
        <div className="recommendedWallet-card-title">
          <div>{typeProfile}</div>
          <div>
            <img
              src={icon085}
              style={{
                height: "20px",
                background: "#0AB8B2",
                borderRadius: "5px"
              }}
              alt="..."
            />
            <img src={icon1308} style={{ height: "20px" }} alt="..." />
          </div>
        </div>
        <div className="recommendedWallet-card-image">
          <img src={mainImage} style={{ height: "120px" }} alt="..." />
        </div>
      </div>
      <div
        disabled={openCardDetails}
        className="recommendedWallet-card-footer"
        onClick={() => {
          dispatch(setOpenBlur());
          setOpenCardDetails(true);
        }}
      >
        {showInfo}
      </div>
      {/* ************************************************************************ */}
      {/* ************************************************************************ */}
      {/* ************************************************************************ */}
      <RecommendedWalletDetailModal
        rangosFunds={rangosFunds}
        rangosAssets={rangosAssets}
        viewTab={viewTab}
        setViewTab={setViewTab}
        setOpenCardDetails={setOpenCardDetails}
        openCardDetails={openCardDetails}
        typeProfile={typeProfile}
      />
    </div>
  );
};

const RecommendedWalletDetailModal = ({
  viewTab,
  setViewTab,
  setOpenCardDetails,
  openCardDetails,
  rangosFunds,
  rangosAssets,
  typeProfile
}) => {
  const dispatch = useDispatch();

  const [staticOver, setStaticOver] = useState("");

  return (
    <div
      className={`recommendedWallet-card-details ${
        openCardDetails ? "open-card-details" : ""
      }`}
    >
      <div className="recommendedWallet-card-details-header">
        <div className="recommendedWallet-card-details-header-tabs">
          <div
            className={`recommendedWallet-selected-tab ${
              viewTab === "fondos" ? "open-tab" : ""
            }`}
            onClick={() => setViewTab("fondos")}
          >
            CARTERA DE FONDOS
          </div>
          <Divider name="L" />
          <div
            className={`recommendedWallet-selected-tab ${
              viewTab !== "fondos" ? "open-tab" : ""
            }`}
            onClick={() => setViewTab("cedears")}
          >
            CARTERA DE CEDEARS
          </div>
        </div>
        <img
          onClick={() => {
            dispatch(setOpenBlur());
            setViewTab("fondos");
            setOpenCardDetails(false);
          }}
          src={closeIcon}
          alt="..."
        />
      </div>
      <div className="recommendedWallet-card-details-line-divider" />

      <div className="recommendedWallet-card-details-statics">
        {viewTab === "fondos" ? (
          <>
            <PiePercentageStatics
              dataList={rangosFunds}
              staticOver={staticOver}
              viewTab={viewTab}
            />
            <ListPercentageStatics
              dataList={rangosFunds}
              typeProfile={typeProfile}
              viewTab={viewTab}
              setStaticOver={setStaticOver}
            />
          </>
        ) : (
          <>
            <PiePercentageStatics
              dataList={rangosAssets}
              staticOver={staticOver}
              viewTab={viewTab}
            />
            <ListPercentageStatics
              setStaticOver={setStaticOver}
              dataList={rangosAssets}
              typeProfile={typeProfile}
              viewTab={viewTab}
            />
          </>
        )}
      </div>
      <div className="recommendedWallet-card-details-btn-operar">
        <Button
          buttonStyle="btn--primary"
          buttonSize="btn--medium"
          className="btn-arreglar"
          arreglar="arreglar"
          onClick={() => console.log("OPERAR")}
        >
          QUIERO OPERAR
        </Button>
      </div>
    </div>
  );
};

const ListPercentageStatics = ({
  dataList,
  typeProfile,
  viewTab,
  setStaticOver
}) => {
  return (
    <div
      className="recommendedWallet-card-details-statics-list-content"
      onMouseOut={() => setStaticOver("")}
    >
      <div className="title">{typeProfile}</div>
      <div className="subtitle">
        {viewTab === "fondos"
          ? "Diseñá tu cartera distribuyendo tus inversiones en los siguientes fondos comunes de inversion."
          : "Diseñá tu cartera distribuyendo tus inversiones en los siguientes Cedears según categoría."}
      </div>
      <div className="recommendedWallet-card-details-statics-list-content-data">
        {dataList.map(rFund => {
          return (
            <div
              onMouseOver={() => setStaticOver(rFund)}
              className="recommendedWallet-card-details-statics-list-content-item"
              key={rFund.id}
            >
              <div
                className={`img-radio-term-wallet `}
                style={{ background: rFund.color }}
              />
              <Divider name="XS" />
              <div className="img-radio-term-wallet-data">
                <div className="img-radio-term-wallet-description">
                  {rFund.name}
                </div>
                <div className="img-radio-term-wallet-percentage">
                  {rFund.percentage}%
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

const PiePercentageStatics = ({ dataList, staticOver, viewTab }) => {
  const initialPosPercentage = id => {
    const percArray = dataList.filter(element => element.id < id);
    const sum = percArray.reduce((acc, x) => acc + x.percentage, 0);
    const result = (sum * 180) / 50;
    return { sum, result };
  };

  return (
    <div className="pie-main-container">
      <div className="my-circle-container">
        {dataList.map(({ id, color, name, percentage }) => {
          return (
            <div
              key={id}
              className={`perc-element ${
                staticOver.name !== name && staticOver !== ""
                  ? "animate-element"
                  : ""
              }`}
              style={{
                zIndex: id === dataList.length ? 0 : id,
                transform: `rotate(${initialPosPercentage(id).result}deg)`,
                background: `conic-gradient(${color} ${percentage}%, transparent ${percentage}% 100%)`
              }}
            ></div>
          );
        })}
        <div
          className={`corona-interna ${
            viewTab === "fondos" ? "big-radio" : "small-radio"
          }`}
        >
          {staticOver && (
            <>
              <div>{staticOver.name}</div>
              <div>{staticOver.percentage + " " + "%"}</div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};
