import React from "react";
import { Divider } from "../../components/divider/divider";
import iconEdit from "../../utils/icon-edit.svg";
import iconCheck1 from "../../utils/checkbox-disabled.svg";
import iconCheck2 from "../../utils/checkbox-activate.svg";
import "./operateTable.css";

export const OperateTable = ({
  dataTable,
  setSelectedCheck,
  onChangePriceRow,
  priceOperateTable
}) => {
  return (
    <div className="operate-table-view">
      {dataTable.map((element, i) => (
        <DataElement
          key={i}
          index={i}
          element={element}
          setSelectedCheck={setSelectedCheck}
          onChangePriceRow={onChangePriceRow}
          priceOperateTable={priceOperateTable}
        />
      ))}
    </div>
  );
};
/* ************************************************************ */
const DataElement = ({
  element,
  setSelectedCheck,
  index,
  priceOperateTable,
  onChangePriceRow
}) => {
  const [editableRow, setEditableRow] = React.useState(false);

  return (
    <div className={`data-element-operate ${index === 0 ? "first" : ""}`}>
      <div className={`icon-check`}>
        <img
          src={element.checked ? iconCheck2 : iconCheck1}
          alt="..."
          onClick={() => setSelectedCheck(element)}
        />
      </div>
      <div className="name">
        <div>{element.fullName}</div>
        <div>N {element.idCustomer}</div>
      </div>
      <Divider name="XXS" />
      <div className="quantity-ganancia">{element.ganancia}</div>
      <Divider name="XXS" />
      <div className="quantity-price">
        <div>{element.quantity}</div>
      </div>
      <Divider name="XXS" />
      <div className="quantity-price">
        <CustomInputPrice
          // price={priceOperateTable}
          price={element.price}
          onChange={onChangePriceRow}
          index={index}
          disabled={!editableRow}
        />
      </div>
      <Divider name="XXS" />
      <div className="quantity-limit">{element.limit}</div>
      <Divider name="XXS" />
      <div className="icon-edit">
        <img
          src={iconEdit}
          alt="..."
          onClick={() => setEditableRow(bool => !bool)}
        />
      </div>
    </div>
  );
};

const CustomInputPrice = ({ price, onChange, disabled, index }) => {
  return (
    <div className="custom-input-price">
      <input
        disabled={disabled}
        type="text"
        placeholder="$0.00"
        value={price}
        onChange={e => onChange(e, index)}
      />
    </div>
  );
};
