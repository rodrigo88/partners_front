import React from "react";
import iconSearch from "../../utils/icon-buscar.svg";
import "./inputTextLineSearch.css";

const SIZES = ["input--small", "input--medium", "input--large"];

export const InputTextLineSearch = ({
  text,
  placeholder,
  onChange,
  type,
  value,
  defaultValue,
  name,
  children,
  inputSize
}) => {
  const checkInputSize = SIZES.includes(inputSize) ? inputSize : SIZES[0];

  return (
    <div className="input-text-line-search-container">
      <img className="img-input-text-search" src={iconSearch} alt="..." />
      <input
        name={name}
        className={`input-text-line-search ${checkInputSize}`}
        text={text}
        placeholder={placeholder}
        onChange={onChange}
        type={type}
        value={value}
        defaultValue={defaultValue}
      >
        {children}
      </input>
    </div>
  );
};
