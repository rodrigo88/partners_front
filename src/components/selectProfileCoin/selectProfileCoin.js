import React, { useState } from "react";
import ArrowDown from "../../utils/Trazado 26.svg";
import "./selectProfileCoin.css";

export const SelectProfileCoin = ({
  options,
  selection,
  setSelectionCoin2
}) => {
  const [openSelect, setOpenSelect] = useState(false);

  return (
    <div>
      <button
        className="selectProfileCoin-container"
        onClick={() => setOpenSelect(x => !x)}
      >
        <div>
          {selection.value === -1 ? "Selección" : selection.description}
        </div>
        <ArrowDirection open={openSelect} />
      </button>

      <div
        className={`selectProfileCoin-options ${
          openSelect ? "open-selectProfileCoin" : ""
        }`}
      >
        {options.map((opt, i) => {
          return (
            <div
              className="option-selectProfileCoin"
              key={i}
              onClick={() => {
                setSelectionCoin2(opt);
                setOpenSelect(false);
              }}
            >
              {opt.description}
            </div>
          );
        })}
      </div>
    </div>
  );
};

const ArrowDirection = ({ open }) => (
  <img className={open ? "arrow-invert" : ""} src={ArrowDown} alt="..." />
);
