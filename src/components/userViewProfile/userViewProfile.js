import React, { useState, useEffect } from "react";
import { InputTextUserProfile } from "../inputTextUserProfile/inputTextUserProfile";
import { SelectBilling } from "../selectBilling/selectBilling";
import { SelectTextUserProfile } from "../selectTextUserProfile/selectTextUserProfile";
import { useSelector, useDispatch } from "react-redux";
import {
  // setUpdateUser,
  // setSuccesfullUpdateUser,
  setResponseBilling
} from "../../redux/actions";
import { Divider } from "../divider/divider";
import { Button } from "../../components/button/button";
import basicTabIcon from "../../utils/BasicTab.svg";
import iconProfile from "../../utils/icon-perfil2.svg";
import iconCopy from "../../utils/icon-copy.svg";
import iconConsult from "../../utils/icon-consulta.svg";
import editIcon from "../../utils/icon-edit.svg";
import trashIcon from "../../utils/icon-drash.svg";
import radioElipseIcon from "../../utils/Outer Ellipse.svg";
import primaryCbuIcon from "../../utils/Grupo 3418.svg";
import successChangeIcon from "../../utils/SUS CAMBIOS SE GUARDARON CON ÉXITO.svg";
import responseBillingIcon from "../../utils/Grupo 3443.svg";
import "./userViewProfile.css";
import {
  setMapBillingTypes,
  setMapProvinces,
  selectAccountPaymentMethod
} from "../../libs/libsUserProfile";

export const UserViewProfile = ({ openProfileView }) => {
  const dispatch = useDispatch();
  const state = useSelector(state => state);
  const {
    responseBilling,
    successfullChange,
    userLogged,
    loadingUserLogged
  } = state;

  useEffect(() => {
    async function getPartnersBillingTypes() {
      await fetch(
        "https://djufzxp9v1.execute-api.us-east-1.amazonaws.com/bi-dev/api_getbillingtype_stg"
      )
        .then(res => res.json())
        .then(response =>
          setBillingTypes(setMapBillingTypes(response.billingTypes))
        )
        .catch(err => console.log(err));
    }
    getPartnersBillingTypes();

    async function getPartnersProvinces() {
      await fetch(
        "https://djufzxp9v1.execute-api.us-east-1.amazonaws.com/bi-dev/api-getpartnerprovinces-stg"
      )
        .then(res => res.json())
        .then(response => setProvinces(setMapProvinces(response.provinces)))
        .catch(err => console.log(err));
    }
    getPartnersProvinces();
  }, []);

  useEffect(() => {
    setUser(userLogged);
  }, [userLogged]);

  const [user, setUser] = useState();
  const [billingTypes, setBillingTypes] = useState([]);
  const [provinces, setProvinces] = useState([]);

  const setSelectionProvince = opt => {
    setUser({
      ...user,
      province: {
        codeProvince: opt.value,
        nameProvince: opt.description
      }
    });
  };

  const setSelectionBilling = opt => {
    setUser({
      ...user,
      billingDetails: {
        businessName: userLogged.billingDetails.businessName,
        cuit: userLogged.billingDetails.cuit,
        billingType: {
          codeBillingType: opt.value,
          descBilling: opt.description
        }
      }
    });
  };

  const [editModeInput, setEditModeInput] = useState("");

  const setEditMode = nameInput => {
    editModeInput === nameInput
      ? setEditModeInput("")
      : setEditModeInput(nameInput);
  };

  const onChange = e => {
    const nameInputBilling = e.target.name;
    if (nameInputBilling === "cuit") {
      setUser({
        ...user,
        billingDetails: {
          businessName: user.billingDetails.businessName,
          cuit: e.target.value,
          billingType: {
            codeBillingType: user.billingDetails.billingType.codeBillingType,
            descBilling: user.billingDetails.billingType.descBilling
          }
        }
      });
    } else if (nameInputBilling === "businessName") {
      setUser({
        ...user,
        billingDetails: {
          cuit: user.billingDetails.cuit,
          businessName: e.target.value,
          billingType: {
            codeBillingType: user.billingDetails.billingType.codeBillingType,
            descBilling: user.billingDetails.billingType.descBilling
          }
        }
      });
    } else {
      setUser({ ...user, [e.target.name]: e.target.value });
    }
  };

  const [helpAddCbu, setHelpAddCbu] = useState(false);

  const [listCbuAccounts, setListCbuAccounts] = useState([
    {
      id: 0,
      typeAccount: "CA - ARS",
      bank: "Banco BBVA",
      primary: true,
      cbuValue: "22222222 222222222222 2"
    },
    {
      id: 1,
      typeAccount: "CC - ARS",
      bank: "Banco Santander",
      primary: false,
      cbuValue: "22222222 222222222222 2"
    }
  ]);

  const deleteAccount = id => console.log(id);

  const selectAccountPayment = id => {
    setListCbuAccounts(selectAccountPaymentMethod(id, listCbuAccounts));
  };

  const addAccount = () => {
    const newAccount = {
      id: 1000 + Math.floor((100000 - 1000) * Math.random()),
      typeAccount: "CC - USD",
      bank: "Banco Provincia",
      cbuValue: "22222222 222222222222 2",
      primary: false
    };
    setListCbuAccounts([...listCbuAccounts, newAccount]);
  };

  const updateUser = u => console.log(u);

  const updateBillingPayment = u => dispatch(setResponseBilling());

  const displayHelpAddCbu = () => setHelpAddCbu(x => !x);

  return (
    <div
      className={`user-profile-view-container ${
        openProfileView ? "openProfileView" : ""
      }`}
    >
      {loadingUserLogged ? (
        ""
      ) : (
        <>
          ""
          {/* <UserProfileViewContainerHeader user={user} />

          <DividerLine />

          <div className="user-profile-view-container-personal-data">
            <UserProfilePersonalDataSection
              editModeInput={editModeInput}
              setEditMode={setEditMode}
              onChange={onChange}
              user={user}
            />

            <UserProfileFiscalData
              editModeInput={editModeInput}
              setEditMode={setEditMode}
              onChange={onChange}
              user={user}
              provincesOptions={provinces}
              setSelectionProvince={setSelectionProvince}
            />
          </div>

          <div className="confirm-section-personal-data">
            <div>
              {successfullChange && <img src={successChangeIcon} alt="..." />}
            </div>
            <Divider name="L" />
            <Button
              onClick={() => updateUser(user)}
              type="button"
              buttonStyle="btn--primary"
              buttonSize="btn--medium"
            >
              GUARDAR CAMBIOS
            </Button>
          </div>

          <DividerLine />

          <div className="user-profile-view-container-transaction-data">
            <UserProfilePayment
              UserProfilePayment={UserProfilePayment}
              listCbuAccounts={listCbuAccounts}
              userLogged={userLogged}
              deleteAccount={deleteAccount}
              selectAccountPayment={selectAccountPayment}
              addAccount={addAccount}
              displayHelpAddCbu={displayHelpAddCbu}
              helpAddCbu={helpAddCbu}
            />

            <UserProfileFactura
              responseBilling={responseBilling}
              options={billingTypes}
              selectionBilling={{
                value: user.billingDetails.billingType.codeBillingType,
                description: user.billingDetails.billingType.descBilling
              }}
              setSelectionBilling={setSelectionBilling}
              editModeInput={editModeInput}
              setEditMode={setEditMode}
              onChange={onChange}
              user={user}
              updateBillingPayment={updateBillingPayment}
            />
          </div> */}
        </>
      )}
    </div>
  );
};

/* **************************************************************** */

const UserProfileViewContainerHeader = ({ user }) => {
  return (
    <div className="user-profile-view-container-header">
      <div className="user-profile-view-container-header-logo">
        <img src={iconProfile} alt="..." />
      </div>
      <div className="user-profile-view-container-header-info">
        <p className="user-profile-view-container-header-info-name">
          Nombre de Persona física o Jurídica: {user.name}
        </p>
        <p className="user-profile-view-container-header-info-email">
          mailpersona@ad-cap.com.ar
          {user.email}
        </p>
        <p className="user-profile-view-container-header-info-codRef">
          Cód. de Referido: {user.referredCode}
        </p>
      </div>
    </div>
  );
};

/* **************************************************************** */

const DividerLine = () => (
  <img style={{ width: "100%" }} src={basicTabIcon} alt="..." />
);

/* **************************************************************** */
const UserProfilePersonalDataSection = ({
  editModeInput,
  setEditMode,
  onChange,
  user
}) => {
  return (
    <div className="user-profile-view-container-personal-data-section-1">
      <div className="section-title-user-profile">DATOS PERSONALES</div>

      <InputTextUserProfile
        label="Celular"
        name="mobile"
        type="text"
        editable="true"
        editModeInput={editModeInput}
        setEditMode={setEditMode}
        onChange={onChange}
        value={user.mobile}
      />

      <InputTextUserProfile
        label="Teléfono"
        name="phone"
        type="text"
        editable="true"
        editModeInput={editModeInput}
        setEditMode={setEditMode}
        onChange={onChange}
        value={user.phone}
      />
      <InputTextUserProfile
        label="DNI"
        name="documentNumber"
        type="text"
        editable="false"
        onChange={onChange}
        value={user.documentNumber}
      />
      <InputTextUserProfile
        label="Fecha de nacimiento"
        name="birthDate"
        type="text"
        editable="false"
        onChange={onChange}
        value={user.birthDate}
      />
    </div>
  );
};
/* **************************************************************** */

const UserProfileFiscalData = ({
  editModeInput,
  setEditMode,
  onChange,
  user,
  provincesOptions,
  setSelectionProvince
}) => {
  return (
    <div className="user-profile-view-container-personal-data-section-2">
      <div className="section-title-user-profile">DIRECCION FISCAL</div>
      <InputTextUserProfile
        label="Calle"
        name="street"
        type="text"
        editable="true"
        editModeInput={editModeInput}
        setEditMode={setEditMode}
        onChange={onChange}
        value={user.street}
      />
      <div className="section-title-user-profile-numbers">
        <InputTextUserProfile
          label="Numero"
          name="number"
          type="text"
          editable="true"
          editModeInput={editModeInput}
          setEditMode={setEditMode}
          onChange={onChange}
          value={user.number}
        />
        <InputTextUserProfile
          label="Piso"
          name="floor"
          type="text"
          editable="true"
          editModeInput={editModeInput}
          setEditMode={setEditMode}
          onChange={onChange}
          value={user.floor}
        />
        <InputTextUserProfile
          label="Dpto"
          name="department"
          type="text"
          editable="true"
          editModeInput={editModeInput}
          setEditMode={setEditMode}
          onChange={onChange}
          value={user.department}
        />
      </div>
      <div className="section-title-user-profile-location">
        <div className="section-title-user-profile-location-loc">
          <InputTextUserProfile
            label="Localidad"
            name="location"
            type="text"
            editable="true"
            editModeInput={editModeInput}
            setEditMode={setEditMode}
            onChange={onChange}
            value={user.location}
          />
        </div>

        <div className="section-title-user-profile-location-cp">
          <InputTextUserProfile
            label="C.P"
            name="postalCode"
            type="text"
            editable="true"
            editModeInput={editModeInput}
            setEditMode={setEditMode}
            onChange={onChange}
            value={user.postalCode}
          />
        </div>
      </div>

      <SelectTextUserProfile
        options={provincesOptions}
        selection={{
          value: user.province.codeProvince,
          description: user.province.nameProvince
        }}
        setSelectionProvince={setSelectionProvince}
      />
    </div>
  );
};

/* **************************************************************** */

const UserProfilePayment = ({
  responseBilling,
  listCbuAccounts,
  userLogged,
  deleteAccount,
  selectAccountPayment,
  addAccount,
  displayHelpAddCbu,
  helpAddCbu
}) => {
  return (
    <div className="user-profile-view-container-transaction-data-payment">
      <div className="transaction-data-payment-header">
        <div className="section-title-user-profile">
          MEDIO DE PAGO POR DEFECTO
        </div>
      </div>
      <div className="transaction-data-payment-add-cbu">
        <div className="transaction-data-payment-left">
          <div
            style={{
              marginRight: "3px",
              fontSize: "14px",
              fontWeight: "600"
            }}
          >
            CBU PRINCIPAL
          </div>
          <img src={iconCopy} alt="..." />
        </div>
        <div className="transaction-data-payment-right">
          <div
            className={`add-new-cbu-option ${
              listCbuAccounts.length === 3 ? "inactive" : ""
            }`}
            onClick={() => addAccount()}
          >
            + AGREGAR CBU
          </div>

          <img
            onClick={() => displayHelpAddCbu()}
            className="help-add-cbu-tooltip"
            src={iconConsult}
            alt="..."
          />
          {helpAddCbu && (
            <div className="tooltip-add-cbu">
              <p>
                Sólo pueden agregarse hasta 3 CBUs en su cuenta Partner,
                pudiendo seleccionar 1 por default.
              </p>
            </div>
          )}
        </div>
      </div>
      <div className="transaction-data-payment-list-cbu">
        {userLogged.paymentMethods &&
          userLogged.paymentMethods.map((cbuAccount, i) => (
            <Account
              account={cbuAccount}
              deleteAccount={deleteAccount}
              selectAccountPayment={selectAccountPayment}
              key={i}
            />
          ))}
      </div>
    </div>
  );
};

/* **************************************************************** */

const Account = ({ account, deleteAccount, selectAccountPayment }) => {
  const {
    idPayment,
    accountType,
    bank,
    cbu,
    isPrincipal,
    currencyType
  } = account;

  return (
    <div className="cbu-account">
      <div className="cbu-account-container">
        <div className="cbu-account-container-radio">
          <img
            onClick={() => selectAccountPayment(idPayment)}
            className={`img-radio-account ${isPrincipal ? "primary" : ""}`}
            src={radioElipseIcon}
            alt="..."
          />
        </div>
        <div className="cbu-account-container-data">
          <div className="cbu-account-container-data-names">
            {accountType.nameAccountType} - {currencyType.nameCurrency} |{" "}
            {bank.nameBank}
            <Divider name="XS" />
            {isPrincipal && <img src={primaryCbuIcon} alt="..." />}
          </div>
          <div className="cbu-account-container-data-options">
            <div className="data-options-cbu-value">{cbu}</div>
            <div className="data-options-icons">
              <img src={editIcon} alt="..." />
              <Divider name="XS" />
              <img
                style={{ pointerEvents: isPrincipal && "none" }}
                onClick={() => !isPrincipal && deleteAccount(idPayment)}
                src={trashIcon}
                alt="..."
              />
            </div>
          </div>
        </div>
      </div>

      <div className="divider-line-black">
        <DividerLine />
      </div>
    </div>
  );
};

/* **************************************************************** */

const UserProfileFactura = ({
  responseBilling,
  options,
  selectionBilling,
  setSelectionBilling,
  editModeInput,
  setEditMode,
  onChange,
  user,
  updateBillingPayment
}) => {
  return (
    <div
      className={`user-profile-view-container-transaction-data-billing ${
        responseBilling ? "billingInactive" : ""
      }`}
    >
      <div className="transaction-data-payment-header">
        <div className="section-title-user-profile">DATOS DE FACTURACION</div>
        <div>
          {responseBilling && <img src={responseBillingIcon} alt="..." />}
        </div>
      </div>
      <div className="transaction-data-payment-select">
        <div className="section-title-user-profile-payment">
          Tipo de Factura
        </div>
        <SelectBilling
          options={options}
          selection={selectionBilling}
          setSelection={setSelectionBilling}
        />
      </div>
      <div className="transaction-data-payment-edit">
        <InputTextUserProfile
          label="CUIT"
          name="cuit"
          type="text"
          editable="true"
          editModeInput={editModeInput}
          setEditMode={setEditMode}
          onChange={onChange}
          value={user.billingDetails.cuit}
        />
        <Divider name="XXL" />
        <InputTextUserProfile
          label="Razón Social"
          name="businessName"
          type="text"
          editModeInput={editModeInput}
          setEditMode={setEditMode}
          editable="true"
          onChange={onChange}
          value={user.billingDetails.businessName}
        />
      </div>
      <Divider name="XS" />
      <div className="confirm-section-payment-data">
        <Button
          onClick={() => updateBillingPayment(user)}
          type="button"
          buttonStyle="btn--secondary"
          buttonSize="btn--medium"
        >
          SOLICITAR CAMBIOS
        </Button>
      </div>
    </div>
  );
};
