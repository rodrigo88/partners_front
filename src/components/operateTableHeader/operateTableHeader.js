import React from "react";
import iconCheck1 from "../../utils/checkbox-disabled.svg";
import iconCheck2 from "../../utils/checkbox-activate.svg";
import "./operateTableHeader.css";

export const OperateTableHeader = ({
  checkAll,
  selectAllListElement,
  dataTable,
  selectedInstrument
}) => {
  const everyChecked = dataTable.every(element => element.checked);
  return (
    <div className="operate-table-header-container">
      <div className={`check `} onClick={() => selectAllListElement()}>
        <img
          src={checkAll || everyChecked ? iconCheck2 : iconCheck1}
          alt="..."
        />
      </div>
      <div className="name">SELECCIONAR TODOS LOS CLIENTES</div>
      <div className="ganancia">% GANANCIA</div>
      <div className="quantity">
        {selectedInstrument.description === "Fondos"
          ? "CUOTAPARTES"
          : "DIGITE CANTIDAD"}
      </div>
      <div className="price">DIGITE PRECIO</div>
      <div className="limit">PRECIO LIMITE</div>
    </div>
  );
};
