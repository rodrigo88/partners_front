import React, { useState } from "react";
import ArrowDown from "../../utils/Trazado 26.svg";
import "./selectProfile.css";

export const SelectProfile = ({ options, selection, setSelection2 }) => {
  const [openSelect, setOpenSelect] = useState(false);

  return (
    <div>
      <button
        className="select-container"
        onClick={() => setOpenSelect(x => !x)}
      >
        <div>
          {selection.value === -1 ? "Selección" : selection.description}
        </div>
        <ArrowDirection open={openSelect} />
      </button>

      <div className={`select-options ${openSelect ? "open-select" : ""}`}>
        {options.map((opt, i) => {
          return (
            <div
              className="option-select"
              key={i}
              onClick={() => {
                setSelection2(opt);
                setOpenSelect(false);
              }}
            >
              {opt.description}
            </div>
          );
        })}
      </div>
    </div>
  );
};

const ArrowDirection = ({ open }) => (
  <img className={open ? "arrow-invert" : ""} src={ArrowDown} alt="..." />
);
