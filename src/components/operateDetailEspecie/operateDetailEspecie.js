import React from "react";
import { Divider } from "../divider/divider";
import { useSelector } from "react-redux";
import { DetailFundEspecie } from "../detailFundEspecie/detailFundEspecie";
import { OperateDetailEspecie2 } from "../../components/operateDetailEspecie2/operateDetailEspecie2";
import { Button } from "../button/button";
import { OperateScheduleView } from "../operateScheduleView/operateScheduleView";
import "./operateDetailEspecie.css";

export const OperateDetailEspecie = ({
  selectedInstrument,
  activeDetailCard,
  selectedEspecie,
  selectedEspecieToShow,
  selectedWallet
}) => {
  const state = useSelector(state => state);

  const { recommendedWallets, detailEspecie, loadingDetailEspecie } = state;

  const open = selectedEspecie && selectedEspecie.description !== "";

  const arrayColours = [
    "#003BFB",
    "#608EF4",
    "#95BAF9",
    "#00E396",
    "#33AA82",
    "#423994",
    "#7947B3",
    "#737DFF",
    "#E6DA2C",
    "#FFF675",
    "#608EF4",
    "#00000029",
    "#707070",
    "#CECECE",
    "#AAA453"
  ];

  const rangosFunds = () => {
    let data;
    const wallet = selectedWallet && selectedWallet.description;
    switch (wallet) {
      case "Cartera Fondos Conservador":
        data = recommendedWallets.agresiva;
        break;
      case "Cartera Fondos Moderado":
        data = recommendedWallets.moderada;
        break;
      default:
        data = recommendedWallets.conservadora;
        break;
    }

    return data.funds.map((element, i) => {
      return {
        id: i + 1,
        color: arrayColours[i],
        name: element.name,
        percentage: parseInt(element.percentage)
      };
    });
  };

  return (
    <div
      className={`operate-detail-especie-container 
      ${activeDetailCard || open ? "active-detail-card" : ""}
      ${selectedWallet.value !== -1 ? "fondoblancopapa" : ""}
      `}
    >
      <OperateScheduleView
        selectedInstrument={selectedInstrument}
        selectedWallet={selectedWallet}
      />
      <div className="container-ahora">
        {detailEspecie ? (
          loadingDetailEspecie ? (
            "Cargando"
          ) : selectedEspecieToShow && selectedEspecieToShow.type === "FUND" ? (
            <div style={{ width: "90%" }}>
              <DetailFundEspecie detailEspecie={detailEspecie} />
            </div>
          ) : (
            <div style={{ width: "100%" }}>
              <OperateDetailEspecie2 detailEspecie={detailEspecie} />
            </div>
          )
        ) : (
          selectedWallet &&
          selectedWallet.value !== -1 && (
            <CarteraRecomendadaOperateDetail
              dataList={rangosFunds()}
              selectedWallet={selectedWallet}
            />
          )
        )}
      </div>
    </div>
  );
};
/* ****************************************************** */
const CarteraRecomendadaOperateDetail = ({ dataList, selectedWallet }) => {
  const { description } = selectedWallet;
  const headerTitle = () => {
    const title = description.substr(14, 25);
    return `Cartera Recomendada: Perfil ${title}`;
  };
  return (
    <>
      <div className="title-ahora">{headerTitle()}</div>
      <PiePercentageStatics dataList={dataList} />
      <ListPercentageStatics dataList={dataList} />
      <Button type="button" buttonStyle="btn--primary" arreglar="arreglar2">
        AMPLIAR INFO
      </Button>
    </>
  );
};

/* ****************************************************** */
export const PiePercentageStatics = ({ dataList }) => {
  const initialPosPercentage = id => {
    const percArray = dataList.filter(element => element.id < id);
    const sum = percArray.reduce((acc, x) => acc + x.percentage, 0);
    const result = (sum * 180) / 50;
    return { sum, result };
  };

  return (
    <div className="operate-pie-main-container">
      <div className="operate-my-circle-container">
        {dataList.map(({ id, color, name, percentage }) => {
          return (
            <div
              key={id}
              className={`perc-element `}
              style={{
                zIndex: id === dataList.length ? 0 : id,
                transform: `rotate(${initialPosPercentage(id).result}deg)`,
                background: `conic-gradient(${color} ${percentage}%, transparent ${percentage}% 100%)`
              }}
            ></div>
          );
        })}
        <div className={`operate-corona-interna`}></div>
      </div>
    </div>
  );
};
/* ****************************************************** */
const ListPercentageStatics = ({ dataList }) => {
  return (
    <div className="detail-cartera-list">
      {dataList.map(rFund => {
        return (
          <div className="element" key={rFund.id}>
            <div className="point-icon" style={{ background: rFund.color }} />
            <Divider name="S" />
            <div className="description">{rFund.name}</div>
            <div className="percentage">{rFund.percentage}%</div>
          </div>
        );
      })}
    </div>
  );
};
