import React from "react";
import { InputTextClientsWallet } from "../../components/inputTextClientsWallet/inputTextClientsWallet";
import { SelectClientsWallet } from "../../components/selectClientsWallet/selectClientsWallet";
import { Divider } from "../divider/divider";
import iconFilter from "../../utils/icon-filtrar.svg";
import iconGroup from "../../utils/icon-grupo.svg";
import "./treasurySelections.css";

export const TreasurySelections = ({ search, onChange }) => {
  return (
    <div className="treasury-selections-container">
      <InputTextClientsWallet
        type="text"
        name="name"
        placeholder="Apellido y Nombre"
        value={search.name}
        onChange={onChange}
        inputSize="input--medium"
      />
      <Divider name="M" />
      <InputTextClientsWallet
        type="text"
        name="cuit"
        placeholder="CUIT/ CUIL"
        value={search.cuit}
        onChange={onChange}
        inputSize="input--medium"
      />
      <Divider name="M" />
      <SelectClientsWallet
        icon={iconGroup}
        title=""
        options={[]}
        selection={""}
        // setSelection={setSelectedGroup}
      />
      <Divider name="M" />
      <SelectClientsWallet
        icon={iconFilter}
        title="Instrumento"
        options={[]}
        selection={""}
        // setSelection={setSelectedInstrument}
        // disabledSelect={selectedWallet.description !== ""}
        sizeList="156px"
        sizeContainer="128px"
      />
      <Divider name="M" />
      <SelectClientsWallet
        icon={iconFilter}
        title="Especie"
        options={[]}
        selection={""}
        // setSelection={setSelectedEspecie}
        // disabledSelect={selectedInstrument.description === ""}
        sizeList="256px"
        sizeContainer="120px"
      />
    </div>
  );
};
