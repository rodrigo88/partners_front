import React from "react";
import iconSearch from "../../utils/icon-buscar.svg";
import iconClean from "../../utils/Grupo 4187.svg";
import "./inputTextSearchClean.css";

const SIZES = ["input--small--10", "input--medium--10", "input--large--100"];

export const InputTextSearchClean = ({
  text,
  placeholder,
  onChange,
  type,
  value,
  defaultValue,
  name,
  inputSize,
  setCleanInput
}) => {
  const checkInputSize = SIZES.includes(inputSize) ? inputSize : SIZES[0];
  const hasValue = () => value !== "";

  return (
    <div
      className={`custom--search--clean--container ${
        hasValue() ? "no-empty" : ""
      } ${checkInputSize}`}
    >
      <div className="custom--search--clean--iconSearch">
        <img src={iconSearch} alt="..." />
      </div>
      <input
        name={name}
        className={`custom--search--clean--input`}
        text={text}
        placeholder={placeholder}
        onChange={onChange}
        type={type}
        value={value}
        defaultValue={defaultValue}
      />
      <div className="custom--search--clean--iconClean">
        {hasValue() && (
          <img alt="..." src={iconClean} onClick={setCleanInput} />
        )}
      </div>
    </div>
  );
};
