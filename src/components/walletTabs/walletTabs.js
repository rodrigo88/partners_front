import React from "react";
import { Divider } from "../divider/divider";
import iconSend from "../../utils/icon-send.svg";
import iconDownload from "../../utils/icon-download.svg";
import iconPrint from "../../utils/icon-print.svg";
// import { useSelector } from "react-redux";
import "./walletTabs.css";

export const WalletTabs = ({ option, setOption, selectedClientDashboard }) => {
  return (
    <div className="wallet-tabs-container">
      {/* <Divider name="XL" /> */}
      <div
        className={`option ${option === "TENENCIA" ? "selected" : ""}`}
        onClick={() => setOption("TENENCIA")}
      >
        {" "}
        TENENCIA{" "}
      </div>
      <Divider name="XXS" />
      <div
        className={`option ${option === "FEE" ? "selected" : ""}`}
        onClick={() => setOption("FEE")}
      >
        {" "}
        FEE{" "}
      </div>
      {selectedClientDashboard && (
        <div
          className={`option ${
            option === "CUENTA CORRIENTE" ? "selected" : ""
          }`}
          onClick={() => setOption("CUENTA CORRIENTE")}
        >
          {" "}
          CUENTA CORRIENTE{" "}
        </div>
      )}

      <div className="resume">
        Resumen
        <Divider name="S" />
        <img src={iconSend} alt="..." />
        <Divider name="S" />
        <img src={iconDownload} alt="..." />
        <Divider name="S" />
        <img src={iconPrint} alt="..." />
        <Divider name="M" />
      </div>
      <div className="line" />
    </div>
  );
};
