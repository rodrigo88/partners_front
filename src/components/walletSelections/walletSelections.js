import React from "react";
import { InputTextClientsWallet } from "../inputTextClientsWallet/inputTextClientsWallet";
import { SelectClientsWallet } from "../selectClientsWallet/selectClientsWallet";
import { Divider } from "../divider/divider";
import iconFilter from "../../utils/icon-filtrar.svg";
import iconSend from "../../utils/icon-send.svg";
import iconDownload from "../../utils/icon-download.svg";
import iconPrint from "../../utils/icon-print.svg";
import iconGroup from "../../utils/icon-grupo.svg";
import "./walletSelections.css";

export const WalletSelections = ({ search, onChange }) => {
  return (
    <div className="wallet-selections-container">
      <div>
        <Divider name="M" />
        <div className="wallet-selections-container-title">Filtros</div>
        <Divider name="M" />
        <SelectClientsWallet
          icon={iconFilter}
          title="Instrumento"
          options={[]}
          selection={""}
          // setSelection={setSelectedInstrument}
          // disabledSelect={selectedWallet.description !== ""}
          sizeList="156px"
          sizeContainer="128px"
        />
        <Divider name="M" />
        <SelectClientsWallet
          icon={iconFilter}
          title="Especie"
          options={[]}
          selection={""}
          // setSelection={setSelectedEspecie}
          // disabledSelect={selectedInstrument.description === ""}
          sizeList="256px"
          sizeContainer="120px"
        />
      </div>

      <div>
        <img src={iconDownload} alt="..." />
        <Divider name="S" />
        <img src={iconPrint} alt="..." />
        <Divider name="M" />
        <Divider name="M" />
      </div>
    </div>
  );
};
