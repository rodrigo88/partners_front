import React, { useState } from "react";
import { Button } from "../button/button";
import { Divider } from "../divider/divider";
import iconExpand from "../../utils/icon-ampliar.svg";
import downArrow from "../../utils/DownArrow.svg";
import upArrow from "../../utils/UpArrow.svg";
import iconDots from "../../utils/hamburger-button.svg";
import "./clientWallet.css";

export const ClientWallet = ({
  client,
  openId,
  setOpenOptionsClient,
  closeOptions
}) => {
  const {
    idCustomer,
    aumArs,
    aumUsd,
    tmArs,
    tmUsd,
    fee,
    name,

    // legalPerson,
    riskategory,

    dateBorn,
    energy
  } = client;

  const [expandClient, setExpandClient] = useState(false);

  // const exactValue = energy >= 50 ? -90 + energy : 90 - energy;

  return (
    <div
      className={`clientWallet-container ${expandClient ? "expandClient" : ""}`}
    >
      <div className="clientWallet-items-first">
        <div className="clientWallet-item-fullname">
          <img
            className={`arrowIcon ${expandClient ? "invertArrow" : ""}`}
            onClick={() => setExpandClient(x => !x)}
            src={iconExpand}
            alt="..."
          />
          <div>{name}</div>
        </div>
        <div className="clientWallet-item-data-1">
          {aumArs}
          <Divider name="XXS" />
          <img src={upArrow} alt="..." />
        </div>
        <div className="clientWallet-item-data-1">
          {aumUsd}
          <Divider name="XXS" />
          <img src={downArrow} alt="..." />
        </div>
        <div className="clientWallet-item-data-1">
          {tmArs}
          <Divider name="XXS" />
          <img src={downArrow} alt="..." />
        </div>
        <div className="clientWallet-item-data-1">
          {tmUsd}
          <Divider name="XXS" />
          <img src={downArrow} alt="..." />
        </div>
        <div className="clientWallet-item-data-1">
          {fee}
          <Divider name="XXS" />
          <img src={upArrow} alt="..." />
        </div>
        <div
          className="clientWallet-item-action"
          onClick={() => {
            if (openId === idCustomer) {
              closeOptions();
            } else {
              setOpenOptionsClient(idCustomer);
            }
          }}
        >
          <figure>
            <img src={iconDots} alt="..." />
          </figure>
        </div>
      </div>

      <div className="clientWallet-items-second">
        <div className="items-second-names">
          <div className="clientWallet-item-data-2">
            {riskategory.description} {idCustomer}
          </div>
          <div className="clientWallet-item-data-2">
            {client.legalPerson
              ? client.legalPerson.email
              : client.physicalPerson.email}
          </div>
          <div className="clientWallet-item-data-2">
            {client.legalPerson
              ? client.legalPerson.address
              : client.physicalPerson.address}
          </div>
        </div>
        <div className="items-second-numbers">
          <div className="clientWallet-item-data-2">
            PHONE:{" "}
            {client.legalPerson
              ? client.legalPerson.phone
              : client.physicalPerson.phone}
          </div>
          <div className="clientWallet-item-data-2">
            {client.legalPerson
              ? "CUIT: " + client.legalPerson.cuit
              : "DNI: " + client.physicalPerson.dni}
          </div>
          <div className="clientWallet-item-data-2">
            {client.physicalPerson &&
              "Birth: " + client.physicalPerson.birthDate}
          </div>
        </div>
        <div className="items-second-graphics-container">
          <div className="items-second-graphics-limits">
            <div>0</div>
            <div className="items-second-graphics">
              <div
                className="items-second-graphics-internal"
                style={{
                  transform: `rotate(${parseInt(
                    riskategory.description.codeCategory
                  )}deg)`
                }}
              >
                <div className="cursor-graphics"></div>
              </div>
            </div>
            <div>100</div>
          </div>
          <div className="items-second-graphics-quality">
            <div>Conservador</div>
            <div>Mod</div>
            <div>Agresivo</div>
          </div>
        </div>
        {/* ****************************************************** */}
        <div className="items-second-buttons">
          <Button
            onClick={() => console.log("OPERAR")}
            type="button"
            buttonStyle="btn--primary"
            buttonSize="btn--small"
          >
            OPERAR
          </Button>
          <Button
            onClick={() => console.log("OPERAR")}
            type="button"
            buttonStyle="btn--secondary"
            buttonSize="btn--small"
          >
            Ver ficha
          </Button>
        </div>
      </div>
    </div>
  );
};
