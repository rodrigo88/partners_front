import React from "react";
import iconSearch from "../../utils/icon-buscar.svg";
import "./inputTextMovementsProof.css";

const SIZES = ["input--small", "input--medium", "input--large"];

export const InputTextMovementsProof = ({
  text,
  placeholder,
  onChange,
  type,
  value,
  defaultValue,
  name,
  label,
  children
  // inputSize
}) => {
  // const checkInputSize = SIZES.includes(inputSize) ? inputSize : SIZES[0];

  return (
    <div className="custom-input-description">
      <div className="label">{label}</div>
      <div className="content1">
        <img src={iconSearch} alt="..." />
        <input
          name={name}
          text={text}
          placeholder={placeholder}
          onChange={onChange}
          type={type}
          value={value}
          defaultValue={defaultValue}
        >
          {children}
        </input>
      </div>
    </div>
  );
};
