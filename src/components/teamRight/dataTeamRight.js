export const optionsProducer = [
  { value: 0, description: "Rodrigo Castro" },
  { value: 1, description: "Eduardo Toledo" },
  { value: 2, description: "Nicolas Higashi" },
  { value: 3, description: "Federico Varela" },
  { value: 4, description: "Adriano Barsanti" }
];

export const optionsTerm = [
  { value: 0, description: "Última semana" },
  { value: 1, description: "Últimos 30 días" },
  { value: 2, description: "Últimos 60 días" },
  { value: 3, description: "Año Calendario" }
];
