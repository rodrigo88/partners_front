import React, { useState } from "react";
import { Divider } from "../divider/divider";
// import { useSelector } from "react-redux";
// import { WalletTabs } from "../walletTabs/walletTabs";
// import { LoadingView } from "../../views/portfolio/portfolio";
// import { WalletCardPie } from "../walletCardPie/walletCardPie";
// import { WalletMiniCard } from "../walletMiniCard/walletMiniCard";
// import { WalletSelections } from "../walletSelections/walletSelections";
// import { WalletHeaderData } from "../walletHeaderData/walletHeaderData";
// import { WalletData } from "../walletData/walletData";
// import iconAmp from "../../utils/icon-ampliar.svg";
// import iconReduce from "../../utils/icon-ampliar (1).svg";
// import iconTicket from "../../utils/icon-ticket2.svg";
// import iconChartBlue from "../../utils/Grupo 4164.svg";
// import iconPerfil from "../../utils/account_circle-24px.svg";
// import iconPerfil2 from "../../utils/icon-perfil.svg";
import iconShowProof from "../../utils/icon-cta-informefacturas.svg";
import iconDownloadFiles from "../../utils/icon-cta-descargarecibos.svg";
import { SelectSimple } from "../selectSimple/selectSimple";
import { BigButtonForModal } from "../bigButtonForModal/bigButtonForModal";
import { optionsProducer, optionsTerm } from "./dataTeamRight";
import "./teamRight.css";

export const TeamRight = () => {
  // const state = useSelector(state => state);
  // const [openModalProof, setOpenModalProof] = useState(false);
  // const [openModalExtract, setOpenModalExtract] = useState(false);

  const [selectionProducer, setSelectionProducer] = React.useState(null);
  const [selectionTerm, setSelectionTerm] = React.useState(null);

  return (
    <div className="team-dashboard-container-right">
      <Divider name="XXL" />
      <div className="team-dashboard-container-right-buttons">
        <BigButtonForModal
          icon={iconShowProof}
          title="INFORME SU FACTURA"
          mainColor="white"
          onClick={() => console.log("ALFA")}
        />
        <Divider name="M" />
        <BigButtonForModal
          icon={iconDownloadFiles}
          title="DESCARGA DE RECIBOS"
          mainColor="blue"
          onClick={() => console.log("ALFA")}
        />
      </div>
      <Divider name="M" />
      <div className="team-dashboard-container-right-selections">
        <div className="team-dashboard-container-right-selections-title">
          Carteras totales
        </div>
        <SelectSimple
          title="Seleccione productor"
          placeholderSelection="Filtrar por Productor"
          options={optionsProducer}
          selection={selectionProducer}
          setSelection={setSelectionProducer}
          disabledSelect={false}
        />

        <SelectSimple
          title="Seleccione período de tiempo"
          placeholderSelection="Filtrar por Trimestre"
          options={optionsTerm}
          selection={selectionTerm}
          setSelection={setSelectionTerm}
          disabledSelect={false}
        />
      </div>
      <Divider name="M" />
      <div className="team-dashboard-container-right-static-charts">
        <div />
        <div />
      </div>
      <Divider name="M" />
      <div className="team-dashboard-container-right-static-lines"></div>
    </div>
  );
};
