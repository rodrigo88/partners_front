import React, { useState } from "react";
import ArrowDown from "../../utils/Trazado 26.svg";

import "./selectClientsWallet.css";

export const SelectClientsWallet = ({
  icon,
  title,
  options,
  selection,
  setSelection,
  sizeList,
  disabledSelect,
  sizeContainer
}) => {
  const [openSelect, setOpenSelect] = useState(false);

  return (
    <div
      style={{
        position: "relative",
        pointerEvents: disabledSelect && "none",
        opacity: disabledSelect && 0.4,
        width: sizeContainer
      }}
    >
      <button
        className="select-container-clients-wallet"
        onClick={() => setOpenSelect(x => !x)}
      >
        <img src={icon} alt="..." />
        <div>
          {selection.value === -1 || selection === "" ? (
            <div className="select-container-clients-wallet-title">
              {title && title}
            </div>
          ) : (
            <div style={{ fontSize: "8px" }}>{selection.description}</div>
          )}
        </div>
        <ArrowDirection open={openSelect} />
      </button>

      <div
        style={{ width: sizeList }}
        className={`select-options-clients-wallet ${
          openSelect ? "open-clients-wallet" : ""
        }`}
      >
        {options.map((opt, i) => {
          return (
            <div
              className="option-select-clients-wallet"
              key={i}
              onClick={() => {
                setSelection(opt);
                setOpenSelect(false);
              }}
            >
              {opt.description}
            </div>
          );
        })}
      </div>
    </div>
  );
};

const ArrowDirection = ({ open }) => (
  <img className={open ? "arrow-invert" : ""} src={ArrowDown} alt="..." />
);
