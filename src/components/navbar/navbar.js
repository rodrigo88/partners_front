import React, { useState } from "react";
// import { Link } from "react-router-dom";
import { setLogOut } from "../../redux/actions";
import { Auth } from "aws-amplify";
import { setOpenBlur } from "../../redux/actions";
import { useSelector, useDispatch } from "react-redux";
import { Divider } from "../divider/divider";
import { Title, Text } from "../../components/titles/titles";
import { UserViewProfile } from "../userViewProfile/userViewProfile";
import iconCalendary from "../../utils/icon-calendario.svg";
import iconEmail from "../../utils/icon-mail.svg";
import iconTicket from "../../utils/icon-ticket.svg";
import iconFaq from "../../utils/icon-faq.svg";
import iconModeNight from "../../utils/icon-modenight.svg";
import iconSmallDown from "../../utils/small-down.svg";
import iconPerfil from "../../utils/icon-perfil.svg";
import iconCarrito from "../../utils/icon-carrito.svg";
import iconLogOut from "../../utils/icon-logout.svg";
import upArrow from "../../utils/UpArrow.svg";
import graphic from "../../utils/Grupo 3379.svg";
import "./navbar.css";

export const Navbar = () => {
  const state = useSelector(state => state);
  const dispatch = useDispatch();

  const signOut = () => Auth.signOut();

  const { userInformation, loadingUserInformation } = state;
  const { data } = userInformation;

  const [openProfileView, setOpenProfileView] = useState(false);

  return (
    <div className="navbar-container">
      <div className="navbar-left">
        <div className="portfolio-content-tenencia">
          <Title type="light" text="FEE 12.498" />
          <div className="content-porcentual">
            <img src={upArrow} alt="..." />
            <Text type="success" text="13,8 %" />
          </div>
        </div>
        <img src={graphic} alt="..." />
      </div>
      <div className="navbar-right">
        <Divider name="L" />
        <IconNavbar icon={iconCalendary} />
        <Divider name="L" />
        <IconNavbar icon={iconEmail} />
        <Divider name="L" />
        <IconNavbar icon={iconTicket} />
        <Divider name="L" />
        <IconNavbar icon={iconFaq} />
        <Divider name="L" />
        <IconNavbar icon={iconModeNight} />
        <Divider name="M" />
        <div className="divider-navbar" />
        <Divider name="M" />
        {loadingUserInformation ? (
          <div className="lds-ellipsis">
            <div />
            <div />
            <div />
            <div />
          </div>
        ) : (
          <div
            onClick={() => setOpenProfileView(x => !x)}
            className={`navbar-user-full-name`}
          >
            {data && data.fullName}
          </div>
        )}
        <Divider name="XS" />
        <img
          onClick={() => setOpenProfileView(x => !x)}
          className={`icon-right-container ${
            openProfileView ? "open-profile-view" : ""
          }`}
          src={iconSmallDown}
          alt="..."
        />
        <Divider name="XS" />
        <IconNavbarUserProfile
          icon={iconPerfil}
          openProfile={() => setOpenProfileView(x => !x)}
        />
        <Divider name="L" />
        <div className="divider-navbar" />
        <Divider name="L" />
        <IconNavbar
          onClick={() => dispatch(setOpenBlur())}
          icon={iconCarrito}
        />
        <Divider name="L" />
        <div className="divider-navbar" />
        <Divider name="L" />
        <IconNavbar
          icon={iconLogOut}
          onClick={() => {
            signOut();
            dispatch(setLogOut());
          }}
        />
        <Divider name="XXL" />
      </div>
      <UserViewProfile openProfileView={openProfileView} />
    </div>
  );
};

const IconNavbar = ({ icon, onClick }) => (
  <figure>
    <img onClick={onClick} src={icon} alt="..." />
  </figure>
);

const IconNavbarUserProfile = ({ icon, openProfile }) => (
  <figure>
    <img
      src={icon}
      alt="..."
      onClick={() => openProfile()}
      className="icon-right-container"
    />
  </figure>
);

// import React, { useState } from "react";
// import { navbarIcons } from "../../constants/constants";
// import { setLogOut } from "../../redux/actions";
// import { Title, Text } from "../../components/titles/titles";
// import { Link } from "react-router-dom";
// import { useDispatch } from "react-redux";
// import { Auth } from "aws-amplify";
// import { UserViewProfile } from "../userViewProfile/userViewProfile";
// import upArrow from "../../utils/UpArrow.svg";
// import graphic from "../../utils/Grupo 3379.svg";
// import "./navbar.css";

// const section_1 = navbarIcons.filter((x, i) => i <= 4);
// const section_2 = navbarIcons.filter((x, i) => i > 4 && i <= 6);
// const section_3 = navbarIcons.filter((x, i) => i === 7);
// const section_4 = navbarIcons.filter((x, i) => i === 8);

// export const Navbar = () => {
//   const [openProfileView, setOpenProfileView] = useState(false);
//   const dispatch = useDispatch();

//   const signOut = () => Auth.signOut();

//   return (
//     <div className="navbar-container">
//       <div className="navbar-left">
//         <div className="portfolio-content-tenencia">
//           <Title type="light" text="FEE 12.498" />
//           <div className="content-porcentual">
//             <img src={upArrow} alt="..." />
//             <Text type="success" text="13,8 %" />
//           </div>
//         </div>
//         <img src={graphic} alt="..." />
//       </div>
//       <div className="navbar-right">
//         {section_1.map((element, i) => {
//           return (
//             <div key={i}>
//               <img
//                 className="icon-right-container"
//                 src={element.imageIcon}
//                 alt="..."
//               />
//             </div>
//           );
//         })}
//         <div
//           className="divider-navbar"
//           style={{
//             marginLeft: "1px",
//             marginRight: "150px"
//           }}
//         />
//         {section_2.map((element, i) => {
//           return (
//             <div key={i}>
//               <img
//                 onClick={() => setOpenProfileView(x => !x)}
//                 className="icon-right-container"
//                 src={element.imageIcon}
//                 alt="..."
//               />
//             </div>
//           );
//         })}
//         <div
//           className="divider-navbar"
//           style={{
//             marginLeft: "-10px",
//             marginRight: "19px"
//           }}
//         />
//         {section_3.map((element, i) => {
//           return (
//             <div key={i}>
//               <img
//                 className="icon-right-container"
//                 src={element.imageIcon}
//                 alt="..."
//               />
//             </div>
//           );
//         })}
//         <div
//           className="divider-navbar"
//           style={{
//             marginLeft: "-10px",
//             marginRight: "20px"
//           }}
//         />
//         {section_4.map((element, i) => {
//           return (

//             <img
//               key={i}
//               onClick={() => {
//                 signOut();
//                 dispatch(setLogOut());
//               }}
//               className="icon-right-container"
//               src={element.imageIcon}
//               alt="..."
//             />
//           );
//         })}
//       </div>
//       <UserViewProfile openProfileView={openProfileView} />
//     </div>
//   );
// };
