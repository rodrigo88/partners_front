import React from "react";
import { Divider } from "../../components/divider/divider";
import iconGraph from "../../utils/Grupo 4048.svg";
import { Button } from "../../components/button/button";
import "./detailFundEspecie.css";

export const DetailFundEspecie = ({ detailEspecie }) => {
  const [option, setOption] = React.useState("COTIZACION");
  const [months] = React.useState(["Mar", "Jun", "Sep", "Dic"]);

  const arrayColours = [
    "#A325C1",
    "#583DC4",
    "#11BCA3",
    "#9590FC",
    "#33AA82",
    "#423994",
    "#7947B3",
    "#737DFF",
    "#E6DA2C",
    "#FFF675",
    "#608EF4",
    "#00000029",
    "#707070",
    "#CECECE",
    "#AAA453"
  ];

  const dataCotization = () => {
    return [
      {
        description: "Precio Año",
        value: "AR$ " + detailEspecie.data.PREV_CLOSE_PRICE
      },
      {
        description: "Moneda de cotización",
        value: detailEspecie.data.CURRENCY
      },
      {
        description: "Vol. Promedio (10d)",
        value: detailEspecie.data.VOLUME_10D_AVG
      },
      {
        description: "Vol. Promedio (3m)",
        value: detailEspecie.data.VOLUME_3M_AVG.toFixed(2)
      },
      {
        description: "Vol. Acumulado",
        value: detailEspecie.data.VOLUME_X_PRICE_ACCUM_LAST_WEEK.toFixed(2)
      },
      { description: "Patrimonio", value: detailEspecie.data.QTD_RETURN },
      {
        description: "Importe mínimo para operar",
        value: detailEspecie.data.MIN_AMOUNT_TO_OPERATE
      },
      { description: "Plazo de liquidez", value: "Inmediato" }
    ];
  };

  const carteraListEspecie = () => {
    const list = detailEspecie.data.PORTFOLIO.map((element, i) => {
      return {
        id: i + 1,
        color: arrayColours[i],
        name: element.ITEM,
        amount: element.AMOUNT,
        currency: element.CURRENCY,
        percentage: parseInt(element.RATIO)
      };
    });
    return list;
  };

  return (
    <div className="detail-fund-especie-container">
      <DetailFundEspecieTabs option={option} setOption={setOption} />
      <Divider name="S" />
      <DetailFundEspecieType detailEspecie={detailEspecie} />
      <Divider name="S" />
      <DetailFundEspecieName detailEspecie={detailEspecie} />
      {option === "COTIZACION" ? (
        <>
          <DetailFundEspecieStatics months={months} />
          <Divider name="S" />
          <DetailFundEspecieDataCotization data={dataCotization()} />
          <Divider name="S" />
        </>
      ) : (
        <>
          <Divider name="XS" />
          <DetailFundEspeciePieStatics dataList={carteraListEspecie()} />
          <DetailFundEspecieCartera dataList={carteraListEspecie()} />
        </>
      )}
      <Divider name="S" />
      <Button
        type="button"
        buttonStyle="btn--primary"
        // buttonSize="btn--large"
        arreglar="arreglar2"
      >
        SUSCRIBIR
      </Button>
      <div className="detail-fund-expecie-line-header" />
    </div>
  );
};

/* ******************************************** */
/* ******************************************** */
/* ******************************************** */
/* ******************************************** */
/* ******************************************** */

const DetailFundEspecieTabs = ({ option, setOption }) => {
  return (
    <div className="tabs">
      <div
        className={`option ${option === "COTIZACION" ? "selected" : ""}`}
        onClick={() => setOption("COTIZACION")}
      >
        COTIZACIÓN
      </div>
      <div
        className={`option ${option === "CARTERA" ? "selected" : ""}`}
        onClick={() => setOption("CARTERA")}
      >
        CARTERA
      </div>
    </div>
  );
};

const DetailFundEspecieDataCotization = ({ data }) => {
  const lengthData = data.length;
  return (
    <div className="data-cotization">
      {data.map((element, i) => {
        return (
          <div
            key={i}
            className={`data-cotization-element ${
              i !== lengthData - 1 ? "apply-border-bottom" : ""
            }`}
          >
            <div className="description">{element.description}</div>
            <div className="value">{element.value}</div>
          </div>
        );
      })}
    </div>
  );
};

const DetailFundEspecieStatics = ({ months }) => {
  return (
    <div className="statics">
      <img src={iconGraph} alt="..." />
      <div className="months">
        {months.map((month, i) => (
          <div key={i}>{month}</div>
        ))}
      </div>
    </div>
  );
};

const DetailFundEspecieType = ({ detailEspecie }) => {
  return (
    <div className="types">
      <div className="type selected-type">Persona Física</div>
      <Divider name="M" />
      <div className="type">Persona Jurídica</div>
    </div>
  );
};

const DetailFundEspecieName = ({ detailEspecie }) => {
  return (
    <div className="name">
      <div className="ticker">{detailEspecie.ticker}</div>
      <div className="data">
        $ {detailEspecie.data.NOW_PRICE.toFixed(2)} /{" "}
        {detailEspecie.data.DAY_PERCENTAGE_CHANGE.toFixed(2)}%
      </div>
    </div>
  );
};

/* ******************************************** */
/* ******************************************** */
/* ******************************************** */
/* ******************************************** */
/* ******************************************** */
export const DetailFundEspecieCartera = ({ dataList }) => {
  return (
    <div className="portfolio">
      {dataList.map((element, i) => {
        return (
          <div key={i} className="portfolio-element">
            <div
              style={{ background: element.color }}
              className="point-color"
            ></div>
            <div className="description">{element.name}</div>
            <div className="percentage">{element.percentage + "%"}</div>
          </div>
        );
      })}
    </div>
  );
};

export const DetailFundEspeciePieStatics = ({ dataList }) => {
  const initialPosPercentage = id => {
    const percArray = dataList.filter(element => element.id < id);
    const sum = percArray.reduce((acc, x) => acc + x.percentage, 0);
    const result = (sum * 180) / 50;
    return { sum, result };
  };

  return (
    <div className="statics">
      <div className="circle-container">
        {dataList.map(({ id, color, name, percentage }) => {
          return (
            <div
              key={id}
              className="perc-element"
              style={{
                zIndex: id === dataList.length ? 0 : id,
                transform: `rotate(${initialPosPercentage(id).result}deg)`,
                background: `conic-gradient(${color} ${percentage}%, transparent ${percentage}% 100%)`
              }}
            ></div>
          );
        })}
        <div
          className={`corona-interior ${true ? "big-radio" : "small-radio"}`}
        />
      </div>
    </div>
  );
};
