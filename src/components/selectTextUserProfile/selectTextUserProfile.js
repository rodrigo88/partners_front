import React, { useState } from "react";
import ArrowDown from "../../utils/Trazado 26.svg";
import "./selectTextUserProfile.css";

export const SelectTextUserProfile = ({
  options,
  selection,
  setSelectionProvince
}) => {
  const [openSelect, setOpenSelect] = useState(false);

  return (
    <div>
      <div className="label-select-text-user-container">Provincia</div>
      <button
        className="select-text-user-container"
        onClick={() => setOpenSelect(x => !x)}
      >
        <div className="select-text-user-container-selected-text">
          {selection.value === -1 ? "Selección" : selection.description}
        </div>
        <div className="select-text-user-container-icon-arrow">
          <ArrowDirection open={openSelect} />
        </div>
      </button>
      <div
        className={`select-text-user-options ${
          openSelect ? "open-select-text" : ""
        }`}
      >
        {options.map((opt, i) => {
          return (
            <div
              className="select-text-user-option"
              key={i}
              onClick={() => {
                setSelectionProvince(opt);
                setOpenSelect(false);
              }}
            >
              {opt.description}
            </div>
          );
        })}
      </div>
    </div>
  );
};

const ArrowDirection = ({ open }) => (
  <img className={open ? "arrow-invert" : ""} src={ArrowDown} alt="..." />
);
