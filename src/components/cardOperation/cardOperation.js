import React from "react";
import "./cardOperation.css";

const CARD_COLORS = [
  "nuevas-cuentas",
  "operaciones-del-dia",
  "total-de-cuentas",
  "activation-rate"
];

export const CardOperation = ({ description, value, classCardOperation }) => {
  const cardStyle = CARD_COLORS.includes(classCardOperation)
    ? classCardOperation
    : CARD_COLORS[0];

  return (
    <div className={`card-operation-container ${cardStyle}`}>
      <div className="card-operation-container-description">{description}</div>
      <div className="card-operation-container-value">{value}</div>
    </div>
  );
};
