import React from "react";
import "./piePercentageRodrigo.css";

export const PiePercentageRodrigo = ({ dataList, dimensions }) => {
  const initialPosPercentage = id => {
    const percArray = dataList.filter(element => element.id < id);
    const sum = percArray.reduce((acc, x) => acc + x.percentage, 0);
    const result = (sum * 180) / 50;
    return { sum, result };
  };

  const margin = 30;
  const difference = dimensions - margin;
  const marginInt = margin / 2;

  return (
    <div className="rodrigo-pie-main-container">
      <div
        className="rodrigo-my-circle-container"
        style={{
          height: dimensions + "px",
          width: dimensions + "px"
        }}
      >
        {dataList.map(({ id, color, name, percentage }) => {
          return (
            <div
              key={id}
              className="rodrigo-perc-element"
              style={{
                zIndex: id === dataList.length ? 0 : id,
                transform: `rotate(${initialPosPercentage(id).result}deg)`,
                background: `conic-gradient(${color} ${percentage}%, transparent ${percentage}% 100%)`
              }}
            ></div>
          );
        })}
        <div
          className={`rodrigo-corona-interna`}
          style={{
            height: difference + "px",
            width: difference + "px",
            left: marginInt + "px",
            top: marginInt + "px"
          }}
        ></div>
      </div>
    </div>
  );
};
