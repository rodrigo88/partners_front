import React from "react";
import "./cardProfile.css";

const STYLES = [
  "card--footer--portfolio--green",
  "card--footer--portfolio--red",
  "card--footer--portfolio--hover"
];

export const CardProfile = ({
  cardStyle,
  description,
  amountPesos,
  amountDollars
}) => {
  const myCardStyle = STYLES.includes(cardStyle) ? cardStyle : STYLES[0];

  return (
    <div className={`card--footer--portfolio ${myCardStyle}`}>
      <div className="card-profile">
        <div className="card-profile-description">{description}</div>
        <div className="card-profile-amount">
          <div>ARS{" " + amountPesos}</div>
          <div
            className={description === "FEE Total Ganancia" ? "avoid-view" : ""}
          >
            USD{" " + amountDollars}
          </div>
        </div>
      </div>
    </div>
  );
};
