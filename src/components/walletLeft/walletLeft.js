import React, { useState } from "react";
import { Divider } from "../divider/divider";
import { useSelector } from "react-redux";
import { SelectClientsWallet } from "../selectClientsWallet/selectClientsWallet";
import { WalletComponenteRaro } from "../walletComponenteRaro/walletComponenteRaro";
import { InputTextSearchClean } from "../inputTextSearchClean/inputTextSearchClean";
import iconPerfil from "../../utils/icon-perfil.svg";
import iconCopy from "../../utils/icon-copy.svg";
import iconGroup from "../../utils/icon-grupo.svg";
import { LoadingView } from "../../views/portfolio/portfolio";
import "./walletLeft.css";

export const WalletLeft = ({
  setOption,
  setSelectedClientDashboard,
  beginToWait
}) => {
  const state = useSelector(state => state);
  const { userLogged, userWalletClients, loadingUserWalletClients } = state;

  const [search, setSearch] = useState({ name: "" });
  const onChange = e =>
    setSearch({ ...search, [e.target.name]: e.target.value });

  const [selectedGroup, setSelectedGroup] = useState({
    value: -1,
    description: ""
  });

  const setCleanInput = () => {
    setSearch({ name: "" });
    setSelectedClientDashboard(null);
    beginToWait();
    setOption("TENENCIA");
  };

  const chooseClient = client => {
    setSelectedClientDashboard(client);
    setSearch({ name: client.name });
    beginToWait();
  };

  const optionsGrupos = [
    { value: 0, description: "G1" },
    { value: 1, description: "G2" },
    { value: 2, description: "G3" }
  ];

  const filteredWalletClients = () => {
    if (search.name === "") {
      return userWalletClients;
    } else {
      return userWalletClients.filter(x =>
        x.name.toLowerCase().includes(search.name.toLowerCase())
      );
    }
  };

  return (
    <div className="wallet-dashboard-container-left">
      <div className="header">
        <div className="title">Cartera</div>
        <Divider name="M" />
        <div className="referCode">
          Cód. Referido <strong>{userLogged.referredCode}</strong>
        </div>
        <Divider name="XS" />
        <div className="image">
          <img src={iconCopy} alt="..." />
        </div>
      </div>
      <div className="componente-raro">
        <WalletComponenteRaro />
      </div>
      <Divider name="M" />
      <div className="table-container">
        <div className="title">
          <Divider name="X" />
          <div>Buscador de Clientes</div>
        </div>
        <Divider name="S" />
        <div className="selections">
          {/* <InputTextClientsWallet */}
          <InputTextSearchClean
            type="text"
            name="name"
            placeholder="Escriba Apellido y Nombre"
            value={search.name}
            onChange={onChange}
            setCleanInput={setCleanInput}
            inputSize="input--large--100"
          />
          <Divider name="S" />
          <SelectClientsWallet
            icon={iconGroup}
            title=""
            options={optionsGrupos}
            selection={selectedGroup}
            setSelection={setSelectedGroup}
          />
        </div>
        <Divider name="M" />
        <div className="table">
          {loadingUserWalletClients ? (
            <LoadingView />
          ) : (
            <>
              {filteredWalletClients() &&
                filteredWalletClients().map((element, i) => (
                  <Element
                    key={i}
                    element={element}
                    chooseClient={chooseClient}
                  />
                ))}
            </>
          )}
        </div>
      </div>
    </div>
  );
};

const Element = ({ element, chooseClient }) => {
  return (
    <div className="element" onClick={() => chooseClient(element)}>
      <div className="item">
        <figure>
          <img src={iconPerfil} alt="..." />
        </figure>
      </div>
      <div className="item-center">
        <div className="name">{element.name}</div>
        <Divider name="XXS" />
        {element.isPhysicalPerson ? (
          <div className="number">CUIT: {element.physicalPerson.dni}</div>
        ) : (
          <div className="number">CUIL: {element.legalPerson.cuit}</div>
        )}
      </div>
      <div className="item">Nº {element.idCustomer}</div>
    </div>
  );
};
