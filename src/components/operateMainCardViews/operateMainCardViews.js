import React from "react";
import { useSelector } from "react-redux";
import iconGroup from "../../utils/icon-grupo.svg";
import iconFilter from "../../utils/icon-filtrar.svg";
import iconHelp from "../../utils/icon-consulta.svg";
import radioElipseIcon from "../../utils/Outer Ellipse.svg";
import { Divider } from "../../components/divider/divider";
import { InputTextClientsWallet } from "../../components/inputTextClientsWallet/inputTextClientsWallet";
import { SelectClientsWallet } from "../../components/selectClientsWallet/selectClientsWallet";
import { InputTextLineSearch } from "../inputTextLineSearch/inputTextLineSearch";
import "./operateMainCardViews.css";

export const OperateMainCardView = ({
  onChange,
  listTerms,
  selectTerm,
  subOptionItem,
  selectedGroup,
  setSelectedGroup,
  selectedInstrument,
  setSelectedInstrument,
  selectedEspecie,
  setSelectedEspecie,
  selectedEspecieGlobal,
  selectedWallet,
  setSelectedWallet,
  search
}) => {
  return (
    <div className="operate-dashboard-container-main-card">
      <div className="operate-sub-items-buy-sell-view">
        {subOptionItem === "COMPRAR" || "SUSCRIBIR" ? (
          <OperatorBuyView
            onChange={onChange}
            listTerms={listTerms}
            selectTerm={selectTerm}
            selectedGroup={selectedGroup}
            setSelectedGroup={setSelectedGroup}
            selectedInstrument={selectedInstrument}
            setSelectedInstrument={setSelectedInstrument}
            selectedEspecie={selectedEspecie}
            setSelectedEspecie={setSelectedEspecie}
            selectedWallet={selectedWallet}
            setSelectedWallet={setSelectedWallet}
            selectedEspecieGlobal={selectedEspecieGlobal}
            subOptionItem={subOptionItem}
            search={search}
          />
        ) : (
          <OperatorSellView />
        )}
      </div>
    </div>
  );
};

/* ************************************************************ */

const OperatorBuyView = ({
  onChange,
  listTerms,
  selectTerm,
  selectedGroup,
  setSelectedGroup,
  selectedInstrument,
  setSelectedInstrument,
  selectedEspecie,
  setSelectedEspecie,
  selectedWallet,
  selectedEspecieGlobal,
  setSelectedWallet,
  subOptionItem,
  search
}) => {
  const state = useSelector(state => state);
  const { fundsData, bondsData, cedearsData, equityData } = state;

  const optionsGrupos = [
    { value: 0, description: "G1" },
    { value: 1, description: "G2" },
    { value: 2, description: "G3" }
  ];

  const optionsInstrument = [
    { value: 0, description: "Bonos" },
    { value: 1, description: "Acciones" },
    { value: 2, description: "Fondos" }
  ];

  const optionsSelect = arrayList => {
    return arrayList.map((element, i) => {
      return { value: i, description: element.ticker, code: element.code };
    });
  };

  const optionsByInstrument = () => {
    let options;
    const { description } = selectedInstrument;
    switch (description) {
      case "Bonos":
        options = optionsSelect(bondsData);
        break;
      case "Acciones":
        options = optionsSelect(cedearsData);
        break;
      case "Fondos":
        options = optionsSelect(fundsData);
        break;
      default:
        options = [];
    }
    return options;
  };

  const optionsWallets = [
    { value: 0, description: "Cartera Fondos Conservador" },
    { value: 1, description: "Cartera Fondos Moderado" },
    { value: 2, description: "Cartera Fondos Agresivo" }
  ];

  const listTermsByInstrument = () => {
    if (
      selectedInstrument.description === "Fondos" &&
      subOptionItem === "RESCATAR"
    ) {
      return listTerms.filter((x, i) => x.id >= 4);
    } else if (
      selectedInstrument.description === "Fondos" &&
      subOptionItem === "SUSCRIBIR"
    ) {
      return [];
    }

    if (selectedInstrument.description !== "Fondos") {
      return listTerms.filter((x, i) => x.id <= 3);
    }
  };

  return (
    <>
      {/* GRID 1*/}
      <div className="operate-sub-items-buy-sell-view-name-group">
        <Divider name="M" />
        <div>
          <div className="title-grid-operate">Buscar por Persona Física</div>
          <Divider name="XS" />
          <InputTextClientsWallet
            type="text"
            name="name"
            placeholder="Apellido y Nombre"
            value={search.name}
            onChange={onChange}
            inputSize="input--medium"
          />
        </div>
        <Divider name="M" />
        <div>
          <div className="title-grid-operate">Elija un grupo</div>
          <Divider name="XS" />
          <SelectClientsWallet
            icon={iconGroup}
            title=""
            options={optionsGrupos}
            selection={selectedGroup}
            setSelection={setSelectedGroup}
          />
        </div>
      </div>
      {/* GRID 2*/}
      <div>
        <div>
          <div className="title-grid-operate">Instrumento</div>
          <Divider name="XS" />
          <SelectClientsWallet
            icon={iconFilter}
            title="Instrumento"
            options={optionsInstrument}
            selection={selectedInstrument}
            setSelection={setSelectedInstrument}
            disabledSelect={selectedWallet.description !== ""}
            sizeList="156px"
            sizeContainer="128px"
          />
        </div>
        <Divider name="M" />
        <Divider name="M" />
        <div>
          <div className="title-grid-operate">Especie</div>
          <Divider name="XS" />
          <SelectClientsWallet
            icon={iconFilter}
            title="Especie"
            options={optionsByInstrument()}
            selection={selectedEspecie}
            setSelection={setSelectedEspecie}
            disabledSelect={
              selectedInstrument.description === "" ||
              selectedWallet.value !== -1
            }
            sizeList="256px"
            sizeContainer="120px"
          />
        </div>
        <Divider name="M" />
        <div>
          <div className="title-grid-operate">Cartera Recomendada</div>
          <Divider name="XS" />
          <SelectClientsWallet
            icon={iconFilter}
            title="Seleccionar"
            options={optionsWallets}
            selection={selectedWallet}
            setSelection={setSelectedWallet}
            sizeList="188px"
            disabledSelect={selectedInstrument.description !== ""}
            sizeContainer="120px"
          />
        </div>
      </div>
      {/* GRID 3*/}
      <div>
        <Divider name="M" />
        <div>
          <div className="title-grid-operate">
            Buscar por Persona Jurídica o Comitente
          </div>
          <Divider name="XS" />
          <div className="operate-sub-items-buy-sell-view-cuit-comit">
            <InputTextLineSearch
              type="text"
              name="cuit"
              placeholder="CUIT"
              value={search.cuit}
              onChange={onChange}
              inputSize="input--medium"
            />
            <Divider name="M" />
            <InputTextLineSearch
              type="text"
              name="comitente"
              placeholder="COMITENTE"
              value={search.comitente}
              onChange={onChange}
              inputSize="input--small"
            />
          </div>
          <Divider name="XS" />
          <div className="title-grid-operate">
            <Divider name="XS" />
            <img
              className="img-radio-term-operate"
              src={radioElipseIcon}
              alt="..."
            />
            <Divider name="XS" />
            <div>Mostrar solo comitentes con títulos adquiridos</div>
          </div>
          <Divider name="M" />
        </div>
      </div>
      {/* GRID 4*/}
      <div>
        <div className="operate-sub-items-buy-sell-view-radio-buttons">
          <div className="title-grid-operate">
            {subOptionItem !== "SUSCRIBIR" && (
              <>
                <div>Elija tipo de plazo </div>
                <Divider name="XXS" />
                <img src={iconHelp} alt="..." />
              </>
            )}
          </div>
          <Divider name="XS" />
          <div className="radios">
            {listTermsByInstrument().map(termRadio => (
              <div className="radio-element" key={termRadio.id}>
                <div className="top">
                  <img
                    onClick={() => selectTerm(termRadio)}
                    className={`img-radio-term-operate ${
                      termRadio.checked ? "checked" : ""
                    }`}
                    src={radioElipseIcon}
                    alt="..."
                  />
                  <Divider name="XS" />
                  <div>{termRadio.term}</div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

/* ************************************************************ */

const OperatorSellView = () => {
  return <div>VENDER</div>;
};
