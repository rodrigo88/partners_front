import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Divider } from "../divider/divider";
import { WalletTabs } from "../walletTabs/walletTabs";
import { LoadingView } from "../../views/portfolio/portfolio";
import { WalletCardPie } from "../walletCardPie/walletCardPie";
import { WalletMiniCard } from "../walletMiniCard/walletMiniCard";
import { WalletSelections } from "../../components/walletSelections/walletSelections";
import { WalletHeaderData } from "../walletHeaderData/walletHeaderData";
import { WalletData } from "../walletData/walletData";
import iconAmp from "../../utils/icon-ampliar.svg";
import iconReduce from "../../utils/icon-ampliar (1).svg";
import iconTicket from "../../utils/icon-ticket2.svg";
import iconChartBlue from "../../utils/Grupo 4164.svg";
import iconPerfil from "../../utils/account_circle-24px.svg";
import iconPerfil2 from "../../utils/icon-perfil.svg";
import "./walletRight.css";

export const WalletRight = ({
  option,
  setOption,
  loadingData,
  selectedClientDashboard
}) => {
  const state = useSelector(state => state);
  const { treasuryData } = state;

  const [search, setSearch] = useState({ cuit: "", name: "" });
  const onChange = e =>
    setSearch({ ...search, [e.target.name]: e.target.value });

  const [openPesos, setOpenPesos] = useState(false);
  const [openDolares, setOpenDolares] = useState(false);

  const dataTable = [
    {
      id: 0,
      title: "Fondos",
      invInicial: "ARS 45.000",
      invActual: "ARS 200.000",
      list: [
        {
          description: "Especie Ad-Cap Retorno Total",
          invInicial: "40.000",
          invActual: "150.000"
        },
        {
          description: "Especie Ad-Cap Ahorro Pesos",
          invInicial: "5.000",
          invActual: "50.000"
        }
      ]
    },
    {
      id: 1,
      title: "Bonos",
      invInicial: "ARS 45.000",
      invActual: "ARS 200.000"
    },
    {
      id: 2,
      title: "Acciones",
      invInicial: "ARS 45.000",
      invActual: "ARS 200.000"
    },
    {
      id: 3,
      title: "Cedears",
      invInicial: "ARS 45.000",
      invActual: "ARS 200.000"
    }
  ];

  const dataTable2 = [
    {
      simbolo: "CRTAFAA",
      description: "Renta Fija",
      quantity: "37,9044",
      ultPrecio: "ARS 8,00",
      ganancia: "1,77 %",
      perdida: "ARS 5,40",
      valorizado: "ARS 305,40"
    },
    {
      simbolo: "CRTAFAA",
      description: "Renta Fija",
      quantity: "37,9044",
      ultPrecio: "ARS 8,00",
      ganancia: "1,77 %",
      perdida: "ARS 5,40",
      valorizado: "ARS 305,40"
    },
    {
      simbolo: "CRTAFAA",
      description: "Renta Fija",
      quantity: "37,9044",
      ultPrecio: "ARS 8,00",
      ganancia: "1,77 %",
      perdida: "ARS 5,40",
      valorizado: "ARS 305,40"
    },
    {
      simbolo: "CRTAFAA",
      description: "Renta Fija",
      quantity: "37,9044",
      ultPrecio: "ARS 8,00",
      ganancia: "1,77 %",
      perdida: "ARS 5,40",
      valorizado: "ARS 305,40"
    },
    {
      simbolo: "CRTAFAA",
      description: "Renta Fija",
      quantity: "37,9044",
      ultPrecio: "ARS 8,00",
      ganancia: "1,77 %",
      perdida: "ARS 5,40",
      valorizado: "ARS 305,40"
    },
    {
      simbolo: "CRTAFAA",
      description: "Renta Fija",
      quantity: "37,9044",
      ultPrecio: "ARS 8,00",
      ganancia: "1,77 %",
      perdida: "ARS 5,40",
      valorizado: "ARS 305,40"
    },
    {
      simbolo: "CRTAFAA",
      description: "Renta Fija",
      quantity: "37,9044",
      ultPrecio: "ARS 8,00",
      ganancia: "1,77 %",
      perdida: "ARS 5,40",
      valorizado: "ARS 305,40"
    },
    {
      simbolo: "CRTAFAA",
      description: "Renta Fija",
      quantity: "37,9044",
      ultPrecio: "ARS 8,00",
      ganancia: "1,77 %",
      perdida: "ARS 5,40",
      valorizado: "ARS 305,40"
    }
  ];

  return (
    <div className="wallet-dashboard-container-right">
      {/* ***************  HEADER **********************/}
      <div className="wallet-dashboard-container-right-header">
        <div className="wallet-dashboard-container-right-header-title">
          {selectedClientDashboard
            ? selectedClientDashboard.name
            : "Cartera Global"}
        </div>
        {selectedClientDashboard && (
          <div className="wallet-dashboard-container-right-header-data">
            <div className="wallet-dashboard-container-right-header-data-left">
              <div className="wallet-dashboard-container-right-header-data-left-icon">
                <img src={iconPerfil} alt="..." />
              </div>
              <div className="wallet-dashboard-container-right-header-data-left-name">
                <div>{selectedClientDashboard.name}</div>
                <Divider name="XXS" />
                <div>Cuenta: {selectedClientDashboard.idCustomer}</div>
              </div>
            </div>
            <div className="wallet-dashboard-container-right-header-data-center">
              <div>
                Cel:{" "}
                {selectedClientDashboard.isPhysicalPerson
                  ? selectedClientDashboard.physicalPerson.mobile
                  : selectedClientDashboard.legalPerson.mobile}
              </div>
              <Divider name="XXS" />
              <div>
                CUIT/ DNI:{" "}
                {selectedClientDashboard.isPhysicalPerson
                  ? selectedClientDashboard.physicalPerson.dni
                  : selectedClientDashboard.legalPerson.cuit}
              </div>
            </div>
            <div className="wallet-dashboard-container-right-header-data-right">
              <div className="wallet-dashboard-container-right-header-data-right-email">
                {selectedClientDashboard.isPhysicalPerson
                  ? selectedClientDashboard.physicalPerson.email
                  : selectedClientDashboard.legalPerson.email}
              </div>
              <Divider name="XXS" />
              <div>
                Nacimiento:{" "}
                {selectedClientDashboard.isPhysicalPerson
                  ? selectedClientDashboard.physicalPerson.birthDate
                  : selectedClientDashboard.legalPerson.phone}
              </div>
            </div>
          </div>
        )}
      </div>
      {/* ***************  TABS **********************/}
      <WalletTabs
        option={option}
        setOption={setOption}
        selectedClientDashboard={selectedClientDashboard}
      />
      {/* ***************  CARDS **********************/}
      <div
        style={{
          width: "800px",
          height: "700px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "flex-start"
        }}
      >
        {loadingData ? (
          <LoadingView />
        ) : (
          <>
            {option === "TENENCIA" && (
              <div>
                <div className="wallet-dashboard-container-right-cards">
                  <WalletCardPie title="Instrumentos" />
                  <WalletCardPie title="Especies" />
                  <WalletMiniCard
                    icon={iconChartBlue}
                    title="FEE Ganancia"
                    value="55.591,80"
                    equivalente="ARS"
                    valueEquivalente="78.828,80"
                  />
                </div>
                <Divider name="M" />
                <div className="wallet-dashboard-container-right-containers-desplegables">
                  {/* ***************  TABLES **********************/}
                  <InstrumentList
                    title="Detalle Tenencia Consolidada en Pesos"
                    dataTable={dataTable}
                    open={openPesos}
                    setOpen={setOpenPesos}
                  />
                  <Divider name="M" />
                  <InstrumentList
                    title="Detalle Tenencia Consolidada en Dólares"
                    dataTable={dataTable}
                    open={openDolares}
                    setOpen={setOpenDolares}
                  />
                </div>
              </div>
            )}
            {option === "FEE" && (
              <div>
                <div className="wallet-dashboard-container-right-cards2">
                  <img
                    style={{ height: "500px", width: "815px" }}
                    src={iconChartBlue}
                    alt="..."
                  />
                </div>
                <Divider name="M" />
                <div className="wallet-dashboard-container-right-table2">
                  <Divider name="XXS" />
                  <div className="wallet-dashboard-container-right-table2-title">
                    Composición - Mi Ganancia
                  </div>
                  <Divider name="M" />
                  <div className="wallet-dashboard-container-right-table2-header">
                    <div>SÍMBOLO</div>
                    <div>DESCRIPCIÓN</div>
                    <div>CANTIDAD</div>
                    <div>ULTIMO PRECIO</div>
                    <div>GANANCIA</div>
                    <div>PERDIDA</div>
                    <div>VALORIZADO</div>
                  </div>
                  <Divider name="XXS" />
                  <div className="wallet-dashboard-container-right-table2-data">
                    {dataTable2.map((item, i) => (
                      <div
                        className="wallet-dashboard-container-right-table2-data-item"
                        key={i}
                      >
                        <div className="simbolo">{item.simbolo}</div>
                        <div className="description">{item.description}</div>
                        <div className="quantity">{item.quantity}</div>
                        <div className="ultPrecio">{item.ultPrecio}</div>
                        <div className="ganancia">{item.ganancia}</div>
                        <div className="perdida">{item.perdida}</div>
                        <div className="valorizado">{item.valorizado}</div>
                        <div className="operate">Operar</div>
                        <div>
                          <img src={iconTicket} alt="..." />
                        </div>
                      </div>
                    ))}
                  </div>
                  <Divider name="XXS" />
                  <div className="wallet-dashboard-container-right-table2-totals">
                    <div className="wallet-dashboard-container-right-table2-totals-title">
                      Total Plan Inversión
                    </div>
                    <div />
                    <div className="wallet-dashboard-container-right-table2-totals-value">
                      ARS 1,017.94
                    </div>
                  </div>
                  <Divider name="XXS" />
                </div>
              </div>
            )}
            {option === "CUENTA CORRIENTE" && (
              <div>
                <Divider name="M" />
                <div className="wallet-dashboard-container-right-cards3">
                  <div className="wallet-dashboard-container-right-cards3-left">
                    <img
                      src={iconPerfil}
                      style={{
                        width: "60px",
                        height: "60px",
                        marginBottom: "20px"
                      }}
                      alt="..."
                    />
                  </div>
                  <div className="wallet-dashboard-container-right-cards3-center">
                    <div className="wallet-dashboard-container-right-cards3-center-salud">
                      Salud de cartera de {selectedClientDashboard.name}
                    </div>
                    <Divider name="XXS" />
                    <div className="wallet-dashboard-container-right-cards3-center-inversor">
                      Inversor declarado
                    </div>
                  </div>
                  <div className="wallet-dashboard-container-right-cards3-right">
                    {`Estado Actual (Nuevo Grafico)`}
                  </div>
                </div>
                <Divider name="M" />
                <div className="wallet-dashboard-container-right-table3">
                  <Divider name="XS" />
                  <WalletSelections search={search} onChange={onChange} />
                  <Divider name="XS" />
                  <WalletHeaderData />
                  <div className="wallet-dashboard-container-right-table3-data">
                    <WalletData treasuryData={treasuryData} search={search} />
                  </div>
                </div>
              </div>
            )}
          </>
        )}
      </div>
    </div>
  );
};

const InstrumentList = ({ title, dataTable, open, setOpen }) => {
  return (
    <div
      className={`wallet-dashboard-container-right-desplegables ${
        open ? "open-tenencia" : ""
      }`}
    >
      <div className="wallet-dashboard-container-right-desplegables-header">
        <div className="wallet-dashboard-container-right-desplegables-header-icon">
          <img
            src={open ? iconReduce : iconAmp}
            alt="..."
            onClick={() => setOpen(x => !x)}
          />
        </div>
        <div
          className="wallet-dashboard-container-right-desplegables-header-title"
          style={{ color: open ? "#737DFF" : "#2d2e8f" }}
        >
          {title}
        </div>
      </div>
      <div
        className={`wallet-dashboard-container-right-desplegables-list ${
          open ? "visible-table" : ""
        }`}
      >
        {dataTable.map((instrument, i) => (
          <Instrument instrument={instrument} key={i} />
        ))}
      </div>
    </div>
  );
};

const Instrument = ({ instrument }) => {
  const [openSubList, setOpenSubList] = useState(false);
  return (
    <div className="wallet-dashboard-container-right-desplegables-list-element">
      <div className="wd-element">
        <div className="wInst-el-iconAmp">
          <img
            src={openSubList ? iconReduce : iconAmp}
            onClick={() => setOpenSubList(x => !x)}
            alt="..."
          />
        </div>
        <div className="wInst-el-title">{instrument.title}</div>
        <Divider name="M" />
        <div className="wInst-el-invInicial">{instrument.invInicial}</div>
        <Divider name="M" />
        <div className="wInst-el-invActual">{instrument.invActual}</div>
        <Divider name="M" />
        <div className="wInst-el-operar">Operar</div>
        <Divider name="M" />
        <div className="wInst-el-iconOp">
          <img src={iconTicket} alt="..." />
        </div>
      </div>
      <div
        className={`wInst-el-sublist ${openSubList ? "visibleSubList" : ""}`}
      >
        {instrument.list &&
          instrument.list.map((element, i) => {
            return (
              <div className="sb-element" key={i}>
                <div className="sb-desc">{element.description}</div>
                <div className="sb-invIni">ARS {element.invInicial}</div>
                <div className="sb-invAct">ARS {element.invActual}</div>
                <div className="sb-operar">Operar</div>
                <div className="sb-icon">
                  <img src={iconTicket} alt="..." />
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
};
