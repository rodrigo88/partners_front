import React from "react";
import sendIcon from "../../utils/icon-enviar.svg";
import ticketIcon from "../../utils/icon-ticket-mov.svg";
import { Divider } from "../divider/divider";
// import iconCopy from "../../utils/icon-copy.svg";
// import { useSelector } from "react-redux";
import "./walletData.css";

export const WalletData = ({ treasuryData, search }) => {
  const even = x => x % 2 === 0;
  const accepted = x => x.state === "ACEPTADO";
  const colorText = x => {
    const { state } = x;
    switch (state) {
      case "ACEPTADO":
        return "#0AB8B2";
      case "PENDIENTE":
        return "#C4CB01";
      default:
        return "#E80451";
    }
  };

  const filteredClients = () => {
    if (search.name === "") {
      return treasuryData;
    } else {
      return treasuryData.filter(element =>
        element.client.toLowerCase().includes(search.name)
      );
    }
  };

  return (
    <div className="wallet-data-container">
      {filteredClients().map((element, i) => {
        return (
          <div key={i} className={`element ${!even(i) ? "gray-row" : ""}`}>
            <Divider name="L" />
            <div className="client normal-letter">{element.date}</div>
            <div className="client normal-letter">{element.date}</div>
            <div className="account normal-letter">{element.instrument}</div>
            <div className="detail normal-letter">{element.price}</div>
            <div className="detail normal-letter">{element.quantity}</div>
            <div className="currency normal-letter">{element.currency}</div>
            <div className="amount strong-letter" style={{ color: "#0AB8B2" }}>
              {element.amount}
            </div>
            <div
              className="state strong-letter"
              style={{ color: colorText(element) }}
            >
              {element.state}
            </div>
            <div className="actions">
              <img src={ticketIcon} alt="..." />
              <img
                className={!accepted(element) ? "inactive" : ""}
                src={sendIcon}
                alt="..."
              />
            </div>
          </div>
        );
      })}
    </div>
  );
};
