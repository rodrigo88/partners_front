export const data = [
  {
    type: 0,
    concentracion: "21/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPACON240",
    instrumento: "AY240",
    precio: "350",
    cantidad: "300,000",
    neto: "9.000,000",
    estado: "cancelar",
    subList: [
      {
        id: 0,
        des: "Orden",
        con: "21/11/2020",
        liq: "PENDIENTE",
        det: "CPACON240",
        inst: "AY240",
        pre: "100",
        can: "120,000",
        neto: "3.500,000",
        estado: "cancelar"
      },

      {
        id: 1,
        des: "Orden",
        con: "21/11/2020",
        liq: "PENDIENTE",
        det: "CPACON240",
        inst: "AY240",
        pre: "200",
        can: "80,000",
        neto: "3.500,000",
        estado: "cancelar"
      },

      {
        id: 2,
        des: "Orden",
        con: "21/11/2020",
        liq: "PENDIENTE",
        det: "CPACON240",
        inst: "AY240",
        pre: "50",
        can: "100,000",
        neto: "2.000,000",
        estado: "cancelar"
      }
    ]
  },
  {
    type: 1,
    concentracion: "12/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPACON241",
    instrumento: "AY241",
    precio: "56,23",
    cantidad: "115,0000",
    neto: "8.836,85",
    estado: "cancelar"
  },
  {
    type: 0,
    concentracion: "25/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPACON242",
    instrumento: "AY242",
    precio: "76,87",
    cantidad: "456,0000",
    neto: "8.163,85",
    estado: "cancelar",
    subList: [
      {
        id: 0,
        des: "Orden",
        con: "21/11/2020",
        liq: "PENDIENTE",
        det: "CPACON240",
        inst: "AY240",
        pre: "100",
        can: "120,000",
        neto: "3.500,000",
        estado: "cancelar"
      },

      {
        id: 1,
        des: "Orden",
        con: "21/11/2020",
        liq: "PENDIENTE",
        det: "CPACON240",
        inst: "AY240",
        pre: "200",
        can: "80,000",
        neto: "3.500,000",
        estado: "cancelar"
      },

      {
        id: 2,
        des: "Orden",
        con: "21/11/2020",
        liq: "PENDIENTE",
        det: "CPACON240",
        inst: "AY240",
        pre: "50",
        can: "100,000",
        neto: "2.000,000",
        estado: "cancelar"
      }
    ]
  },
  {
    type: 1,
    concentracion: "07/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPBCON243",
    instrumento: "AY243",
    precio: "43,67",
    cantidad: "723,0000",
    neto: "8.734,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "18/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPBCON243",
    instrumento: "AY244",
    precio: "87,90",
    cantidad: "112,0000",
    neto: "8.843,85",
    estado: "cancelar"
  },
  {
    type: 0,
    concentracion: "20/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPBCON243",
    instrumento: "AY245",
    precio: "23,76",
    cantidad: "376,0000",
    neto: "8.187,85",
    estado: "cancelar",
    subList: [
      {
        id: 0,
        des: "Orden",
        con: "21/11/2020",
        liq: "PENDIENTE",
        det: "CPACON240",
        inst: "AY240",
        pre: "100",
        can: "120,000",
        neto: "3.500,000",
        estado: "cancelar"
      },

      {
        id: 1,
        des: "Orden",
        con: "21/11/2020",
        liq: "PENDIENTE",
        det: "CPACON240",
        inst: "AY240",
        pre: "200",
        can: "80,000",
        neto: "3.500,000",
        estado: "cancelar"
      },

      {
        id: 2,
        des: "Orden",
        con: "21/11/2020",
        liq: "PENDIENTE",
        det: "CPACON240",
        inst: "AY240",
        pre: "50",
        can: "100,000",
        neto: "2.000,000",
        estado: "cancelar"
      }
    ]
  },
  {
    type: 1,
    concentracion: "29/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPBCON243",
    instrumento: "AY246",
    precio: "65,12",
    cantidad: "623,0000",
    neto: "8.749,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "30/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPBCON243",
    instrumento: "AY247",
    precio: "16,48",
    cantidad: "378,0000",
    neto: "8.654,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "15/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPFCON248",
    instrumento: "AY248",
    precio: "29,94",
    cantidad: "172,0000",
    neto: "8.678,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "12/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPFCON248",
    instrumento: "AY249",
    precio: "68,91",
    cantidad: "278,0000",
    neto: "8.344,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "22/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPFCON248",
    instrumento: "AY250",
    precio: "71,34",
    cantidad: "627,0000",
    neto: "8.242,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "03/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPFCON248",
    instrumento: "AY251",
    precio: "54,28",
    cantidad: "498,0000",
    neto: "8.974,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "08/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPFCON248",
    instrumento: "AY252",
    precio: "12,89",
    cantidad: "851,0000",
    neto: "8.643,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "09/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPFCON248",
    instrumento: "AY253",
    precio: "55,33",
    cantidad: "671,0000",
    neto: "8.321,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "12/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPHCON254",
    instrumento: "AY254",
    precio: "67,76",
    cantidad: "887,0000",
    neto: "8.556,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "11/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPHCON254",
    instrumento: "AY255",
    precio: "39,65",
    cantidad: "761,0000",
    neto: "8.743,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "20/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPHCON254",
    instrumento: "AY56",
    precio: "52,27",
    cantidad: "186,0000",
    neto: "8.753,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "30/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPHCON254",
    instrumento: "AY257",
    precio: "78,88",
    cantidad: "267,0000",
    neto: "8.489,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "11/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPHCON254",
    instrumento: "AY258",
    precio: "53,29",
    cantidad: "389,0000",
    neto: "8.871,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "01/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPRCON259",
    instrumento: "AY259",
    precio: "41,77",
    cantidad: "833,0000",
    neto: "8.122,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "20/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPRCON259",
    instrumento: "AY260",
    precio: "23,67",
    cantidad: "731,0000",
    neto: "8.828,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "17/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPRCON259",
    instrumento: "AY261",
    precio: "34,22",
    cantidad: "199,0000",
    neto: "8.462,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "15/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPRCON259",
    instrumento: "AY262",
    precio: "33,78",
    cantidad: "375,0000",
    neto: "8.671,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "22/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPRCON259",
    instrumento: "AY263",
    precio: "16,76",
    cantidad: "878,0000",
    neto: "8.783,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "11/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPRCON259",
    instrumento: "AY264",
    precio: "54,87",
    cantidad: "654,0000",
    neto: "8.992,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "05/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPWCON265",
    instrumento: "AY265",
    precio: "66,12",
    cantidad: "579,0000",
    neto: "8.734,85",
    estado: "cancelar"
  },
  {
    type: 1,

    concentracion: "29/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPWCON265",
    instrumento: "AY266",
    precio: "43,56",
    cantidad: "787,0000",
    neto: "8.842,85",
    estado: "cancelar"
  },
  {
    type: 1,

    concentracion: "17/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPWCON265",
    instrumento: "AY267",
    precio: "77,00",
    cantidad: "543,0000",
    neto: "8.845,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "14/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPWCON265",
    instrumento: "AY268",
    precio: "21,22",
    cantidad: "332,0000",
    neto: "8.851,85",
    estado: "cancelar"
  },
  {
    type: 1,

    concentracion: "22/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPWCON265",
    instrumento: "AY269",
    precio: "43,00",
    cantidad: "226,0000",
    neto: "8.178,85",
    estado: "cancelar"
  },
  {
    type: 1,

    concentracion: "26/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPWCON265",
    instrumento: "AY270",
    precio: "65,65",
    cantidad: "668,0000",
    neto: "8.000,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "08/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPZCON272",
    instrumento: "AY271",
    precio: "28,00",
    cantidad: "874,0000",
    neto: "8.563,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "18/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPZCON272",
    instrumento: "AY272",
    precio: "38,50",
    cantidad: "560,0000",
    neto: "8.789,85",
    estado: "cancelar"
  },
  {
    type: 1,
    concentracion: "15/11/2020",
    liquidacion: "PENDIENTE",
    detalle: "CPZCON272",
    instrumento: "AY273",
    precio: "62,70",
    cantidad: "100,0000",
    neto: "8.511,85",
    estado: "cancelar"
  }
];
