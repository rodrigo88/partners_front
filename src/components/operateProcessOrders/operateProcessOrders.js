import React, { useState } from "react";
import { data } from "./dataProcessOrders";
import { Divider } from "../divider/divider";
import { SelectClientsWallet } from "../../components/selectClientsWallet/selectClientsWallet";
import { InputTextClientsWallet } from "../../components/inputTextClientsWallet/inputTextClientsWallet";
import iconFilter from "../../utils/icon-filtrar.svg";
import iconGroup from "../../utils/icon-grupo.svg";
import iconDownload from "../../utils/download.svg";
import iconPrint from "../../utils/print.svg";
import iconAmp from "../../utils/icon-ampliar.svg";
import iconReduce from "../../utils/icon-ampliar (1).svg";
import "./operateProcessOrders.css";

export const OperateProcessOrders = ({
  activeDetailPendientesElement,
  handleDetailProcess
}) => {
  const [openFilter, setOpenFilter] = useState(false);

  const [pendingProcessList, setPendingProcessList] = useState(data);

  const [search, setSearch] = useState({
    cuit: "",
    comitente: "",
    name: ""
  });

  const onChange = e =>
    setSearch({ ...search, [e.target.name]: e.target.value });

  const deleteProcess = process => {
    const list = pendingProcessList.filter(p => p.detalle !== process.detalle);
    setPendingProcessList(list);
    handleDetailProcess(process);
  };

  const [selectedGroup, setSelectedGroup] = useState({
    value: -1,
    description: ""
  });

  const [selectedInstrument, setSelectedInstrument] = useState({
    value: -1,
    description: ""
  });

  const [selectedEspecie, setSelectedEspecie] = useState({
    value: -1,
    description: ""
  });

  const [selectedType, setSelectedType] = useState({
    value: 2,
    description: "Todas las Operatorias"
  });

  const optionsFilterHeader = [
    { value: 0, description: "Operatoria en Lote" },
    { value: 1, description: "Operatoria Individual" },
    { value: 2, description: "Todas las Operatorias" }
  ];

  const chooseOption = proccess => setSelectedType(proccess);

  const filteredProcess = () => {
    let resultList;
    const value = selectedType.value;

    if (search.name !== "") {
      resultList = pendingProcessList
        .filter(x => x.type === value)
        .filter(x =>
          x.detalle.toLowerCase().includes(search.name.toLowerCase())
        );
    } else {
      resultList = pendingProcessList.filter(x => x.type === value);
    }

    if (value === 2) {
      return pendingProcessList;
    } else {
      return resultList;
    }
  };

  return (
    <div className="ordenes-en-proceso">
      <div className="ordenes-en-proceso-header">
        <div className="tabs">
          <div className="title">ORDENES PENDIENTES</div>
          <div className="icons">
            <div>Resumen</div>
            <Divider name="S" />
            <div className="container-icon">
              <img src={iconDownload} alt="..." />
            </div>
            <Divider name="S" />
            <div className="container-icon">
              <img src={iconPrint} alt="..." />
            </div>
            <Divider name="L" />
          </div>
        </div>
        <div className="ordenes-linea-horizontal" />

        <div className="selections">
          <Divider name="M" />
          <InputTextClientsWallet
            type="text"
            name="name"
            placeholder="Buscar por cliente"
            value={search.price}
            onChange={onChange}
            inputSize="input--large--10"
          />
          <Divider name="M" />
          <SelectClientsWallet
            icon={iconFilter}
            title="Instrumento"
            options={[]}
            selection={selectedInstrument}
            // setSelection={setSelectedInstrument}
            // disabledSelect={disabledSelectInstEsp}
            sizeList="156px"
            sizeContainer="128px"
          />
          <Divider name="M" />
          <SelectClientsWallet
            icon={iconFilter}
            title="Especie"
            options={[]}
            selection={selectedEspecie}
            // setSelection={setSelectedEspecie}
            // disabledSelect={disabledSelectInstEsp}
            sizeList="256px"
            sizeContainer="120px"
          />
          <Divider name="M" />
          <SelectClientsWallet
            icon={iconGroup}
            title=""
            options={[]}
            selection={selectedGroup}
            // setSelection={setSelectedGroup}
          />
        </div>
      </div>
      <Divider name="S" />
      <Divider name="S" />
      <div className="ordenes-en-proceso-table-header">
        <div className="columns">
          <div className="tipo" onClick={() => setOpenFilter(x => !x)}>
            <img src={iconFilter} alt="..." />
            <Divider name="XXS" />
            <div>Tipo</div>
          </div>
          <div className="contratacion">Fecha Contratación</div>
          <div className="liquidacion">Fecha Liquidación</div>
          <div className="detalle">Detalle</div>
          <div className="instrumento">Instrumento</div>
          <div className="precio">Precio</div>
          <div className="cantidad">Cantidad</div>
          <div className="neto">Neto</div>
          <div className="estado">Estado</div>
        </div>
        <div
          className={`filter-header-options ${
            openFilter ? "openFilterHeader" : ""
          }`}
        >
          {optionsFilterHeader.map((option, i) => {
            return (
              <div
                key={i}
                className={`filter-header-option ${
                  option.value === selectedType.value
                    ? "optionSelectedType"
                    : ""
                } `}
                onClick={() => {
                  chooseOption(option);
                  setOpenFilter(x => !x);
                }}
              >
                {option.description}
              </div>
            );
          })}
        </div>
      </div>
      <Divider name="S" />
      <div className="ordenes-en-proceso-table-pendientes">
        {filteredProcess().map((process, i) => (
          <ElementData
            key={i}
            process={process}
            activeDetailPendientesElement={activeDetailPendientesElement}
            deleteProcess={deleteProcess}
          />
        ))}
      </div>
    </div>
  );
};

const ElementData = ({
  process,
  activeDetailPendientesElement,
  deleteProcess
}) => {
  const [openSubData, setOpenSubData] = useState(false);
  return (
    <div className="ordenes-en-proceso-table-pendientes-element">
      <div className="data">
        <div className="orden">
          {process.subList ? (
            <>
              <img
                src={openSubData ? iconReduce : iconAmp}
                alt="..."
                onClick={() => setOpenSubData(x => !x)}
              />
              <Divider name="XXS" />
              <div>Lote</div>
            </>
          ) : (
            "Orden"
          )}
        </div>
        <div className="con">{process.concentracion}</div>
        <div className="liq" style={{ color: "#F1C40F" }}>
          {process.liquidacion}
        </div>
        <div
          className="det"
          onClick={() => activeDetailPendientesElement(process)}
        >
          {process.detalle}
        </div>
        <div
          className="inst"
          onClick={() => activeDetailPendientesElement(process)}
        >
          {process.instrumento}
        </div>
        <div className="pre">$ {process.precio}</div>
        <div className="can">{process.cantidad}</div>
        <div className="neto">$ {process.neto}</div>
        <div className="estado" onClick={() => deleteProcess(process)}>
          {process.estado.toUpperCase()}
        </div>
      </div>
      {process.subList && (
        <div className={`sub-data-list ${openSubData ? "openSubData" : ""}`}>
          {process.subList.map((subElement, i) => (
            <div className="sub-data-element" key={i}>
              <div>{subElement.des}</div>
              <div>{subElement.con}</div>
              <div>{subElement.liq}</div>
              <div>{subElement.det}</div>
              <div>{subElement.inst}</div>
              <div>$ {subElement.pre}</div>
              <div>{subElement.can}</div>
              <div>$ {subElement.neto}</div>
              <div>{subElement.estado}</div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};
