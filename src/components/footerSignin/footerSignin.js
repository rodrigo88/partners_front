import React from "react";
import { Paragraph } from "../titles/titles";
import { Divider } from "../divider/divider";
import question from "../../utils/question.svg";
import "./footerSignin.css";

export const FooterSignin = () => {
  return (
    <div className="footer">
      <img
        src={question}
        // className="App-logo"
        alt="logo"
      />
      <Divider name="XS" />
      <Paragraph
        type="text"
        text="Tenes problemas para ingresar a tu cuenta ?"
      />
      <Paragraph type="link" text="Escribenos aqui" />
      <Paragraph type="text" text="Te responderemos a la brevedad" />
      <Divider name="L" />
      <Paragraph
        type="link"
        text="Al ingresar, aceptas los términos y condiciones"
      />
    </div>
  );
};
