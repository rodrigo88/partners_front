import React, { useState } from "react";
import ArrowDown from "../../utils/Trazado 26.svg";
import "./selectBilling.css";

export const SelectBilling = ({ options, selection, setSelection }) => {
  const [openSelect, setOpenSelect] = useState(false);

  return (
    <div>
      <button
        className="select-billing-container"
        onClick={() => setOpenSelect(x => !x)}
      >
        <div>
          {selection.value === -1 ? "Selección" : selection.description}
        </div>
        <ArrowDirection open={openSelect} />
      </button>

      <div
        className={`select-billing-options ${openSelect ? "open-billing" : ""}`}
      >
        {options.map((opt, i) => {
          return (
            <div
              className="option-billing"
              key={i}
              onClick={() => {
                setSelection(opt);
                setOpenSelect(false);
              }}
            >
              {opt.description}
            </div>
          );
        })}
      </div>
    </div>
  );
};

const ArrowDirection = ({ open }) => (
  <img className={open ? "arrow-invert" : ""} src={ArrowDown} alt="..." />
);
