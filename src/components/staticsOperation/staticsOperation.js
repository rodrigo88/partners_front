import React, { useState } from "react";
// import { Subtitle } from "../titles/titles";
import { useSelector } from "react-redux";
import { Divider } from "../divider/divider";
import reactangleBlue from "../../utils/Rectángulo 1918.svg";
import reactangleGreen from "../../utils/Rectángulo 1919.svg";
import { SelectProfileCoin } from "../selectProfileCoin/selectProfileCoin";
import "./staticsOperation.css";

export const StaticsOperation = ({ nextIncome }) => {
  // const state = useSelector(state => state);
  // const { userOperations, loadingUserOperations } = state;

  const [holdingValues, SetHoldingValues] = useState({
    holding24: nextIncome.holding24Ars,
    holding48: nextIncome.holding48Ars,
    holdingToday: nextIncome.holdingTodayArs
  });

  const [selectionCoin, setSelectionCoin] = useState({
    value: 0,
    description: "ARS"
  });

  const optionsCoin = [
    { value: 0, description: "ARS" },
    { value: 1, description: "USD" }
  ];

  const setSelectionCoin2 = opt => {
    setSelectionCoin(opt);
    switch (opt) {
      case 0:
        SetHoldingValues({
          ...holdingValues,
          holding24: nextIncome.holding24Ars,
          holding48: nextIncome.holding48Ars,
          holdingToday: nextIncome.holdingTodayArs
        });
        break;
      default:
        SetHoldingValues({
          ...holdingValues,
          holding24: nextIncome.holding24Usd,
          holding48: nextIncome.holding48Usd,
          holdingToday: nextIncome.holdingTodayUsd
        });
        break;
    }
  };

  return (
    <div className="statics-operation-container">
      <div className="statics-operation-header">
        <div className="statics-operation-header-title">
          Tenencia Líquida - Próximos ingresos
        </div>
        <div className="statics-operation-header-selection">
          <SelectProfileCoin
            options={optionsCoin}
            selection={selectionCoin}
            setSelectionCoin2={setSelectionCoin2}
          />
        </div>
      </div>
      <div className="statics-operation-content">
        <StaticValues
          value={holdingValues.holdingToday}
          description="A ingresar hoy"
        />
        <StaticValues
          value={holdingValues.holding24}
          description="Ingreso en 24 hs"
        />
        <StaticValues
          value={holdingValues.holding48}
          description="Ingreso en 48 hs"
        />
      </div>
    </div>
  );
};

const StaticValues = ({ value, description }) => {
  return (
    <div className="static-value-container">
      <div className="static-value-container-colors">
        <img src={reactangleBlue} alt="..." />
        <Divider name="XXS" />
        <img src={reactangleGreen} alt="..." />
      </div>
      <div className="static-value-container-value">
        <div style={{ color: "#67698B", fontSize: "16px" }}>{value}</div>
      </div>
      <div className="static-value-container-description">
        <div style={{ color: "#A4A7BE", fontSize: "12px" }}>{description}</div>
      </div>
    </div>
  );
};
