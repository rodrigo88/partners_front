import React, { useState } from "react";
import ArrowDown from "../../utils/Trazado 26.svg";
import "./selectSimple.css";

export const SelectSimple = ({
  title,
  options,
  selection,
  setSelection,
  disabledSelect,
  placeholderSelection
}) => {
  const [openSelect, setOpenSelect] = useState(false);

  return (
    <div
      className="select--simple--container"
      onMouseLeave={() => setOpenSelect(false)}
    >
      <div className="select--simple--container--title">{title}</div>

      <button
        className="select--simple--container--button"
        onClick={() => setOpenSelect(x => !x)}
      >
        <div className="select--simple--container--button--description">
          {!selection ? placeholderSelection : selection.description}
        </div>
        <div className="select--simple--container--button--arrow-icon">
          <ArrowDirection open={openSelect} />
        </div>
      </button>

      <div
        className={`select--simple--container--options ${
          openSelect ? "open--ssc--options" : ""
        }`}
      >
        {options.map((opt, i) => {
          return (
            <div
              className="select--simple--container--options--item"
              key={i}
              onClick={() => {
                setSelection(opt);
                setOpenSelect(false);
              }}
            >
              {opt.description}
            </div>
          );
        })}
      </div>
    </div>
  );
};

const ArrowDirection = ({ open }) => (
  <img className={open ? "arrow-invert" : ""} src={ArrowDown} alt="..." />
);
