import React from "react";
import iconHomeNewsPayment from "../../utils/icon-home-news-payment.svg";
import iconHomeNewsBirthday from "../../utils/icon-home-news-birthday.svg";
import iconHomeNewsAlert from "../../utils/icon-home-alert.svg";
import iconHomeNewsMarket from "../../utils/Enmascarar grupo 1@2x.png";
import "./latestNew.css";

export const LatestNew = ({ notice }) => {
  const { id, type, title, content } = notice;

  return (
    <div className="latest-container-new">
      <div className="latest-container-new-image">
        <img
          src={
            type === "new"
              ? iconHomeNewsMarket
              : type === "payment"
              ? iconHomeNewsPayment
              : type === "birth"
              ? iconHomeNewsBirthday
              : iconHomeNewsAlert
          }
        />
      </div>
      {/* <div className="latest-container-new-info">{id}</div> */}
      <div className="latest-new-container-description">
        <div className="latest-new-container-description-title">{title}</div>
        <div className="latest-new-container-description-content">
          {content}
        </div>
      </div>
    </div>
  );
};
