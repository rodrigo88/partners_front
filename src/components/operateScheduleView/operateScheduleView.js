import React from "react";
import { Divider } from "../../components/divider/divider";
import iconCalendar from "../../utils/calendar.svg";
import "./operateScheduleView.css";

export const OperateScheduleView = ({ selectedInstrument, selectedWallet }) => {
  const horaFondo = 15;
  const horaBolsa = 12;

  const assignClassName = () => {
    if (selectedInstrument.description === "Fondos") {
      if (horaFondo === 15) {
        return "nextStepRodrigo";
      } else {
        return "warningRodrigo";
      }
    } else {
      if (horaBolsa === 17) {
        return "nextStepRodrigo";
      } else {
        return "warningRodrigo";
      }
    }
  };

  return (
    <div className="operate-container-schedule">
      <div
        className={`operate-container-schedule-container ${assignClassName()}`}
      >
        <Divider name="S" />
        <div className="operate-container-schedule-calendar">
          <img src={iconCalendar} alt="..." />
        </div>
        <Divider name="S" />
        <div
          className="operate-container-schedule-title"
          style={{ fontSize: "14px", fontWeight: "600" }}
        >
          HORARIO
        </div>
        <Divider name="S" />
        <div
          className="operate-container-schedule-atention"
          style={{ fontSize: "12px" }}
        >
          {selectedInstrument.description === "Fondos" ||
          selectedWallet.description === "Fondos"
            ? "Lunes a viernes de 10 a 15 hs"
            : "Lunes a viernes de 11 a 17 hs"}
        </div>
      </div>
    </div>
  );
};
