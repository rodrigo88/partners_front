import React from "react";
import checkBlue from "../../utils/Rectángulo 194.svg";
import checkAvailable from "../../utils/Rectángulo 195.svg";
import checkGreen from "../../utils/Rectángulo 196.svg";
import "./staticsProfile.css";

const COINS = ["ARS", "USD"];

export const StaticsProfile = ({ coin }) => {
  const checkCoin = COINS.includes(coin) ? coin : COINS[0];

  return (
    <div className="circle-statics">
      <div className={`circle1 ${checkCoin}`}>
        <div className="circle2">{coin}</div>
      </div>
      <div className="checks">
        <div className="checks-title">
          <img
            src={coin === "ARS" ? checkBlue : checkGreen}
            alt="..."
            className="check-title-image"
          />
          <div>Consolidada</div>
        </div>
        <div className="checks-title">
          <img className="check-title-image" src={checkAvailable} alt="..." />
          <div>Disponible</div>
        </div>
      </div>
    </div>
  );
};
