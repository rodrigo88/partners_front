import React from "react";
import sendIcon from "../../utils/icon-enviar.svg";
import ticketIcon from "../../utils/icon-ticket-mov.svg";
import pointGreen from "../../utils/Elipse 96.svg";
import pointBlue from "../../utils/Elipse 97.svg";
import pointRed from "../../utils/Elipse 98.svg";
import pointYellow from "../../utils/Elipse 99.svg";
import "./movement.css";

export const Movement = ({ movement }) => {
  const { id, description, date, name, amount } = movement;

  return (
    <div className="movement-container">
      <div className="movement-item-description">
        <img
          src={
            [0, 4, 8, 12].includes(id)
              ? pointGreen
              : [1, 5, 9, 13].includes(id)
              ? pointBlue
              : [2, 6, 10, 14].includes(id)
              ? pointRed
              : pointYellow
          }
          alt="..."
          style={{ marginRight: "10px" }}
        />

        <div>{description}</div>
      </div>

      <div className="movement-item-2 grayStyle schedule">{date}</div>
      <div className="movement-item-3 grayStyle">{name}</div>

      <div className="movement-icons">
        <div className="movement-item amount">{amount}</div>
        <img className="movement-img" src={ticketIcon} alt="..." />
        <img className="movement-img" src={sendIcon} alt="..." />
      </div>
    </div>
  );
};
