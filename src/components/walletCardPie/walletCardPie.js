import React, { useState } from "react";
import { Divider } from "../../components/divider/divider";
import { userBalanceList } from "../../libs/libsTreasury";
import { CustomSelectRodrigo } from "../customSelectRodrigo/customSelectRodrigo";
import { GridPercentageStatics } from "../gridPercentageStatics/gridPercentageStatics";
import { PiePercentageRodrigo } from "../piePercentageRodrigo/piePercentageRodrigo";
import {
  ListPercentageStatics,
  PiePercentageStatics
} from "../../views/treasury/treasury";
import "./walletCardPie.css";
import { LoadingView } from "../../views/portfolio/portfolio";

export const WalletCardPie = ({ title }) => {
  const [loadingData, setLoadingData] = useState(false);

  const data1 = [
    { description: "Fondos", currency: "ARS", value: "4260", ratio: 10 },
    { description: "Acciones", currency: "ARS", value: "4260", ratio: 20 },
    { description: "Cedears", currency: "ARS", value: "4260", ratio: 10 },
    { description: "Fondos", currency: "ARS", value: "4260", ratio: 20 },
    { description: "Acciones", currency: "ARS", value: "4260", ratio: 30 },
    { description: "Cedears", currency: "ARS", value: "4260", ratio: 10 }
  ];

  const data2 = [
    { description: "ALUA", currency: "ARS", value: "2500", ratio: 10 },
    { description: "BBAR", currency: "ARS", value: "2500", ratio: 20 },
    { description: "BMA", currency: "ARS", value: "2500", ratio: 10 },
    { description: "BYMA", currency: "ARS", value: "2500", ratio: 20 },
    { description: "CEPU", currency: "ARS", value: "2500", ratio: 30 },
    { description: "COME", currency: "ARS", value: "2500", ratio: 10 }
  ];

  const filteredData = () => {
    if (title === "Instrumentos") {
      return data1;
    } else {
      return data2;
    }
  };

  const options = [
    { value: 0, description: "Fondos" },
    { value: 1, description: "Acciones" },
    { value: 2, description: "Bonos" },
    { value: 3, description: "Cedears" },
    { value: 4, description: "Commodities" }
  ];

  const [selection, setSelection] = useState({
    value: -1,
    description: ""
  });

  const setSelectionInstrument = option => {
    setSelection(option);
    setLoadingData(true);
    setTimeout(() => setLoadingData(false), 1500);
  };

  const arrayColoursARS = [
    "#3A9DDD",
    "#737DFF",
    "#423994",
    "#903ADD",
    "#2D2E8F",
    "#0AB8B2"
  ];

  const [option, setOption] = useState("Pesos");

  return (
    <div className="wallet-card-pie-container">
      <div className="wallet-card-pie-container-header">
        <div className="wallet-card-pie-container-header-title">{title}</div>
        {title === "Especies" && (
          <div className="wallet-card-pie-container-header-selection">
            <CustomSelectRodrigo
              options={options}
              selection={selection}
              setSelectionInstrument={setSelectionInstrument}
            />
          </div>
        )}
      </div>
      <div className="wallet-card-pie-container-types">
        <div
          onClick={() => setOption("Pesos")}
          className={`wallet-card-pie-container-types-option ${
            option === "Pesos" ? "selected" : ""
          }`}
        >
          Pesos
        </div>
        <Divider name="XXS" />
        <div
          onClick={() => setOption("Dólares")}
          className={`wallet-card-pie-container-types-option ${
            option === "Dólares" ? "selected" : ""
          }`}
        >
          Dólares
        </div>
      </div>
      <div className="wallet-card-pie-container-statics">
        {loadingData ? (
          <>
            <Divider name="XXL" />
            <LoadingView />
          </>
        ) : (
          <>
            <PiePercentageRodrigo
              dimensions={110}
              dataList={userBalanceList(filteredData(), arrayColoursARS)}
            />
            <GridPercentageStatics
              dataList={userBalanceList(filteredData(), arrayColoursARS)}
              currency="ARS"
            />
          </>
        )}
      </div>
    </div>
  );
};
