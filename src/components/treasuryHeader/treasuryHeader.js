import React from "react";
import iconCopy from "../../utils/icon-copy.svg";
import { Divider } from "../divider/divider";
import { useSelector } from "react-redux";
import "./treasuryHeader.css";

export const TreasuryHeader = () => {
  const state = useSelector(state => state);
  const { userLogged } = state;
  return (
    <div className="treasury-dashboard-container-header">
      <div className="title">Tesorería</div>
      <Divider name="M" />
      <div className="referCode">
        Cód. Referido <strong>{userLogged.referredCode}</strong>
      </div>
      <Divider name="XS" />
      <div className="image">
        <img src={iconCopy} alt="..." />
      </div>
    </div>
  );
};
