import React from "react";
import { Button } from "../button/button";
// import { InputTextMovementsProof } from "../inputTextMovemenProof/inputTextMovementsProof";
// import { SelectMovementsProof } from "../selectMovementsProof/selectMovementsProof";
// import { InputExactAmountValue } from "../inputExactAmountValue/inputExactAmountValue";
import { Divider } from "../divider/divider";
import iconClose from "../../utils/Grupo 4165.svg";
import iconSuccess from "../../utils/icon-mensaje-exito.svg";
import iconRelojArena from "../../utils/_x33_6_hourglass.svg";
import iconTimeOff from "../../utils/icon-timeoff.svg";
import iconFailure from "../../utils/icon-mensaje-cancelar.svg";
import "./operateModalFinish.css";

export const OperateModalFinish = ({
  resultConfirm,
  openModal,
  closeModal
}) => {
  const modalName = "operate-custom-modal-finish-container";
  const clickModal = e => e.target.id === modalName && closeModal();

  const randomInt = (min, max) => {
    return min + Math.floor((max - min) * Math.random());
  };

  return (
    <div
      id="operate-custom-modal-finish-container"
      className={`operate-custom-modal-finish-container ${
        openModal ? "open-modal" : ""
      }`}
      onClick={clickModal}
    >
      <div
        id="operate-custom-modal-finish-container--content"
        className="region"
        onClick={clickModal}
      >
        {resultConfirm && resultConfirm.legend === "nextStepRodrigo" ? (
          <ModalProccessWait
            instrument={resultConfirm.instrument}
            iconClose={iconClose}
            iconResult={iconRelojArena}
            closeModal={closeModal}
          />
        ) : resultConfirm && resultConfirm.legend === "warningRodrigo" ? (
          <ModalCloseMarket
            instrument={resultConfirm.instrument}
            iconClose={iconClose}
            iconResult={iconTimeOff}
            closeModal={closeModal}
          />
        ) : (
          <RandomFailure
            iconClose={iconClose}
            iconResult={iconFailure}
            closeModal={closeModal}
          />
        )}
      </div>
    </div>
  );
};

const RandomFailure = ({ iconClose, iconResult, closeModal }) => {
  return (
    <div className="content">
      <div className="cancel">
        <img src={iconClose} alt="..." onClick={() => closeModal()} />
      </div>
      <Divider name="M" />
      <img src={iconResult} alt="..." onClick={() => closeModal()} />
      <Divider name="L" />
      <div className="title">Error en la Operación</div>
      <Divider name="M" />
      <div className="description">
        Hubo un problema al programar tu orden&nbsp; Por favor vuelta a intertar
        mas tarde&nbsp;
      </div>
      <Divider name="M" />
      <div className="buttons">
        <div className="load">
          <Button type="button" buttonStyle="btn--primary" arreglar="arreglar3">
            IR A OPERAR
          </Button>
        </div>
        <Divider name="M" />
      </div>
      <Divider name="M" />
      <Divider name="M" />
    </div>
  );
};

const ModalCloseMarket = ({
  iconClose,
  iconResult,
  closeModal,
  instrument
}) => {
  return (
    <div className="content">
      <div className="cancel">
        <img src={iconClose} alt="..." onClick={() => closeModal()} />
      </div>
      <Divider name="M" />
      <img src={iconResult} alt="..." onClick={() => closeModal()} />
      <Divider name="L" />
      <div className="title">Mercado Cerrado momentáneamente</div>
      <Divider name="M" />
      {instrument === "Fondos" ? (
        <div className="description">
          ¿Desea Realizar la Operación y Programar la Orden ? &nbsp;Si opera por
          Fondos la orden se programará&nbsp; y ejecutará automáticamente al
          momento&nbsp;de apertura del Mercado
        </div>
      ) : (
        <div className="description">
          ¿Desea Realizar la Operación y Programar la Orden ?&nbsp; Si opera por
          Bolsa la orden se programará y quedará&nbsp; pendiente a ejecutarse
          cuando aperture el Mercado&nbsp;
        </div>
      )}

      <Divider name="M" />
      <div className="description">
        {instrument !== "Fondos" && (
          <div>
            Es posible que los precios se reajusten al momento&nbsp; de operar
            fuera de hora. Usted previamente confirmará&nbsp; la operación y
            visualizará la variación&nbsp;
          </div>
        )}
        <div className="no-load">Puede consultar horarios Aquí</div>
      </div>
      <Divider name="M" />
      <div className="buttons">
        <div className="load">
          <Button type="button" buttonStyle="btn--primary" arreglar="arreglar3">
            QUIERO PROGRAMAR ORDEN
          </Button>
        </div>
        <Divider name="M" />
        <div className="no-load">No cargar por ahora</div>
      </div>
      <Divider name="M" />
      <Divider name="M" />
    </div>
  );
};

const ModalProccessWait = ({
  iconClose,
  iconResult,
  closeModal,
  instrument
}) => {
  React.useEffect(() => {
    setTimeout(() => {
      closeModal();
    }, 3000);
  }, []);
  return (
    <div className="content">
      <div className="cancel">
        <img src={iconClose} alt="..." onClick={() => closeModal()} />
      </div>
      <Divider name="M" />
      <img src={iconResult} alt="..." onClick={() => closeModal()} />
      <Divider name="L" />
      <div className="title">Tu Orden está en proceso</div>
      <Divider name="M" />
      <div className="description">
        Te enviaremos una notificación cuando tengamos &nbsp; una actualización
        de estado. &nbsp; Puede visualizar el estado en la sección de&nbsp;
        <div className="no-load">Ordenes en Proceso</div>
      </div>
      <Divider name="M" />
      <div className="buttons">
        <div className="load">
          <Button type="button" buttonStyle="btn--primary" arreglar="arreglar3">
            ir a operar
          </Button>
        </div>
      </div>
      <Divider name="M" />
      <Divider name="M" />
    </div>
  );
};
