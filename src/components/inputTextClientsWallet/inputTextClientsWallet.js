import React from "react";
import iconSearch from "../../utils/icon-buscar.svg";
import "./inputTextClientsWallet.css";

const SIZES = ["input--small--10", "input--medium--10", "input--large--10"];

export const InputTextClientsWallet = ({
  text,
  placeholder,
  onChange,
  type,
  value,
  defaultValue,
  name,
  children,
  inputSize
}) => {
  const checkInputSize = SIZES.includes(inputSize) ? inputSize : SIZES[0];

  return (
    <div className={`input-text-clients-wallet-container `}>
      <img className="img-input-text" src={iconSearch} alt="..." />
      <input
        name={name}
        className={`input-text-clients-wallet ${checkInputSize}`}
        text={text}
        placeholder={placeholder}
        onChange={onChange}
        type={type}
        value={value}
        defaultValue={defaultValue}
      >
        {children}
      </input>
    </div>
  );
};
