import React, { useState } from "react";
import iconClose from "../../utils/Grupo 4165.svg";
// import { Button } from "../button/button";
// import { InputTextMovementsProof } from "../inputTextMovemenProof/inputTextMovementsProof";
// import { SelectMovementsProof } from "../selectMovementsProof/selectMovementsProof";
// import { InputExactAmountValue } from "../inputExactAmountValue/inputExactAmountValue";
// import { Divider } from "../divider/divider";
import "./teamModalDeshabilitarCuenta.css";

export const TeamModalDeshabilitarCuenta = ({ openModal, closeModal }) => {
  const modalName = "custom--modal--deshabilitar--cuenta--container";
  const clickModal = e => e.target.id === modalName && closeModal();

  return (
    <div
      id="custom--modal--deshabilitar--cuenta--container"
      className={`custom--modal--deshabilitar--cuenta--container ${
        openModal ? "open--modal--deshabilitar" : ""
      }`}
      onClick={clickModal}
    >
      <div
        id="custom--modal--deshabilitar--cuenta--container--content"
        className="custom--modal--deshabilitar--cuenta--container--content--region"
        onClick={clickModal}
      >
        <div className="custom--modal--deshabilitar--cuenta--container--content--region--content">
          <div className="custom--modal--deshabilitar--cuenta--container--content--region--content--cancel">
            <img src={iconClose} alt="..." onClick={closeModal} />
          </div>
          <div className="title">Deshabilitar Cuenta</div>
        </div>
      </div>
    </div>
  );
};
