import React, { useState } from "react";
import { Button } from "../../components/button/button";
import iconStatics from "../../utils/Grupo 4045.svg";
import { Divider } from "../divider/divider";

import "./operateDetailEspecie2.css";

export const OperateDetailEspecie2 = ({ detailEspecie }) => {
  const [option, setOption] = useState("PRECIOS");

  const [months] = useState(["Mar", "Jun", "Sep", "Dic"]);

  const financial = x => Number.parseFloat(x).toFixed(2);

  const time = new Date();
  const hours = time.getHours();
  const min = time.getMinutes();
  const sec = time.getSeconds();

  const dataCard = [
    {
      field: "Precio",
      value: "$ " + detailEspecie.data.NOW_PRICE
    },
    {
      field: "Dif del dia",
      value: financial(detailEspecie.data.DAY_PERCENTAGE_CHANGE)
    },
    { field: "Volumen", value: "$ " + detailEspecie.data.VOLUME },
    { field: "Hora", value: hours + ":" + min + ":" + sec }
  ];

  const horizontalList = ["C.Compra", "P.Compra", "C.Venta", "P.Venta"];

  const dataCompra = [
    { c1: detailEspecie.data.BID_SIZE, c2: detailEspecie.data.BID_PRICE }
  ];

  const dataVenta = [
    { c1: detailEspecie.data.ASK_SIZE, c2: detailEspecie.data.ASK_PRICE }
  ];

  const dataPerformance = () => {
    return [
      {
        description: "Precio del Día",
        value:
          "AR$ " +
          detailEspecie.data.LOW_PRICE +
          " - " +
          "AR$ " +
          detailEspecie.data.HIGH_PRICE
      },
      {
        description: "Precio Año",
        value:
          "AR$ " +
          detailEspecie.data.YEAR_MIN +
          " - " +
          "AR$ " +
          detailEspecie.data.YEAR_MAX
      },
      {
        description: "Volument Nominal",
        value: detailEspecie.data.VOLUME
      },
      {
        description: "Vol. Promedio (10d)",
        value: detailEspecie.data.VOLUME_10D_AVG
      },
      {
        description: "Vol. Promedio (3m)",
        value: detailEspecie.data.VOLUME_3M_AVG
      },
      {
        description: "BETA",
        value: detailEspecie.data.BETA
      },
      {
        description: "Cap. de Mercado",
        value: detailEspecie.data.MARKET_CAPITAL
      }
    ];
  };

  return (
    <div className={`operate-detail-especie2-container`}>
      <div className="tabs">
        <div
          className={`option ${option === "PRECIOS" ? "selected" : ""}`}
          onClick={() => setOption("PRECIOS")}
        >
          PRECIOS
        </div>
        <div
          className={`option ${option === "PERFORMANCE" ? "selected" : ""}`}
          onClick={() => setOption("PERFORMANCE")}
        >
          PERFORMANCE
        </div>
      </div>
      <Divider name="XS" />
      <div className="title">{detailEspecie.ticker}</div>
      <div className="title2">
        {detailEspecie.name + " / " + detailEspecie.data.NOW_PRICE}
      </div>
      {/* <div className="title">{detailEspecie.data.NOW_PRICE}</div> */}
      {option === "PRECIOS" ? (
        <div className="detail-especie2-info">
          <div className="statics">
            <img src={iconStatics} alt="..." />
            <div className="months">
              {months.map((month, i) => (
                <div key={i}>{month}</div>
              ))}
            </div>
          </div>
          <div className="data">
            {dataCard.map((element, i) => (
              <ElementData key={i} element={element} />
            ))}
          </div>
          <div className="horizontal-data">
            {horizontalList.map((element, i) => (
              <ElementHeader key={i} element={element} />
            ))}
          </div>
          <Divider name="XS" />
          <div className="data-color">
            <div style={{ background: "#0AB8B2" }}>
              {dataCompra.map((element, i) => (
                <div className="data-color-element" key={i}>
                  <div>{element.c1}</div>
                  <div>{element.c2}</div>
                </div>
              ))}
            </div>
            <div style={{ width: "1px" }} />
            <div style={{ background: "#E80451" }}>
              {dataVenta.map((element, i) => (
                <div className="data-color-element" key={i}>
                  <div>{element.c1}</div>
                  <div>{element.c2}</div>
                </div>
              ))}
            </div>
          </div>
          <Divider name="S" />
        </div>
      ) : (
        <div className="data-cotization">
          {dataPerformance().map((element, i) => {
            return (
              <div
                key={i}
                className={`data-cotization-element ${
                  i !== dataPerformance().length - 1
                    ? "apply-border-bottom"
                    : ""
                }`}
              >
                <div className="description">{element.description}</div>
                <div className="value">{element.value}</div>
              </div>
            );
          })}
        </div>
      )}
      <div className="btn-operate-button">
        <Button type="button" buttonStyle="btn--primary" arreglar="arreglar2">
          OPERAR
        </Button>
      </div>
    </div>
  );
};

const ResultMiniCard = ({ title, value }) => {
  return (
    <div className="result-mini-card">
      <div className="title">{title}</div>
      <div className="value">{value}</div>
    </div>
  );
};

const ElementData = ({ element }) => {
  return (
    <div className="element-data">
      <div className="field">{element.field}</div>
      <div className="value">{element.value}</div>
    </div>
  );
};

const ElementHeader = ({ element }) => {
  return <div>{element}</div>;
};
