import React from "react";
import iconCopy from "../../utils/icon-copy.svg";
import { Divider } from "../../components/divider/divider";
import { useSelector } from "react-redux";
import "./operateHeader.css";

export const OperateHeader = () => {
  const state = useSelector(state => state);
  const { userLogged } = state;
  return (
    <div className="operate-dashboard-container-header">
      <div className="operate-dashboard-container-header-title">
        Operatoria en lote o individual
      </div>
      <Divider name="M" />
      <div className="operate-dashboard-container-header-referCode">
        Cód. Referido <strong>{userLogged.referredCode}</strong>
      </div>
      <Divider name="XS" />
      <div className="operate-dashboard-container-header-image">
        <img src={iconCopy} alt="..." />
      </div>
    </div>
  );
};
