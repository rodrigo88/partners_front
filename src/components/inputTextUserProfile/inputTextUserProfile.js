import React from "react";
import editIcon from "../../utils/icon-edit.svg";
import "./inputTextUserProfile.css";

const SIZES = ["input--small-1", "input--medium-1", "input--large-1"];

export const InputTextUserProfile = ({
  label,
  placeholder,
  onChange,
  type,
  value,
  name,
  inputSize,
  editable,
  editModeInput,
  setEditMode,
  children
}) => {
  const checkInputSize = SIZES.includes(inputSize) ? inputSize : SIZES[0];

  return (
    <div className={`input-text-user-profile-container ${checkInputSize}`}>
      <label htmlFor={name}>{label}</label>
      <div
        className={`input-text-user-profile ${
          editModeInput === name ? "focusInput" : ""
        }`}
      >
        <input
          disabled={editModeInput !== name}
          id={name}
          name={name}
          placeholder={placeholder}
          onChange={onChange}
          type={type}
          value={value}
          editable={editable}
        >
          {children}
        </input>

        <div className="input-text-user-container-icon">
          {editable === "true" && (
            <img
              className="input-text-user-profile-edit-icon"
              onClick={() => {
                // setEditMode(edition => !edition);
                setEditMode(name);
              }}
              src={editIcon}
              alt=""
            />
          )}
        </div>
      </div>
    </div>
  );
};
