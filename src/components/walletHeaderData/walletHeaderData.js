import React from "react";
import iconFilter from "../../utils/icon-filtrar.svg";
import iconCalendar from "../../utils/calendar (1).svg";
import ArrowDown from "../../utils/Trazado 26.svg";
import { Divider } from "../divider/divider";
import "./walletHeaderData.css";

export const WalletHeaderData = () => {
  return (
    <div className="wallet-header-data-container">
      <div>FECHA CONCENTRACION</div>
      <div>FECHA LIQUIDACION</div>
      <div>INST.</div>
      <div>PRECIO</div>
      <div>CANT</div>
      <div className="currency icon">
        <img src={iconFilter} alt="..." />
        <Divider name="XXS" />
        <div>MONEDA</div>
      </div>
      <div className="amount">MONTO/NETO</div>
      <div className="state icon">
        <img src={iconFilter} alt="..." />
        <Divider name="XXS" />
        <div>ESTADO</div>
      </div>
      <div></div>
    </div>
  );
};
