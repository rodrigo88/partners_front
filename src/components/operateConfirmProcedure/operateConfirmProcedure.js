import React from "react";
import { Divider } from "../divider/divider";
import { useSelector, useDispatch } from "react-redux";
import iconLeftArrow from "../../utils/Grupo 4084.svg";
import iconRadio from "../../utils/Outer Ellipse2.svg";
// import iconRadioInt from "../../utils/Inner Ellipse.svg";
import "./operateConfirmProcedure.css";

export const OperateConfirmProcedure = ({
  selectedInstrument,
  dataTable,
  openConfirmProcedure,
  cancelProcedure,
  openModalFinish,
  resultConfirm
}) => {
  const horaFondo = 15;
  const horaBolsa = 12;

  const state = useSelector(state => state);
  const dispatch = useDispatch();
  const { detailEspecie } = state;

  const peopleList = dataTable.filter(x => x.physicalPerson);
  const totalPeopleSum = peopleList.reduce((acum, x) => acum + x.price, 0);
  const totalPeopleQSum = peopleList.reduce(
    (acum, x) => acum + parseInt(x.quantity),
    0
  );

  const legalList = dataTable.filter(x => !x.physicalPerson);
  const totalLegalSum = legalList.reduce((acum, x) => acum + x.price, 0);
  const totalLegalQSum = legalList.reduce(
    (acum, x) => acum + parseInt(x.quantity),
    0
  );

  const assignClassName = () => {
    if (selectedInstrument.description === "Fondos") {
      if (horaFondo === 15) {
        return "nextStepRodrigo";
      } else {
        return "warningRodrigo";
      }
    } else {
      if (horaBolsa === 17) {
        return "nextStepRodrigo";
      } else {
        return "warningRodrigo";
      }
    }
  };

  const typeInstrument = () => {
    if (selectedInstrument && selectedInstrument.description === "Fondos") {
      return "Operatoria Fondos";
    } else {
      return "Operatoria Bolsa";
    }
  };

  const confirmOperation = table => {
    const instrument = selectedInstrument.description;
    const legend = assignClassName();
    openModalFinish(instrument, legend);
  };

  const total = totalPeopleSum + totalLegalSum;

  return (
    <div
      className={`operate-confirm-procedure-container 
      ${openConfirmProcedure ? "active-confirm-card" : ""}
      
      `}
    >
      <div className="title">
        <Divider name="XXL" />
        <img src={iconLeftArrow} alt="..." onClick={() => cancelProcedure()} />
        <Divider name="X" />
        <div>Resumen {typeInstrument()}</div>
      </div>
      <div className="data">
        <Divider name="L" />
        <DataFields
          titleHeader="Venta de Instrumento - Persona Física"
          clients={peopleList.length}
          total={totalPeopleSum}
          totalQSum={totalPeopleQSum}
          titleFooter="Subtotal P. Física"
          typeInstrument={typeInstrument()}
          detailEspecie={detailEspecie}
        />
        <Divider name="L" />
        <DataFields
          titleHeader="Venta de Instrumento - Persona Jurídica"
          clients={legalList.length}
          total={totalLegalSum}
          totalQSum={totalLegalQSum}
          titleFooter="Subtotal P. Jurídica"
          typeInstrument={typeInstrument()}
          detailEspecie={detailEspecie}
        />
      </div>
      <Divider name="M" />
      <div className="total-operation">
        {resultConfirm &&
          resultConfirm.instrument !== "Fondos" &&
          resultConfirm.legend === "warningRodrigo" && (
            <div>
              Permitir Reajustar precio al operar fuera de hora de mercado
            </div>
          )}
        <div>TOTAL OPERACIÓN:</div>
        <div>TOTAL OPERACIÓN:</div>
        <Divider name="L" />
        <div>$ {total}</div>
        <Divider name="L" />
      </div>
      <Divider name="L" />
      <div className={`final-confirm ${assignClassName()}`}>
        <img src={iconRadio} alt="..." />
        <Divider name="S" />
        {selectedInstrument.description !== "Fondos" ? (
          <>
            <div style={{ color: "#fff", fontSize: "14px" }}>Aceptar</div>
            <Divider name="XS" />
            <div className="cancel">Declaracion Jurada</div>
          </>
        ) : (
          <>
            <div style={{ color: "#fff", fontSize: "14px" }}>
              Declado haber recibido el
            </div>
            <Divider name="XS" />
            <div className="cancel">Reglamento de Gestión</div>
          </>
        )}
        <Divider name="X" />
        <div className="cancel" onClick={() => cancelProcedure()}>
          No realizar por ahora
        </div>
        <Divider name="M" />
        <div className="finish-ok" onClick={() => confirmOperation(dataTable)}>
          CONFIRMAR OPERACIÓN
        </div>
        <Divider name="X" />
        <Divider name="X" />
        <Divider name="X" />
      </div>
    </div>
  );
};

const DataFields = ({
  typeInstrument,
  titleHeader,
  titleFooter,
  clients,
  total,
  totalQSum,
  detailEspecie
}) => {
  if (typeInstrument === "Operatoria Bolsa") {
    return (
      <div className="data-fields">
        <ElementData
          myClass="my-header"
          description={titleHeader}
          value={
            detailEspecie && detailEspecie.ticker + " / " + detailEspecie.name
          }
        />
        <ElementData
          myClass="my-content"
          description="Precio"
          value={`AR$ ${detailEspecie &&
            detailEspecie.data.NOW_PRICE.toFixed(2)}`}
        />
        <ElementData
          myClass="my-content"
          description="Importe Bruto"
          value="AR$ x.xxx,xx"
        />
        <ElementData
          myClass="my-content"
          description="Cantidad de clientes"
          value={clients}
        />
        <ElementData
          myClass="my-content"
          description="Unidades"
          value={totalQSum}
        />
        <ElementData
          myClass="my-content"
          description="Gastos"
          value="ARS x.xxx,xx"
        />
        <ElementData
          myClass="my-content"
          description="Fecha de Ingreso"
          value="02/03/2020"
        />
        <ElementData
          myClass="my-content"
          description="Importe Neto"
          value="ARS xx.xxx,xx"
        />
        <TotalData
          myClass="my-footer"
          description={titleFooter}
          value={`ARS: ${total}`}
        />
      </div>
    );
  } else {
    return (
      <div className="data-fields-funds">
        <ElementData
          myClass="my-header"
          description={titleHeader}
          value={
            detailEspecie && detailEspecie.ticker + " / " + detailEspecie.name
          }
        />
        <ElementData
          myClass="my-content"
          description="Precio"
          value={`AR$ ${detailEspecie &&
            detailEspecie.data.NOW_PRICE.toFixed(2)}`}
        />
        <ElementData
          myClass="my-content"
          description="Cantidad de Cuotapartes"
          value={totalQSum}
        />
        <ElementData
          myClass="my-content"
          description="Cantidad de clientes"
          value={clients}
        />
        <ElementData
          myClass="my-footer"
          description="Fecha de Ingreso"
          value="02/03/2020"
        />
        <Divider name="X" />

        <TotalData
          myClass="my-footer"
          description={titleFooter}
          value={`ARS: ${total}`}
        />
      </div>
    );
  }
};

const ElementData = ({ myClass, description, value }) => {
  return (
    <div className={`${myClass} description-value`}>
      <div className="description">{description}</div>
      <div className="value">{value}</div>
    </div>
  );
};

const TotalData = ({ myClass, description, value }) => {
  return (
    <div className={`${myClass} description-value-total`}>
      <div className="description">{description}</div>
      <div className="value">{value}</div>
    </div>
  );
};
