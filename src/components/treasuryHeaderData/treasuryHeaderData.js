import React from "react";
import iconFilter from "../../utils/icon-filtrar.svg";
import iconCalendar from "../../utils/calendar (1).svg";
import ArrowDown from "../../utils/Trazado 26.svg";
import { Divider } from "../divider/divider";
import "./treasuryHeaderData.css";

export const TreasuryHeaderData = ({ option }) => {
  return (
    <div className="treasury-header-data-container">
      <Divider name="S" />
      <div className="date icon">
        <img src={iconCalendar} alt="..." />
        <Divider name="XXS" />
        <div>FECHA</div>
        <Divider name="XXS" />
        <img src={ArrowDown} alt="..." />
      </div>
      <div className="client">CLIENTE</div>
      <div className="account">CUENTA</div>
      <div className="tLiquida">T. LIQUIDA</div>
      <div className="detail icon">
        <img src={iconFilter} alt="..." />
        <Divider name="XXS" />
        <div>DETALLE</div>
      </div>
      <div className="currency icon">
        <img src={iconFilter} alt="..." />
        <Divider name="XXS" />
        <div>MONEDA</div>
      </div>
      <div className="amount">MONTO/NETO</div>
      <div className="state icon">
        <img src={iconFilter} alt="..." />
        <Divider name="XXS" />
        <div>ESTADO</div>
      </div>
      <div></div>
    </div>
  );
};
