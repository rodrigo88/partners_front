import React from "react";
import iconSearch from "../../utils/icon-buscar.svg";
import "./inputExactAmountValue.css";

const SIZES = ["input--small", "input--medium", "input--large"];

export const InputExactAmountValue = ({
  // text,
  // placeholder,
  // type,
  value,
  onChange,
  name,
  label,
  children
}) => {
  return (
    <div className="custom-input-exact-value-container">
      <div className="content">
        <input
          name={name}
          // className={`input`}
          // text={text}
          placeholder="$ 00.00"
          onChange={onChange}
          type="text"
          value={value}
        >
          {children}
        </input>
      </div>
      <div className="label">{label}</div>
    </div>
  );
};
