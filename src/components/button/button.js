import React from "react";
import "./button.css";

const STYLES = ["btn--primary", "btn--secondary", "btn--third"];

const SIZES = ["btn--small", "btn--medium", "btn--large"];

const XXX = ["arreglar1, arreglar2, arreglar3"];

export const Button = ({
  children,
  onClick,
  type,
  buttonStyle,
  buttonSize,
  arreglar
}) => {
  const btnStyle = STYLES.includes(buttonStyle) ? buttonStyle : STYLES[0];

  const btnSize = SIZES.includes(buttonSize) ? buttonSize : SIZES[0];

  const btnArreglar = XXX.includes(arreglar) ? arreglar : "";

  return (
    <button
      onClick={onClick}
      type={type}
      className={`btn ${btnStyle} ${btnSize} ${btnArreglar}`}
    >
      {children}
    </button>
  );
};
