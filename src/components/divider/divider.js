import React from "react";
import "./divider.css";

const NAMES = ["XXS", "XS", "S", "M", "L", "X", "XL", "XXL", "XXXL", "XXXXL"];

export const Divider = ({ name }) => {
  const checkName = NAMES.includes(name) ? name : NAMES[0];

  return <div className={`divider ${checkName}`} />;
};
