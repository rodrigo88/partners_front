import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { setExpanded } from "../../redux/actions";
import { Link } from "react-router-dom";
import LO from "../../utils/LO.png";
import point from "../../utils/Elipse 238.svg";
import "./sidebar.css";

export const Sidebar = () => {
  const state = useSelector(state => state);
  const { routes, currentUrl, expanded } = state;

  const dispatch = useDispatch();

  const items1 = routes && routes.filter(x => x.id <= 6);
  const items2 = routes && routes.filter(x => x.id > 6);

  return (
    <div className={`sidebar-container ${expanded ? "expanded" : ""}`}>
      <div className="sidebar-logo">
        <div className="header-logo">
          <img
            src={LO}
            alt="Banza"
            onClick={() => {
              window.innerWidth > 1800 && dispatch(setExpanded());
            }}
          />
        </div>
        <div className="header-title">{expanded && "Banza Partners"}</div>
      </div>

      <div className="sidebar-items">
        {items1.map((item, i) => (
          <Item
            selectedItem={currentUrl}
            item={item}
            key={i}
            expanded={expanded}
          />
        ))}
      </div>

      <div className="sidebar-support">
        {items2.map((item, i) => (
          <Item
            selectedItem={currentUrl}
            item={item}
            key={i}
            expanded={expanded}
          />
        ))}
      </div>
    </div>
  );
};

const Item = ({ item, selectedItem, expanded }) => {
  const isSelected = (item, selected) => item.id === selected;

  return (
    <Link to={item.url} className="link-router-dom">
      <div className="item-container">
        <div className="item-icon">
          <img
            className="item-icon-image"
            src={isSelected(item, selectedItem) ? item.imgSelected : item.image}
          />
          {!expanded && (
            <div>
              <div className={isSelected(item, selectedItem) && "selected"}>
                {item.title}
              </div>
              {isSelected(item, selectedItem) && (
                <div className="selected3">
                  <img src={point} />
                </div>
              )}
            </div>
          )}
        </div>
        <div
          className={`item-description ${
            isSelected(item, selectedItem) ? "selected2" : ""
          }`}
        >
          {expanded && (
            <div className={`${!expanded && "no-selectionable"}`}>
              {item.description}
            </div>
          )}
        </div>
        {!expanded && isSelected(item, selectedItem) && (
          <div className="mask-triangle" />
        )}
        {expanded && isSelected(item, selectedItem) && (
          <div className="mask-border" />
        )}
      </div>
    </Link>
  );
};
