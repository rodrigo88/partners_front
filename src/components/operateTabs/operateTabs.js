import React from "react";
import { Divider } from "../../components/divider/divider";
import "./operateTabs.css";

export const OperateTabs = ({
  optionItem,
  setOptionItem,
  closeActiveDetailCard
}) => {
  return (
    <div className="operate-dashboard-container-tabs">
      <Divider name="S" />
      <div
        className={`operate-dashboard-container-tabs-item ${
          optionItem === "OPERAR" ? "optionItemActive" : ""
        }`}
        onClick={() => setOptionItem("OPERAR")}
      >
        OPERAR
      </div>
      <Divider name="XXL" />
      <div
        className={`operate-dashboard-container-tabs-item ${
          optionItem === "ORDENES EN PROCESO" ? "optionItemActive" : ""
        }`}
        onClick={() => {
          setOptionItem("ORDENES EN PROCESO");
          closeActiveDetailCard();
        }}
      >
        ORDENES EN PROCESO
      </div>
      <Divider name="XXL" />
      <div
        className={`operate-dashboard-container-tabs-item ${
          optionItem === "ORDENES FINALIZADAS" ? "optionItemActive" : ""
        }`}
        onClick={() => setOptionItem("ORDENES FINALIZADAS")}
      >
        ORDENES FINALIZADAS
      </div>
    </div>
  );
};
