import React from "react";
import { Divider } from "../divider/divider";
import "./gridPercentageStatics.css";

export const GridPercentageStatics = ({ dataList, currency }) => {
  const totalBalance = () =>
    dataList.reduce((acc, x) => acc + parseFloat(x.value), 0);
  return (
    <div className="wallet-cartera-list">
      <div className="list">
        {dataList.map(x => {
          return (
            <div className="element" key={x.id}>
              <div className="point-icon" style={{ borderColor: x.color }} />
              <Divider name="S" />
              <div className="description">{x.name}</div>
              <div className="percentage">{x.value}</div>
            </div>
          );
        })}
      </div>

      <div className="line" />
      <div className="total">
        <div className="title">TOTAL TENENCIA</div>
        <Divider name="S" />
        <div className="value">
          {currency} {totalBalance().toFixed(2)}
        </div>
      </div>
    </div>
  );
};
