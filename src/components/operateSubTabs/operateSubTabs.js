import React from "react";
import "./operateSubTabs.css";

export const OperateSubTabs = ({
  subOptionItem,
  setSubOptionItem,
  selectedInstrument
}) => {
  return (
    <div>
      {selectedInstrument.description === "Fondos" ? (
        <div className="operate-sub-items-buy-sell">
          <div
            onClick={() => setSubOptionItem("SUSCRIBIR")}
            className={`sub-items-buy ${
              subOptionItem === "SUSCRIBIR" ? "selected-sub-item" : ""
            }`}
          >
            SUSCRIBIR
          </div>
          <div
            onClick={() => setSubOptionItem("RESCATAR")}
            className={`sub-items-sell ${
              subOptionItem === "RESCATAR" ? "selected-sub-item" : ""
            }`}
          >
            RESCATAR
          </div>
        </div>
      ) : (
        <div className="operate-sub-items-buy-sell">
          <div
            onClick={() => setSubOptionItem("COMPRAR")}
            className={`sub-items-buy ${
              subOptionItem === "COMPRAR" ? "selected-sub-item" : ""
            }`}
          >
            COMPRAR
          </div>
          <div
            onClick={() => setSubOptionItem("VENDER")}
            className={`sub-items-sell ${
              subOptionItem === "VENDER" ? "selected-sub-item" : ""
            }`}
          >
            VENDER
          </div>
        </div>
      )}
    </div>
  );
};
