import React from "react";
import "./inputText.css";

const SIZES = ["input--small", "input--medium", "input--large"];

export const InputText = ({
  text,
  placeholder,
  onChange,
  type,
  value,
  defaultValue,
  name,
  children,
  inputSize
}) => {
  const checkInputSize = SIZES.includes(inputSize) ? inputSize : SIZES[0];

  return (
    <input
      name={name}
      className={`input-text ${checkInputSize}`}
      text={text}
      placeholder={placeholder}
      onChange={onChange}
      type={type}
      value={value}
      defaultValue={defaultValue}
    >
      {children}
    </input>
  );
};
