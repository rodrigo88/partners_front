import React, { useState } from "react";
import { Divider } from "../divider/divider";
import { useSelector } from "react-redux";
import { SelectClientsWallet } from "../selectClientsWallet/selectClientsWallet";
import { WalletComponenteRaro } from "../walletComponenteRaro/walletComponenteRaro";
import { InputTextSearchClean } from "../inputTextSearchClean/inputTextSearchClean";
import iconPerfil from "../../utils/account_circle-24px (1).svg";
import iconCopy from "../../utils/icon-copy.svg";
// import iconGroup from "../../utils/icon-grupo.svg";
import iconAmp from "../../utils/icon-ampliar.svg";
import iconReduce from "../../utils/icon-ampliar (1).svg";
import iconMainPartner from "../../utils/icon-mercado-staron.svg";
import iconDots from "../../utils/hamburger-button.svg";
import { LoadingView } from "../../views/portfolio/portfolio";
import "./teamLeft.css";

export const TeamLeft = ({ setProducer, setSelectionPopupMenu }) => {
  const state = useSelector(state => state);
  const { userLogged, teamsPartner, loadingTeamsPartner } = state;

  return (
    <div className="team-dashboard-container-left">
      <div className="header">
        <div className="title">Mi Team</div>
        <Divider name="M" />
        <div className="referCode">
          Cód. Referido <strong>{userLogged.referredCode}</strong>
        </div>
        <Divider name="XS" />
        <div className="image">
          <img src={iconCopy} alt="..." />
        </div>
      </div>
      <div className="componente-raro">
        <WalletComponenteRaro />
      </div>
      <Divider name="M" />
      <div className="table-container">
        <div className="title">
          <Divider name="X" />
          <div>Productores a cargo</div>
        </div>
        <Divider name="S" />
        <div className="table">
          {!teamsPartner || loadingTeamsPartner ? (
            <LoadingView />
          ) : (
            <>
              {teamsPartner.teamleader &&
                teamsPartner.teamleader
                  .concat(teamsPartner.producers)
                  .map((leader, i) => (
                    <Leader
                      key={i}
                      leader={leader}
                      setProducer={setProducer}
                      setSelectionPopupMenu={setSelectionPopupMenu}
                    />
                  ))}
            </>
          )}
        </div>
      </div>
    </div>
  );
};

const Leader = ({ leader, setProducer, setSelectionPopupMenu }) => {
  const [open, setOpen] = useState(false);
  const [openPopupMenu, setOpenPopupMenu] = useState(false);

  const clickOnPopUpElement = idElement =>
    idElement === "popup-menu-element" && setOpenPopupMenu(false);

  const optionsPopUpElement = [
    { id: 0, description: "REASIGNAR CARTERA" },
    { id: 1, description: "DESHABILITAR CUENTA" },
    { id: 2, description: "CAMBIAR ROL" },
    { id: 3, description: "Enviar Mensaje" }
  ];

  return (
    <div className={`element ${open ? "remake-left-border" : ""}`}>
      <div className={`main-producer ${open ? "line-producer" : ""}`}>
        <div>
          <figure>
            <img src={iconPerfil} alt="..." />
          </figure>
        </div>
        <div>
          <figure>
            <img src={iconMainPartner} alt="..." />
          </figure>
        </div>
        <div className="fullname">{leader.fullName}</div>
        <div className="team">
          {!open && leader.totalMyPortfolio && "T" + leader.totalMyPortfolio}
          {/* {!open && (
            <div>
              {leader.totalMyPortfolio ? "T" + leader.totalMyPortfolio : "T0"}
            </div>
          )} */}
        </div>
        <div className="tamanio">{!open && "C 256"}</div>
        <div
          className="icon-options"
          onClick={() => {
            setProducer(leader);
            setOpenPopupMenu(x => !x);
          }}
        >
          <figure>
            <img src={iconDots} alt="..." />
          </figure>
        </div>
      </div>
      <div className={`sublist-element ${open ? "open-producer" : ""}`}>
        {leader.producers &&
          leader.producers.map((p, i) => {
            return (
              <div className="sublist-item" key={i}>
                <div>
                  <figure>
                    <img src={iconMainPartner} alt="..." />
                  </figure>
                </div>
                <div>{p.fullName}</div>
                <div>{p.totalPortfolio}</div>
                <div className="icon">
                  <figure>
                    <img src={iconDots} alt="..." />
                  </figure>
                </div>
              </div>
            );
          })}
      </div>
      <div
        className={`agents-producer ${open ? "remake-top" : ""}`}
        style={{ opacity: !leader.producers && "0.5" }}
      >
        <div className="icon" style={{ cursor: leader.producers && "pointer" }}>
          <figure>
            <img
              src={open ? iconReduce : iconAmp}
              alt="..."
              onClick={() => leader.producers && setOpen(x => !x)}
            />
          </figure>
        </div>
        <Divider name="S" />
        <div className="show-team">VER TEAM</div>
      </div>
      <div
        onClick={e => clickOnPopUpElement(e.target.id)}
        id="popup-menu-element"
        className={`popup-menu-element ${
          openPopupMenu ? "visible-popup-menu-element" : ""
        }`}
      >
        <div
          className="content-popup-menu-element"
          id="content-popup-menu-element"
        >
          {optionsPopUpElement.map((item, i) => {
            return (
              <div
                key={i}
                className={`content-popup-menu-elemen-option ${
                  i !== optionsPopUpElement.length - 1 ? "middle-element" : ""
                }`}
                onClick={() => {
                  setSelectionPopupMenu(item.description);
                  setOpenPopupMenu(x => !x);
                }}
              >
                {item.description}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
