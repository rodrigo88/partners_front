import React from "react";
import { Divider } from "../divider/divider";

import "./operateDetailProcess.css";

export const OperateDetailProcess = ({
  activeDetailProcess,
  selectedPendingProcess
}) => {
  return (
    <div
      className={`operate-detail-process-container ${
        activeDetailProcess ? "active-detail-process-card" : ""
      }`}
    >
      <div className="detail-process-info">
        <DetailField value={"Última órden"} />
        <Divider name="M" />
        <DetailField
          field={"Detalle"}
          value={selectedPendingProcess && selectedPendingProcess.detalle}
        />
        <Divider name="XS" />
        <DetailField
          field={"Instrumento"}
          value={selectedPendingProcess && selectedPendingProcess.instrumento}
        />
        <Divider name="M" />
        <DividerLine />
        <Divider name="XS" />
        <DetailField
          field={"Importe Bruto"}
          value={selectedPendingProcess && selectedPendingProcess.precio}
          coinValue={true}
        />
        <Divider name="XS" />
        <div className="horizontal-distribution">
          <DetailField
            field={"Unidades"}
            value={selectedPendingProcess && selectedPendingProcess.cantidad}
          />
          <DetailField field={"Gastos"} value={"AR$ 00"} />
        </div>
        <Divider name="XS" />
        <DetailField
          field={"Fecha De Ingreso"}
          value={selectedPendingProcess && selectedPendingProcess.concentracion}
        />
        <Divider name="XS" />
        <DetailField
          field={"Importe Neto"}
          value={selectedPendingProcess && selectedPendingProcess.neto}
          coinValue={true}
        />
      </div>
    </div>
  );
};

const DetailField = ({ field, value, coinValue }) => {
  return (
    <div>
      <div className="title-detail-process">{field}</div>
      <Divider name="XS" />
      <div
        className={`content-detail-process`}
        style={{ color: field === "Importe Bruto" && "#737DFF" }}
      >
        {coinValue && "AR$ "}
        {value}
      </div>
    </div>
  );
};

const DividerLine = () => (
  <div
    style={{
      display: "flex",
      flexDirection: "column",
      alignItems: "flex-start",
      justifyContent: "flex-start",
      width: "93%",
      height: "1px",
      background: "#a4a7be",
      color: "#a4a7be"
    }}
    alt="..."
  />
);
