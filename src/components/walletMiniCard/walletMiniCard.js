import React, { useState } from "react";
import { Divider } from "../divider/divider";
import { SelectProfileCoin } from "../selectProfileCoin/selectProfileCoin";
import iconConversion from "../../utils/conversion.svg";
import "./walletMiniCard.css";

export const WalletMiniCard = ({
  icon,
  title,
  value,
  equivalente,
  valueEquivalente
}) => {
  const [selectionCoin, setSelectionCoin] = useState({
    value: 0,
    description: "ARS"
  });

  const optionsCoin = [
    { value: 0, description: "ARS" },
    { value: 1, description: "USD" }
  ];

  return (
    <div className="wallet-mini-card-container">
      <div className="wallet-mini-card-container-top">
        <SelectProfileCoin
          options={optionsCoin}
          selection={selectionCoin}
          setSelectionCoin={setSelectionCoin}
        />
        <div>
          <img src={icon} alt="..." />
        </div>
      </div>
      <div className="wallet-mini-card-container-medium">
        <div className="wallet-mini-card-container-medium-title">{title}</div>
        <div className="wallet-mini-card-container-medium-value">{value}</div>
      </div>
      <Divider name="S" />
      <div className="wallet-mini-card-container-down">
        <div className="wallet-mini-card-container-down-icon">
          <img src={iconConversion} alt="..." />
        </div>
        <div className="wallet-mini-card-container-down-description">
          <div className="wallet-mini-card-container-down-title">
            Equivalente {equivalente}
          </div>
          <Divider name="XXS" />
          <div className="wallet-mini-card-container-down-value">
            {valueEquivalente}
          </div>
        </div>
      </div>
      <div className="wallet-mini-card-container-line"></div>
    </div>
  );
};
