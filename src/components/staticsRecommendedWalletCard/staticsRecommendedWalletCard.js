import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

import "./staticsRecommendedWalletCard.css";

am4core.useTheme(am4themes_animated);

export default class StaticsRecommendedWalletCard extends React.Component {
  componentDidMount() {
    let chart = am4core.create("chartdiv", am4charts.PieChart);

    console.log("AGRESIVA FUNDS: ", this.props.data.funds);
    chart.data = this.props && this.props.data.funds;

    // [
    //   {
    //     country: "Argentina",
    //     litres: "40",
    //     color: am4core.color("red")
    //   },
    //   {
    //     country: "Brasil",
    //     litres: "20",
    //     color: am4core.color("#1f1fe1")
    //   },
    //   {
    //     country: "España",
    //     litres: "10",
    //     color: am4core.color("#e1ce1f")
    //   },
    //   {
    //     country: "Francia",
    //     litres: "10",
    //     color: am4core.color("#07b075")
    //   },
    //   {
    //     country: "Alemania",
    //     litres: "20",
    //     color: am4core.color("aqua")
    //   }
    // ];

    let pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "percentage";
    pieSeries.dataFields.category = "name";
    pieSeries.labels.template.disabled = true;
    pieSeries.ticks.template.disabled = true;

    this.chart = chart;
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  render() {
    return (
      <div id="chartdiv" style={{ width: "100%", height: "500px" }}>
        {this.props.data.funds.map((fund, i) => (
          <div key={i}>
            {fund.name}- {fund.percentage}
          </div>
        ))}
      </div>
    );
  }
}
