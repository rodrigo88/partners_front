import React from "react";
import { Divider } from "../divider/divider";
import "./operateFooter.css";

export const OperateFooter = ({
  selectedInstrument,
  selectedEspecie,
  selectedWallet,
  dataTable,
  totalQuantity,
  totalPrice,
  confirmProcedure
}) => {
  const validValuePrice = typeof totalPrice === "number" && totalPrice > 0;
  const validValueQuantity =
    typeof totalQuantity === "number" && totalQuantity > 0;
  return (
    <div className="operate-dashboard-container-footer">
      <Divider name="XXS" />
      <Divider name="XXS" />
      <div className="operate-dashboard-container-footer-totals">
        <div className="title-total">{""} </div>
        <Divider name="XXS" />
        <Divider name="XXS" />
        <div className="numbers-total">TOTAL:</div>
        <Divider name="XXL" />
        <div className="total-quantity-price">{totalQuantity}</div>
        <Divider name="L" />
        <Divider name="XXS" />
        <div className="total-quantity-price">{totalPrice + ""}</div>
        <div className="total-limit">Limite</div>
      </div>
      <Divider name="M" />
      <div
        className={`operate-dashboard-container-footer-confirm-btn ${
          validValuePrice || validValueQuantity ? "active-button" : ""
        }`}
        style={{
          pointerEvents:
            selectedEspecie.description === "" &&
            selectedWallet.description === "" &&
            "none"
        }}
        onClick={() => confirmProcedure()}
      >
        VER ORDEN Y CONFIRMAR
      </div>
    </div>
  );
};
