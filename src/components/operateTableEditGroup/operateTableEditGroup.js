import React from "react";
import { Divider } from "../../components/divider/divider";
import "./operateTableEditGroup.css";

export const OperateTableEditGroup = ({
  quantityOperateTable,
  priceOperateTable,
  onChangeQuantity,
  onChangePrice
}) => {
  return (
    <div className="operate-table-edit-group-container">
      <div className="main">
        <div className="text">
          Ingrese Valores para Operar en Lote o Individual:
        </div>
        <div className="quantity">
          <CustomInputValue
            value={quantityOperateTable}
            onChange={onChangeQuantity}
            name="quantity"
            disabled={priceOperateTable !== ""}
          />
        </div>
        <Divider name="XS" />
        <div className="price">
          <CustomInputValue
            value={priceOperateTable}
            onChange={onChangePrice}
            name="price"
            disabled={quantityOperateTable !== ""}
          />
        </div>
        <div className="limit"></div>
      </div>
    </div>
  );
};

const CustomInputValue = ({ value, onChange, name, disabled }) => {
  return (
    <div
      style={{ border: value !== "" ? "2px solid green" : "" }}
      className="custom-input-price"
    >
      <input
        disabled={disabled}
        type="text"
        placeholder={name === "quantity" ? "0" : "$0"}
        value={value}
        onChange={onChange}
      />
    </div>
  );
};
