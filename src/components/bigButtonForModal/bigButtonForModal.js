import React from "react";
import "./bigButtonForModal.css";

export const BigButtonForModal = ({ icon, title, mainColor, onClick }) => {
  return (
    <div
      className={`big--button--for--modal--container ${
        mainColor === "white" ? "background--white" : "background--blue"
      }`}
      onClick={onClick}
    >
      <img
        className="big--button--for--modal--container--icon"
        src={icon}
        alt="..."
      />
      <div className="big--button--for--modal--container--title">{title}</div>
    </div>
  );
};
