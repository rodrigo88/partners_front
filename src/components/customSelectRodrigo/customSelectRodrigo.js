import React, { useState } from "react";
import ArrowDown from "../../utils/Trazado 26.svg";
import "./customSelectRodrigo.css";

export const CustomSelectRodrigo = ({
  options,
  selection,
  setSelectionInstrument
}) => {
  const [openSelect, setOpenSelect] = useState(false);

  return (
    <div style={{ position: "relative" }}>
      <button
        className="custom--select--container"
        onClick={() => setOpenSelect(x => !x)}
      >
        <div>
          {selection.value === -1 ? "Selección" : selection.description}
        </div>
        <ArrowDirection open={openSelect} />
      </button>

      <div
        className={`custom--select--container--options ${
          openSelect ? "open--select" : ""
        }`}
      >
        {options.map((opt, i) => {
          return (
            <div
              className={`custom--select--container--options--option ${
                i !== 0 ? "border--top" : ""
              }`}
              key={i}
              onClick={() => {
                setSelectionInstrument(opt);
                setOpenSelect(false);
              }}
            >
              {opt.description}
            </div>
          );
        })}
      </div>
    </div>
  );
};

const ArrowDirection = ({ open }) => (
  <img className={open ? "arrow-invert" : ""} src={ArrowDown} alt="..." />
);
