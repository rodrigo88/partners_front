import React, { useState } from "react";
import { Button } from "../../components/button/button";
import { InputTextMovementsProof } from "../../components/inputTextMovemenProof/inputTextMovementsProof";
import { SelectMovementsProof } from "../../components/selectMovementsProof/selectMovementsProof";
import { InputExactAmountValue } from "../../components/inputExactAmountValue/inputExactAmountValue";
import { Divider } from "../divider/divider";
import iconClose from "../../utils/Grupo 4165.svg";
import "./treasuryModalProof.css";

export const TreasuryModalProof = ({ openModal, closeModal }) => {
  const [data, setData] = useState({
    name: "",
    currency: "",
    account: "",
    value: ""
  });

  const [selectionCurrency, setSelectionCurrency] = useState({
    value: -1,
    description: ""
  });
  const optionsCurrency = [
    { value: 0, description: "Pesos" },
    { value: 1, description: "Dolares" },
    { value: 2, description: "Patacones" }
  ];

  const [selectionCBU, setSelectionCBU] = useState({
    value: -1,
    description: ""
  });
  const optionsCBU = [
    { value: 0, description: "0025 4589 4523 8264" },
    { value: 1, description: "7428 4589 8264 1974" },
    { value: 2, description: "3689 8264 1974 7389" },
    { value: 3, description: "8264 1974 7389 9812" },
    { value: 4, description: "1974 7389 9812 7364" },
    { value: 5, description: "7389 9812 7364 3569" }
  ];

  const onChange = e => setData({ ...data, [e.target.name]: e.target.value });

  const modalName = "custom--modal--proof--container";
  const clickModal = e => e.target.id === modalName && closeModal();

  const showFormData = () => {
    const { name, value } = data;
    const currency = selectionCurrency.description;
    const cbu = selectionCBU.description;
    const formData = { name, currency, cbu, value };
    console.log(formData);
  };

  return (
    <div
      id="custom--modal--proof--container"
      className={`custom--modal--proof--container ${
        openModal ? "open-modal" : ""
      }`}
      onClick={clickModal}
    >
      <div
        id="custom--modal--proof--container--content"
        className="region"
        onClick={clickModal}
      >
        <div className="content">
          <div className="cancel">
            <img src={iconClose} alt="..." onClick={closeModal} />
          </div>
          <div className="title">Ingresar comprobantes</div>
          <Divider name="XXL" />
          <div className="names">
            <div className="name">
              <InputTextMovementsProof
                type="text"
                label="Nombre del cliente"
                name="name"
                placeholder="Apellido y nombre del cliente"
                value={data.name}
                onChange={onChange}
              />
            </div>
          </div>
          <Divider name="L" />
          <div className="selections">
            <div className="currency">
              <SelectMovementsProof
                label="Moneda"
                title="..."
                options={optionsCurrency}
                selection={selectionCurrency}
                setSelection={setSelectionCurrency}
              />
            </div>
            <Divider name="S" />
            <div className="account">
              <SelectMovementsProof
                label="Seleccione cuenta de origen"
                title="..."
                options={optionsCBU}
                selection={selectionCBU}
                setSelection={setSelectionCBU}
              />
            </div>
          </div>
          <Divider name="XXL" />
          <div className="values">
            <div className="value">
              <InputExactAmountValue
                value={data.value}
                onChange={onChange}
                name="value"
                label="Ingrese valor exacto"
              />
            </div>
          </div>
          <Divider name="XL" />
          <div className="files">
            <div className="file"></div>
          </div>
          <Divider name="L" />
          <div className="buttons">
            <div className="load">
              <Button
                type="button"
                buttonStyle="btn--primary"
                arreglar="arreglar3"
                onClick={() => showFormData()}
              >
                CARGAR ARCHIVO
              </Button>
            </div>
            <Divider name="S" />
            <div className="no-load">No cargar por ahora</div>
          </div>
        </div>
      </div>
    </div>
  );
};
