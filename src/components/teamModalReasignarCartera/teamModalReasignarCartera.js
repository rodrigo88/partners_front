import React, { useState } from "react";
import iconClose from "../../utils/Grupo 4165.svg";
// import { Button } from "../button/button";
// import { InputTextMovementsProof } from "../inputTextMovemenProof/inputTextMovementsProof";
// import { SelectMovementsProof } from "../selectMovementsProof/selectMovementsProof";
// import { InputExactAmountValue } from "../inputExactAmountValue/inputExactAmountValue";
// import { Divider } from "../divider/divider";
import "./teamModalReasignarCartera.css";

export const TeamModalReasignarCartera = ({ openModal, closeModal }) => {
  const modalName = "custom--modal--reasignar--cartera--container";
  const clickModal = e => e.target.id === modalName && closeModal();

  return (
    <div
      id="custom--modal--reasignar--cartera--container"
      className={`custom--modal--reasignar--cartera--container ${
        openModal ? "open--modal--reasignar" : ""
      }`}
      onClick={clickModal}
    >
      <div
        id="custom--modal--reasignar--cartera--container--content"
        className="custom--modal--reasignar--cartera--container--content--region"
        onClick={clickModal}
      >
        <div className="custom--modal--reasignar--cartera--container--content--region--content">
          <div className="custom--modal--reasignar--cartera--container--content--region--content--cancel">
            <img src={iconClose} alt="..." onClick={closeModal} />
          </div>
          <div className="title">Reasignar Cartera</div>
        </div>
      </div>
    </div>
  );
};
