import React from "react";
import { Divider } from "../divider/divider";
// import iconCopy from "../../utils/icon-copy.svg";
// import { useSelector } from "react-redux";
import "./treasuryTabs.css";

export const TreasuryTabs = ({ option, setOption }) => {
  return (
    <div className="treasury-tabs-container">
      <Divider name="XL" />
      <div
        className={`option ${option === "TOTAL" ? "selected" : ""}`}
        onClick={() => setOption("TOTAL")}
      >
        {" "}
        TOTAL{" "}
      </div>
      <Divider name="XL" />
      <div
        className={`option ${
          option === "OPERACIONES VENTAS" ? "selected" : ""
        }`}
        onClick={() => setOption("OPERACIONES VENTAS")}
      >
        {" "}
        OPERACIONES VENTAS{" "}
      </div>
      <Divider name="XL" />
      <div
        className={`option ${
          option === "OPERACIONES COMPRA" ? "selected" : ""
        }`}
        onClick={() => setOption("OPERACIONES COMPRA")}
      >
        {" "}
        OPERACIONES COMPRA{" "}
      </div>
      <Divider name="XL" />
      <div
        className={`option ${
          option === "DEPOSITOS CLIENTES" ? "selected" : ""
        }`}
        onClick={() => setOption("DEPOSITOS CLIENTES")}
      >
        {" "}
        DEPOSITOS CLIENTES{" "}
      </div>
      <Divider name="XL" />
      <div
        className={`option ${
          option === "EXTRACCIONES CLIENTES" ? "selected" : ""
        }`}
        onClick={() => setOption("EXTRACCIONES CLIENTES")}
      >
        {" "}
        EXTRACCIONES CLIENTES{" "}
      </div>
      <div className="line" />
    </div>
  );
};
