/* ********************************************************************* */
export const SET_OPEN_BLUR = "SET_OPEN_BLUR";
/* ********************************************************************* */
/* ********************************************************************* */
export const LOAD_USER_INFORMATION = "LOAD_USER_INFORMATION";
export const SET_USER_INFORMATION = "SET_USER_INFORMATION";
/* ********************************************************************* */
/* ********************************************************************* */
/* ********************************************************************* */
export const GET_USERS = "GET_USERS";
export const REMOVE_USER = "REMOVE_USER";
/* ********************************************************************* */
export const ADD_USER = "ADD_USER";
export const SET_AUTH = "SET_AUTH";
/* ********************************************************************* */
export const SET_ROUTES = "SET_ROUTES";
export const SET_SELECTED_ROUTE = "SET_SELECTED_ROUTE";
/* ********************************************************************* */
export const SET_EXPANDED = "SET_EXPANDED";
/* ********************************************************************* */
// export const SET_LOG_OUT = "SET_LOG_OUT";
export const SET_UPDATE_USER = "SET_UPDATE_USER";
/* ********************************************************************* */
export const SET_SUCCESSFUL_UPDATE_USER = "SET_SUCCESSFUL_UPDATE_USER";
export const SET_RESPONSE_BILLING = "SET_RESPONSE_BILLING";
/* ********************************************************************* */
export const LOAD_USER_DATA = "LOAD_USER_DATA";
export const SET_USER_DATA = "SET_USER_DATA";
/* ********************************************************************* */
export const LOAD_CLIENTS = "LOAD_CLIENTS";
export const SET_CLIENTS = "SET_CLIENTS";
/* ********************************************************************* */
export const LOAD_RECOMMENDED_WALLETS = "LOAD_RECOMMENDED_WALLETS";
export const SET_RECOMMENDED_WALLETS = "SET_RECOMMENDED_WALLETS";
/* ********************************************************************* */
export const LOAD_PROVINCES_DATA = "LOAD_PROVINCES_DATA";
export const SET_PROVINCES_DATA = "SET_PROVINCES_DATA";
/* ********************************************************************* */
export const LOAD_COIN_MERVAL_DATA = "LOAD_COIN_MERVAL_DATA";
export const SET_COIN_MERVAL_DATA = "SET_COIN_MERVAL_DATA";
/* ********************************************************************* */
export const LOAD_COIN_PRICES_DATA = "LOAD_COIN_PRICES_DATA";
export const SET_COIN_PRICES_DATA = "SET_COIN_PRICES_DATA";
/* ********************************************************************* */
export const LOAD_FUNDS_DATA = "LOAD_FUNDS_DATA";
export const SET_FUNDS_DATA = "SET_FUNDS_DATA";
/* ********************************************************************* */
export const LOAD_CEDEARS_DATA = "LOAD_CEDEARS_DATA";
export const SET_CEDEARS_DATA = "SET_CEDEARS_DATA";
/* ********************************************************************* */
export const LOAD_EQUITY_DATA = "LOAD_EQUITY_DATA";
export const SET_EQUITY_DATA = "SET_EQUITY_DATA";
/* ********************************************************************* */
export const LOAD_BONDS_DATA = "LOAD_BONDS_DATA";
export const SET_BONDS_DATA = "SET_BONDS_DATA";
/* ********************************************************************* */
export const LOAD_USER_WALLET_CLIENTS = "LOAD_USER_WALLET_CLIENTS";
export const SET_USER_WALLET_CLIENTS = "SET_USER_WALLET_CLIENTS";
export const LOAD_USER_WALLET_CLIENTS2 = "LOAD_USER_WALLET_CLIENTS2";
export const SET_USER_WALLET_CLIENTS2 = "SET_USER_WALLET_CLIENTS2";
/* ********************************************************************* */
export const LOAD_DETAIL_ESPECIE = "LOAD_DETAIL_ESPECIE";
export const SET_DETAIL_ESPECIE = "SET_DETAIL_ESPECIE";
/* ********************************************************************* */
export const LOAD_USER_BALANCE = "LOAD_USER_BALANCE";
export const SET_USER_BALANCE = "SET_USER_BALANCE";
/* ********************************************************************* */
export const LOAD_TREASURY_DATA = "LOAD_TREASURY_DATA";
export const SET_TREASURY_DATA = "SET_TREASURY_DATA";
/* ********************************************************************* */
export const LOAD_MOVEMENTS = "LOAD_MOVEMENTS";
export const SET_MOVEMENTS = "SET_MOVEMENTS";
/* ********************************************************************* */
export const LOAD_OPERATIONS = "LOAD_OPERATIONS";
export const SET_OPERATIONS = "SET_OPERATIONS";
/* ********************************************************************* */
export const LOAD_LATEST_NEWS = "LOAD_LATEST_NEWS";
export const SET_LATEST_NEWS = "SET_LATEST_NEWS";
/* ********************************************************************* */
export const LOAD_LIST_MARKET_INDEX = "LOAD_LIST_MARKET_INDEX";
export const SET_LIST_MARKET_INDEX = "SET_LIST_MARKET_INDEX";
/* ********************************************************************* */
export const LOAD_MARKET_CARDS = "LOAD_MARKET_CARDS";
export const SET_MARKET_CARDS = "SET_MARKET_CARDS";
/* ********************************************************************* */
export const SET_LOG_OUT = "SET_LOG_OUT";
export const SET_LOG_IN = "SET_LOG_IN";
/* ********************************************************************* */
export const LOAD_TEAMS_PARTNER = "LOAD_TEAMS_PARTNER";
export const SET_TEAMS_PARTNER = "SET_TEAMS_PARTNER";
