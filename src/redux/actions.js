import axios from "axios";
import {
  // // SET_AUTH,
  // SET_ROUTES,
  // SET_SELECTED_ROUTE,
  // SET_EXPANDED,
  // SET_LOG_OUT,
  // SET_LOG_IN,
  SET_LOG_IN,
  SET_OPEN_BLUR,
  GET_USERS,
  REMOVE_USER,
  ADD_USER,
  SET_AUTH,
  SET_ROUTES,
  SET_SELECTED_ROUTE,
  SET_EXPANDED,
  SET_LOG_OUT,
  SET_UPDATE_USER,
  SET_SUCCESSFUL_UPDATE_USER,
  SET_RESPONSE_BILLING,
  LOAD_CLIENTS,
  SET_CLIENTS,
  LOAD_MOVEMENTS,
  SET_MOVEMENTS,
  LOAD_USER_DATA,
  SET_USER_DATA,
  LOAD_LATEST_NEWS,
  SET_LATEST_NEWS,
  LOAD_LIST_MARKET_INDEX,
  SET_LIST_MARKET_INDEX,
  LOAD_MARKET_CARDS,
  SET_MARKET_CARDS,
  LOAD_USER_INFORMATION,
  SET_USER_INFORMATION,
  LOAD_OPERATIONS,
  SET_OPERATIONS,
  LOAD_USER_WALLET_CLIENTS,
  SET_USER_WALLET_CLIENTS,
  LOAD_RECOMMENDED_WALLETS,
  SET_RECOMMENDED_WALLETS,
  LOAD_PROVINCES_DATA,
  SET_PROVINCES_DATA,
  LOAD_COIN_PRICES_DATA,
  SET_COIN_PRICES_DATA,
  LOAD_COIN_MERVAL_DATA,
  SET_COIN_MERVAL_DATA,
  LOAD_FUNDS_DATA,
  SET_FUNDS_DATA,
  LOAD_BONDS_DATA,
  SET_BONDS_DATA,
  LOAD_CEDEARS_DATA,
  SET_CEDEARS_DATA,
  LOAD_EQUITY_DATA,
  SET_EQUITY_DATA,
  LOAD_DETAIL_ESPECIE,
  SET_DETAIL_ESPECIE,
  LOAD_USER_BALANCE,
  SET_USER_BALANCE,
  LOAD_TREASURY_DATA,
  SET_TREASURY_DATA,
  LOAD_USER_WALLET_CLIENTS2,
  SET_USER_WALLET_CLIENTS2,
  LOAD_TEAMS_PARTNER,
  SET_TEAMS_PARTNER
} from "./actionTypes";

export const setOpenBlur = () => ({
  type: SET_OPEN_BLUR
});

export const mapCustomUsers = res => {
  const response = res.data.map(user => customUser(user));
  return response;
};

const customUser = user => {
  return {
    id: user.id,
    name: user.name,
    email: user.email
  };
};

export const getUsers = () => {
  return dispatch => {
    axios.get("https://jsonplaceholder.typicode.com/users").then(res =>
      dispatch({
        type: GET_USERS,
        payload: mapCustomUsers(res)
      })
    );
  };
};

export const removeUser = id => ({
  type: REMOVE_USER,
  payload: id
});

export const addUser = user => ({
  type: ADD_USER,
  payload: user
});

export const setAuth = () => ({
  type: SET_AUTH
});

export const setRoutes = routes => ({
  type: SET_ROUTES,
  payload: routes
});

export const setSelectedRoute = idRoute => ({
  type: SET_SELECTED_ROUTE,
  payload: idRoute
});

export const setExpanded = () => ({
  type: SET_EXPANDED
});
// **************************************
export const setLogOut = () => ({
  type: SET_LOG_OUT
});

// **************************************
export const setUpdateUser = user => ({
  type: SET_UPDATE_USER,
  payload: user
});
// **************************************
export const setSuccesfullUpdateUser = () => ({
  type: SET_SUCCESSFUL_UPDATE_USER
});

export const setResponseBilling = () => ({
  type: SET_RESPONSE_BILLING
});
// **************************************
export const loadClients = () => ({
  type: LOAD_CLIENTS
});

export const setClients = data => ({
  type: SET_CLIENTS,
  payload: data
});
// **************************************
export const loadRecommendedWallets = () => ({
  type: LOAD_RECOMMENDED_WALLETS
});

export const setRecommendedWallets = data => ({
  type: SET_RECOMMENDED_WALLETS,
  payload: data
});
// **************************************
export const loadCoinMervalData = () => ({
  type: LOAD_COIN_MERVAL_DATA
});

export const setCoinMervalData = data => ({
  type: SET_COIN_MERVAL_DATA,
  payload: data
});
// **************************************
export const loadFundsData = () => ({
  type: LOAD_FUNDS_DATA
});

export const setFundsData = data => ({
  type: SET_FUNDS_DATA,
  payload: data
});
// **************************************
export const loadCoinPricesData = () => ({
  type: LOAD_COIN_PRICES_DATA
});

export const setCoinPricesData = data => ({
  type: SET_COIN_PRICES_DATA,
  payload: data
});
// **************************************
export const loadBondsData = () => ({
  type: LOAD_BONDS_DATA
});

export const setBondsData = data => ({
  type: SET_BONDS_DATA,
  payload: data
});
// **************************************
export const loadCedearsData = () => ({
  type: LOAD_CEDEARS_DATA
});

export const setCedearsData = data => ({
  type: SET_CEDEARS_DATA,
  payload: data
});
// **************************************
export const loadEquityData = () => ({
  type: LOAD_EQUITY_DATA
});

export const setEquityData = data => ({
  type: SET_EQUITY_DATA,
  payload: data
});
// **************************************
export const loadDetailEspecie = () => ({
  type: LOAD_DETAIL_ESPECIE
});

export const setDetailEspecie = data => ({
  type: SET_DETAIL_ESPECIE,
  payload: data
});
// **************************************
export const loadProvincesData = () => ({
  type: LOAD_PROVINCES_DATA
});

export const setProvincesData = data => ({
  type: SET_PROVINCES_DATA,
  payload: data
});

// **************************************
export const loadUserBalance = () => ({
  type: LOAD_USER_BALANCE
});

export const setUserBalance = data => ({
  type: SET_USER_BALANCE,
  payload: data
});
// **************************************
export const loadTreasuryData = () => ({
  type: LOAD_TREASURY_DATA
});

export const setTreasuryData = data => ({
  type: SET_TREASURY_DATA,
  payload: data
});

// **************************************
export const loadUserWalletClients = () => ({
  type: LOAD_USER_WALLET_CLIENTS
});

export const setUserWalletClients = data => ({
  type: SET_USER_WALLET_CLIENTS,
  payload: data
});
export const loadUserWalletClients2 = () => ({
  type: LOAD_USER_WALLET_CLIENTS2
});

export const setUserWalletClients2 = data => ({
  type: SET_USER_WALLET_CLIENTS2,
  payload: data
});
// **************************************
export const loadMovements = () => ({
  type: LOAD_MOVEMENTS
});

export const setMovements = data => ({
  type: SET_MOVEMENTS,
  payload: data
});
// **************************************
export const loadUserOperations = () => ({
  type: LOAD_OPERATIONS
});

export const setUserOperations = data => ({
  type: SET_OPERATIONS,
  payload: data
});
// **************************************
export const loadUserData = () => ({
  type: LOAD_USER_DATA
});

export const setUserData = data => ({
  type: SET_USER_DATA,
  payload: data
});
// **************************************
export const loadLatestNews = () => ({
  type: LOAD_LATEST_NEWS
});

export const setLatestNews = data => ({
  type: SET_LATEST_NEWS,
  payload: data
});
// **************************************
export const loadListMarketIndex = () => ({
  type: LOAD_LIST_MARKET_INDEX
});

export const setListMarketIndex = data => ({
  type: SET_LIST_MARKET_INDEX,
  payload: data
});
// **************************************
export const loadMarketCards = () => ({
  type: LOAD_MARKET_CARDS
});

export const setMarketCards = data => ({
  type: SET_MARKET_CARDS,
  payload: data
});
// **************************************
export const loadTeamsPartner = () => ({
  type: LOAD_TEAMS_PARTNER
});

export const setTeamsPartner = data => ({
  type: SET_TEAMS_PARTNER,
  payload: data
});
// **************************************
export const loadUserInformation = () => ({
  type: LOAD_USER_INFORMATION
});

export const setUserInformation = data => ({
  type: SET_USER_INFORMATION,
  payload: data
});

// export const setLogOut = () => ({
//   type: SET_LOG_OUT
// });

export const setLogIn = () => ({
  type: SET_LOG_IN
});

// export {
//   getUsers,
//   removeUser,
//   addUser,
//   setAuth,
//   setRoutes,
//   setSelectedRoute,
//   setExpanded,
//   setLogOut,
//   setLogIn
// };
