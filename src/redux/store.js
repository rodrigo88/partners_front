import {
  GET_USERS,
  REMOVE_USER,
  ADD_USER,
  SET_AUTH,
  SET_ROUTES,
  SET_SELECTED_ROUTE,
  SET_EXPANDED,
  SET_LOG_OUT,
  SET_LOG_IN,
  SET_UPDATE_USER,
  SET_SUCCESSFUL_UPDATE_USER,
  SET_RESPONSE_BILLING,
  LOAD_CLIENTS,
  SET_CLIENTS,
  LOAD_MOVEMENTS,
  SET_MOVEMENTS,
  LOAD_USER_DATA,
  SET_USER_DATA,
  LOAD_LATEST_NEWS,
  SET_LATEST_NEWS,
  LOAD_LIST_MARKET_INDEX,
  SET_LIST_MARKET_INDEX,
  LOAD_MARKET_CARDS,
  SET_MARKET_CARDS,
  LOAD_USER_INFORMATION,
  SET_USER_INFORMATION,
  LOAD_OPERATIONS,
  SET_OPERATIONS,
  LOAD_USER_WALLET_CLIENTS,
  SET_USER_WALLET_CLIENTS,
  LOAD_USER_WALLET_CLIENTS2,
  SET_USER_WALLET_CLIENTS2,
  LOAD_RECOMMENDED_WALLETS,
  SET_RECOMMENDED_WALLETS,
  SET_OPEN_BLUR,
  LOAD_PROVINCES_DATA,
  SET_PROVINCES_DATA,
  LOAD_COIN_PRICES_DATA,
  SET_COIN_PRICES_DATA,
  LOAD_COIN_MERVAL_DATA,
  SET_COIN_MERVAL_DATA,
  LOAD_FUNDS_DATA,
  SET_FUNDS_DATA,
  LOAD_BONDS_DATA,
  SET_BONDS_DATA,
  LOAD_CEDEARS_DATA,
  SET_CEDEARS_DATA,
  LOAD_EQUITY_DATA,
  SET_EQUITY_DATA,
  LOAD_DETAIL_ESPECIE,
  SET_DETAIL_ESPECIE,
  LOAD_USER_BALANCE,
  SET_USER_BALANCE,
  LOAD_TREASURY_DATA,
  SET_TREASURY_DATA,
  LOAD_TEAMS_PARTNER,
  SET_TEAMS_PARTNER
} from "./actionTypes";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

const logger = store => next => action => {
  console.log("dispatching", action);
  let result = next(action);
  console.log("next state", store.getState());
  return result;
};

export const store = createStore(
  reducer,
  {
    loadingTeamsPartner: false,
    teamsPartner: null,

    loadingTreasuryData: false,
    treasuryData: [],

    loadingUserBalance: false,
    userBalance: [],

    detailEspecie: null,
    loadingDetailEspecie: false,

    loadingFundsData: false,
    fundsData: [],

    loadingEquityData: false,
    equityData: [],

    loadingCedearsData: false,
    cedearsData: [],

    loadingBondsData: false,
    bondsData: [],

    loadingCoinPricesData: false,
    coinPricesData: [],

    loadingCoinMervalData: false,
    coinMervalData: [],

    openBlur: false,

    loadingUserInformation: true,
    userInformation: {},

    loadingUserOperations: true,
    userOperations: {},

    loadingRecommendedWallets: true,
    recommendedWallets: {},

    users: [],
    isLogged: false,
    currentUrl: 0,

    routes: [],

    expanded: false,

    successfullChange: false,
    responseBilling: false,

    loadingUserLogged: true,
    userLogged: {
      mobile: "",
      comission: "",
      department: "",
      documentNumber: "",
      email: "",
      floor: "",
      fullName: "",
      location: "",
      name: "",
      number: "",
      phone: "",
      physical: "",
      postalCode: "",
      referredCode: "",
      street: "",
      birthDate: "",
      province: {
        codeProvince: "",
        nameProvince: ""
      },
      billingDetails: {
        businessName: "",
        cuit: "",
        billingType: {
          codeBillingType: "",
          descBilling: ""
        }
      }
    },

    provincesData: [],
    loadingProvincesData: false,

    clients: [],
    loadingClients: false,

    userWalletClients: [],
    loadingUserWalletClients: false,

    userWalletClients2: [],
    loadingUserWalletClients2: false,

    movements: [],
    loadingMovements: false,

    latestNews: [],
    loadingLatestNews: false,

    listMarketIndex: [],
    loadingListMarketIndex: false,

    listMarketCards: [],
    loadingMarketCards: false
  },
  applyMiddleware(logger, thunk)
  //   window.devToolsExtension && window.devToolsExtension()
);

export default function reducer(state, action) {
  switch (action.type) {
    case SET_OPEN_BLUR:
      return {
        ...state,
        openBlur: !state.openBlur
      };

    case LOAD_USER_INFORMATION:
      return {
        ...state,
        loadingUserInformation: true
      };
    case SET_USER_INFORMATION:
      return {
        ...state,
        loadingUserInformation: false,
        userInformation: action.payload
      };
    // ***********************************************************
    case LOAD_COIN_MERVAL_DATA:
      return {
        ...state,
        loadingCoinMervalData: true
      };
    case SET_COIN_MERVAL_DATA:
      return {
        ...state,
        loadingCoinMervalData: false,
        coinMervalData: action.payload
      };
    // ***********************************************************
    case LOAD_COIN_PRICES_DATA:
      return {
        ...state,
        loadingCoinPricesData: true
      };
    case SET_COIN_PRICES_DATA:
      return {
        ...state,
        loadingCoinPricesData: false,
        coinPricesData: action.payload
      };
    // ***********************************************************
    case LOAD_FUNDS_DATA:
      return {
        ...state,
        loadingFundsData: true
      };
    case SET_FUNDS_DATA:
      return {
        ...state,
        loadingFundsData: false,
        fundsData: action.payload
      };
    // ***********************************************************
    case LOAD_BONDS_DATA:
      return {
        ...state,
        loadingBondsData: true
      };
    case SET_BONDS_DATA:
      return {
        ...state,
        loadingBondsData: false,
        bondsData: action.payload
      };
    // ***********************************************************
    case LOAD_CEDEARS_DATA:
      return {
        ...state,
        loadingCedearsData: true
      };
    case SET_CEDEARS_DATA:
      return {
        ...state,
        loadingCedearsData: false,
        cedearsData: action.payload
      };
    // ***********************************************************
    case LOAD_EQUITY_DATA:
      return {
        ...state,
        loadingEquityData: true
      };
    case SET_EQUITY_DATA:
      return {
        ...state,
        loadingEquityData: false,
        equityData: action.payload
      };
    // ***********************************************************
    case LOAD_DETAIL_ESPECIE:
      return {
        ...state,
        loadingDetailEspecie: true
      };
    case SET_DETAIL_ESPECIE:
      return {
        ...state,
        loadingDetailEspecie: false,
        detailEspecie: action.payload
      };
    // ***********************************************************
    case LOAD_USER_BALANCE:
      return {
        ...state,
        loadingUserBalance: true
      };
    case SET_USER_BALANCE:
      return {
        ...state,
        loadingUserBalance: false,
        userBalance: action.payload
      };
    // ***********************************************************
    case LOAD_TREASURY_DATA:
      return {
        ...state,
        loadingTreasuryData: true
      };
    case SET_TREASURY_DATA:
      return {
        ...state,
        loadingTreasuryData: false,
        treasuryData: action.payload
      };
    // ***********************************************************
    case LOAD_TEAMS_PARTNER:
      return {
        ...state,
        loadingTeamsPartner: true
      };
    case SET_TEAMS_PARTNER:
      return {
        ...state,
        loadingTeamsPartner: false,
        teamsPartner: action.payload
      };
    // ***********************************************************
    case GET_USERS:
      return {
        ...state,
        users: action.payload
      };
    case REMOVE_USER:
      return {
        ...state,
        users: state.users.filter(user => user.id !== action.payload)
      };
    case ADD_USER:
      return {
        ...state,
        users: [...state.users, action.payload]
      };
    case SET_AUTH:
      return {
        ...state,
        isLogged: !state.isLogged
      };
    case SET_ROUTES:
      return {
        ...state,
        routes: action.payload
      };
    case SET_SELECTED_ROUTE:
      return {
        ...state,
        currentUrl: action.payload
      };
    case SET_EXPANDED:
      return {
        ...state,
        expanded: !state.expanded
      };
    case SET_LOG_OUT:
      return {
        ...state,
        isLogged: !state.isLogged
      };
    case SET_LOG_IN:
      return {
        ...state,
        isLogged: true
      };
    // case SET_UPDATE_USER:
    //   return {
    //     ...state,
    //     userLogged: { ...state.userLogged, data: action.payload }
    //   };
    case SET_SUCCESSFUL_UPDATE_USER:
      return {
        ...state,
        successfullChange: !state.successfullChange
      };
    case SET_RESPONSE_BILLING:
      return {
        ...state,
        responseBilling: !state.responseBilling
      };
    case LOAD_CLIENTS:
      return {
        ...state,
        loadingClients: true
      };
    case SET_CLIENTS:
      return {
        ...state,
        loadingClients: false,
        clients: action.payload
      };

    case LOAD_PROVINCES_DATA:
      return {
        ...state,
        loadingProvincesData: true
      };
    case SET_PROVINCES_DATA:
      return {
        ...state,
        loadingProvincesData: false,
        provincesData: action.payload
      };
    // ***************************
    case LOAD_USER_WALLET_CLIENTS:
      return {
        ...state,
        loadingUserWalletClients: true
      };
    case SET_USER_WALLET_CLIENTS:
      return {
        ...state,
        loadingUserWalletClients: false,
        userWalletClients: action.payload
      };
    case LOAD_USER_WALLET_CLIENTS2:
      return {
        ...state,
        loadingUserWalletClients2: true
      };
    case SET_USER_WALLET_CLIENTS2:
      return {
        ...state,
        loadingUserWalletClients2: false,
        userWalletClients2: action.payload
      };
    // ***************************
    case LOAD_RECOMMENDED_WALLETS:
      return {
        ...state,
        loadingRecommendedWallets: true
      };
    case SET_RECOMMENDED_WALLETS:
      return {
        ...state,
        loadingRecommendedWallets: false,
        recommendedWallets: action.payload
      };

    case LOAD_MOVEMENTS:
      return {
        ...state,
        loadingMovements: true
      };
    case SET_MOVEMENTS:
      return {
        ...state,
        loadingMovements: false,
        movements: action.payload
      };

    case LOAD_USER_DATA:
      return {
        ...state,
        loadingUserLogged: true
      };
    case SET_USER_DATA:
      return {
        ...state,
        loadingUserLogged: false,
        userLogged: action.payload
      };

    case LOAD_LATEST_NEWS:
      return {
        ...state,
        loadingLatestNews: true
      };
    case SET_LATEST_NEWS:
      return {
        ...state,
        loadingLatestNews: false,
        latestNews: action.payload
      };

    case LOAD_LIST_MARKET_INDEX:
      return {
        ...state,
        loadingListMarketIndex: true
      };
    case SET_LIST_MARKET_INDEX:
      return {
        ...state,
        loadingListMarketIndex: false,
        listMarketIndex: action.payload
      };

    case LOAD_MARKET_CARDS:
      return {
        ...state,
        loadingMarketCards: true
      };
    case SET_MARKET_CARDS:
      return {
        ...state,
        loadingMarketCards: false,
        listMarketCards: action.payload
      };

    case LOAD_OPERATIONS:
      return {
        ...state,
        loadingUserOperations: true
      };
    case SET_OPERATIONS:
      return {
        ...state,
        loadingUserOperations: false,
        userOperations: action.payload
      };
    default:
      return state;
  }
}
